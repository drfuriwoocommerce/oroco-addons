(function ($) {
    'use strict';

    var portfolioCarousel = function ($scope, $) {

        $scope.find('.oroco-portfolio-carousel').each(function () {
            var $el = $(this),
                $innerEl = $el.find('.list-portfolio__inner'),
                options = {
                    prevArrow: '<span class="slick-prev-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.73587 0V2.3752H16V3.62346H4.73587V6L0 2.99966L4.73587 0Z"></path></svg></span>',
                    nextArrow: '<span class="slick-next-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.2641 0V2.3752H0V3.62346H11.2641V6L16 2.99966L11.2641 0Z"></path></svg></span>'
                };

            if ($innerEl.children().length < $el.data('show')) {
                return;
            }

            $innerEl.not('.slick-initialized').slick(options);
        });

    };

    var postsCarousel = function ($scope, $) {
        postVideo();
        postGallery();

        $scope.find('.oroco-blog-shortcode').each(function () {
            var $el = $(this),
                $innerEl = $el.find('.oroco-blog-shortcode__list-post'),
                options = {
                    prevArrow: '<span class="slick-prev-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.73587 0V2.3752H16V3.62346H4.73587V6L0 2.99966L4.73587 0Z"></path></svg></span>',
                    nextArrow: '<span class="slick-next-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.2641 0V2.3752H0V3.62346H11.2641V6L16 2.99966L11.2641 0Z"></path></svg></span>'
                };

            if ($innerEl.children().length < $el.data('show')) {
                return;
            }

            $innerEl.not('.slick-initialized').slick(options);
        });

    };

    var postVideo = function () {
        $('.popup-video').magnificPopup({
            type: 'iframe',
            mainClass: 'fmp-fade',
            removalDelay: 300,
            preloader: false,
            fixedContentPos: false,
            disableOn: function () { // don't use a popup for mobile
                if ($(window).width() < 600) {
                    return false;
                }
                return true;
            },
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                        id: 'v=', // String that splits URL in a two parts, second part should be %id%
                        src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    },
                    gmaps: {
                        index: '//maps.google.',
                        src: '%id%&output=embed'
                    }
                },

                srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
            }
        });
    };

    var postGallery = function () {
        var $selector = $('.format-gallery').find('.slides');

        var options = {
            prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
            nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
        };
        $selector.not('.slick-initialized').slick(options);
    };

    var instagramCarousel = function ($scope, $) {

        $scope.find('.oroco-instagram-carousel').each(function () {
            var $el = $(this),
                $innerEl = $el.find('.instagram-wrapper'),
                options = {
                    prevArrow: '<span class="slick-prev-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.73587 0V2.3752H16V3.62346H4.73587V6L0 2.99966L4.73587 0Z"></path></svg></span>',
                    nextArrow: '<span class="slick-next-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.2641 0V2.3752H0V3.62346H11.2641V6L16 2.99966L11.2641 0Z"></path></svg></span>'
                };

            $innerEl.not('.slick-initialized').slick(options);
        });

    };

    /**
     * Faq
     * @param $scope
     * @param $
     */
    var faq = function ($scope, $) {
        $scope.find('.oroco-faq').each(function () {
            var $selector = $(this),
                els = $selector.find('.box-content');

            els.not(':first-child').find('.faq-desc').slideUp();
            els.first().addClass('active');

            els.click(function () {
                var item = $(this),
                    siblings = item.siblings(item);

                if (els.hasClass('.active')) {
                    return;
                }

                siblings.removeClass('active');
                item.addClass('active');
                siblings.find('.faq-desc').slideUp();
                item.find('.faq-desc').slideDown();

            });

        })
    };

    var testimonialsCarousel = function ($scope, $) {

        $scope.find('.oroco-testimonials-carousel').each(function () {
            var $el = $(this),
                options = {
                    prevArrow: '<span class="slick-prev-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.73587 0V2.3752H16V3.62346H4.73587V6L0 2.99966L4.73587 0Z"></path></svg></span>',
                    nextArrow: '<span class="slick-next-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.2641 0V2.3752H0V3.62346H11.2641V6L16 2.99966L11.2641 0Z"></path></svg></span>'
                };

            if ($el.children().length < $el.data('show')) {
                return;
            }

            $el.not('.slick-initialized').slick(options);
        });

    };

    var imgCarousel = function ($scope, $) {

        $scope.find('.oroco-image-carousel').each(function () {
            var $el = $(this),
                $innerEl = $el.find('.content-image'),
                options = {
                    prevArrow: '<span class="slick-prev-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.73587 0V2.3752H16V3.62346H4.73587V6L0 2.99966L4.73587 0Z"></path></svg></span>',
                    nextArrow: '<span class="slick-next-arrow oroco-svg-icon"><svg class="svg-icon" aria-hidden="true" role="img" focusable="false" width="16" height="6" viewBox="0 0 16 6" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.2641 0V2.3752H0V3.62346H11.2641V6L16 2.99966L11.2641 0Z"></path></svg></span>'
                };

            if ($innerEl.children().length < $el.data('show')) {
                return;
            }

            $innerEl.not('.slick-initialized').slick(options);
        });
    };

     var map = function ($scope, $) {
        $scope.find('.oroco-map').each(function () {
            var el = $(this),
                elsMap = el.data('map'),
                wrapper = $(this).attr('id');

            mapboxgl.accessToken = elsMap.token;

            var mapboxClient = mapboxSdk( { accessToken: mapboxgl.accessToken } ),
                local = elsMap.local;

            mapboxClient.geocoding.forwardGeocode( {
                query       : local,
                autocomplete: false,
                limit       : 1
            } )
                .send()
                .then( function ( response ) {
                    if ( response && response.body && response.body.features && response.body.features.length ) {
                        var feature = response.body.features[0];

                        var map = new mapboxgl.Map( {
                            container: wrapper,
                            style    : 'mapbox://styles/mapbox/'+ elsMap.mode +'-v10',
                            center   : feature.center,
                            zoom     : elsMap.zom
                        } );
                        map.on( 'load', function () {
                            map.loadImage( elsMap.marker, function ( error, image ) {
                                if ( error ) throw error;
                                map.addImage( 'marker', image );
                                map.addLayer( {
                                    "id"    : "points",
                                    "type"  : "symbol",
                                    "source": {
                                        "type": "geojson",
                                        "data": {
                                            "type"    : "FeatureCollection",
                                            "features": [{
                                                "type"    : "Feature",
                                                "geometry": {
                                                    "type"       : "Point",
                                                    "coordinates": feature.center
                                                }
                                            }]
                                        }
                                    },
                                    "layout": {
                                        "icon-image": "marker",
                                        "icon-size" : 1
                                    }
                                } );
                            } );
                        } );

                        // disable map zoom when using scroll
                        map.scrollZoom.disable();

                        var geocoder = new MapboxGeocoder( {
                            accessToken: mapboxgl.accessToken
                        } );

                        map.addControl( geocoder );

                        // After the map style has loaded on the page, add a source layer and default
                        // styling for a single point.
                        map.on( 'load', function () {
                            map.addSource( 'single-point', {
                                "type": "geojson",
                                "data": {
                                    "type"    : "FeatureCollection",
                                    "features": []
                                }
                            } );

                            map.addLayer( {
                                "id"    : "point",
                                "source": "single-point",
                                "type"  : "circle",
                                "paint" : {
                                    "circle-radius": 10,
                                    "circle-color" : "#007cbf"
                                }
                            } );

                            map.setPaintProperty( 'water', 'fill-color', elsMap.color_1 );
                            map.setPaintProperty( 'building', 'fill-color', elsMap.color_2 );

                            map.addControl( new mapboxgl.NavigationControl() );
                            // Listen for the `result` event from the MapboxGeocoder that is triggered when a user
                            // makes a selection and add a symbol that matches the result.
                            geocoder.on( 'result', function ( ev ) {
                                map.getSource( 'single-point' ).setData( ev.result.geometry );
                            } );
                        } );
                    }
                } );
        });
    };

    var tab = function ($scope, $) {
        $scope.find('.oroco-tab-shortcode').each(function () {
        
                var $el = $(this).find('.tabs-nav .tab-nav'),
                    $panels = $(this).find('.tab-panel');

                $el.filter(':first').addClass('active');
                $(this).find('.tabs-nav').children().filter(':first').addClass('active');
                $panels.filter(':first').addClass('active');

                $el.on('click', function (e) {
                    e.preventDefault();

                    var $tab = $(this),
                        index = $tab.index();

                    if ($tab.hasClass('active')) {
                        return;
                    }

                    $tab.siblings().removeClass('active');
                    $tab.addClass('active');
                    $panels.removeClass('active');
                    $panels.filter(':eq(' + index + ')').addClass('active');
                });
            });

     };

     var bannerVideo = function ($scope, $) {

        $scope.find('.oroco-banner-video').each(function () {
            $(this).find('.oroco-banner-video__marker').magnificPopup({
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 300,
                preloader: false,
                fixedContentPos: false,
                disableOn: function () { // don't use a popup for mobile
                    if ($(window).width() < 600) {
                        return false;
                    }
                    return true;
                },
                iframe: {
                    patterns: {
                        youtube: {
                            index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                            id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                        },
                        vimeo: {
                            index: 'vimeo.com/',
                            id: '/',
                            src: '//player.vimeo.com/video/%id%?autoplay=1'
                        },
                        gmaps: {
                            index: '//maps.google.',
                            src: '%id%&output=embed'
                        }
                    },

                    srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
                }
            });
        });
    };
    
    var slideCarousel = function ($scope, $) {
		$scope.find('.oroco-slides-wrapper').each(function () {
			var $selector = $(this).find('.oroco-slides'),
				elementSettings = $selector.data('slider_options'),
				$arrow_wrapper = $(this).find('.arrows-inner'),
				slidesToShow = parseInt(elementSettings.slidesToShow);

			$selector.not('.slick-initialized').slick({
				rtl: $('body').hasClass('rtl'),
				slidesToShow: slidesToShow,
				arrows: elementSettings.arrows,
				appendArrows: $arrow_wrapper,
				dots: elementSettings.dots,
				infinite: elementSettings.infinite,
				prevArrow: '<span class="slick-prev-arrow"><i class="icon-chevron-left"></i><span class="text">' + elementSettings.btn_text_prev + '</span></span>',
                nextArrow: '<span class="slick-next-arrow"><span class="text">' + elementSettings.btn_text_next + '</span><i class="icon-chevron-right"></i></span>',
				autoplay: elementSettings.autoplay,
				autoplaySpeed: parseInt(elementSettings.autoplaySpeed),
				speed: parseInt(elementSettings.speed),
				fade: elementSettings.fade,
				pauseOnHover: elementSettings.pauseOnHover,
				responsive: []
			});

			$selector.imagesLoaded(function () {
				$selector.closest('.oroco-slides-wrapper').removeClass('loading');
			});

			var animation = $selector.data('animation');

			if (animation) {
				$selector
					.on('beforeChange', function () {
						var $sliderContent = $selector.find('.oroco-slide-content'),
							$sliderPriceBox = $selector.find('.oroco-slide-price-box');

						$sliderContent.removeClass('animated' + ' ' + animation).hide();

						$sliderPriceBox.removeClass('animated zoomIn').hide();
					})
					.on('afterChange', function (event, slick, currentSlide) {
						var $currentSlide = $(slick.$slides.get(currentSlide)).find('.oroco-slide-content'),
							$currentPriceBox = $(slick.$slides.get(currentSlide)).find('.oroco-slide-price-box');

						$currentSlide.show().addClass('animated' + ' ' + animation);

						$currentPriceBox.show().addClass('animated zoomIn');
					});
            }
            
            $(window).on('resize', function () {
                if ($(window).width() < 767) {
                    $selector.find('.oroco-slide-price-box .text').click(function () {
                        $selector.closest('.oroco-slides-wrapper').addClass('hide-pagination');
                    });

                    $(document).mouseup(function(e) {
                        var containerText = $selector.find('.oroco-slide-price-box .text');
                        if (!containerText.is(e.target) && containerText.has(e.target).length === 0) {
                            $selector.closest('.oroco-slides-wrapper').removeClass('hide-pagination');
                        }
                    });
                }
            }).trigger('resize');
		});
    };

    var slideCarousel2 = function ($scope, $) {
		$scope.find('.oroco-slides-wrapper-2').each(function () {
            var $selector = $(this).closest('.swiper-container');

            var swiper = new Swiper($selector.eq(0), {
                direction: 'vertical',
				clickable         : true,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
				parallax                    : true,
				loop                        : false,
				autoplay                    : false,
				autoplayDisableOnInteraction: false,
				speed                       : 1000,
				keyboardControl             : true,
				mousewheelControl           : false,
                mousewheel: {
                    releaseOnEdges          : true,
                    sensitivity             : 1,
                },
            });

            swiper.on('slideChangeTransitionEnd', function() {  
                // check, if active slide has a class of 'second-child'
                if (this.slides[this.activeIndex].classList.contains("text-color-light")) {
                    $('body').addClass('header-text-light');
                } else {
                    $('body').removeClass('header-text-light');
                }
            });
            
		});
    };

    var bannersParallax = function ($scope, $) {
		$scope.find('.oroco-banners-parallax').each(function () {
            var $selector = $(this).find('.oroco-jarallax');

            $selector.jarallax({
                speed: 0.5,
                imgWidth: 1366,
                imgHeight: 768
            })
		});
    };

    /**
	 * Load Products
	 */
	var loadProductsGrid = function ($selector) {
		$selector.each(function () {
			var $this = $(this),
				ajax_url = orocoData.ajax_url.toString().replace('%%endpoint%%', 'oroco_ajax_load_products');

			// Load Products
			$this.on('click', 'a.ajax-load-products', function (e) {
				e.preventDefault();

				var $el = $(this);

				if ($el.hasClass('loading')) {
					return;
				}

				$el.addClass('loading');

				$.post(
					ajax_url,
					{
						page: $el.data('page'),
						settings: $el.data('settings'),
						type: $el.data('type'),
						load_more: $el.data('load_more'),
						text: $el.data('text'),
						nonce: $el.data('nonce')
					},
					function (response) {
						if (!response) {
							return;
						}

						$el.removeClass('loading');

						var $data = $(response.data),
							$products = $data.find('ul.products > li'),
							$button = $data.find('.ajax-load-products'),
							$container = $el.closest('.or-products-grid'),
							$grid = $container.find('ul.products');

						// If has products
						if ($products.length) {
							// Add classes before append products to grid
							$products.addClass('product');

							for (var index = 0; index < $products.length; index++) {
								$($products[index]).css('animation-delay', index * 100 + 100 + 'ms');
							}

							$products.addClass('orocoFadeInUp orocoAnimation');
							$grid.append($products);

							if ($button.length) {
								$el.replaceWith($button);
							} else {
								$el.slideUp();
								$el.closest('.load-more').addClass('loaded');
							}
						}
					}
				);
			});

			// AJAX Filter
			$this.find('ul.product-filter li:first').addClass('active');
			$this.on('click', 'ul.product-filter li', function (e) {
				e.preventDefault();

				var $el = $(this);

				if ($el.hasClass('loading')) {
					return;
				}

				$el.addClass('active').siblings('.active').removeClass('active');

				var filter = $el.attr('data-filter'),
					$wrapper = $this.find('.products-wrapper');

				var data = {
					nonce: $this.data('nonce'),
					text: $this.data('text'),
					type: $this.data('type')
				};

				data.settings = $this.data('settings');
				data.settings.product_cats = filter;

				data.load_more = $this.data('load_more');

				$this.addClass('loading');

				$.post(
					ajax_url,
					data,
					function (response) {
						if (!response) {
							return;
						}

						var $data = $(response.data),
							$products = $data.find('ul.products > li'),
							$button = $data.find('.ajax-load-products');

						$this.removeClass('loading');

						// If has products
						if ($products.length) {
							// Add classes before append products to grid
							$products.addClass('product');

							for (var index = 0; index < $products.length; index++) {
								$($products[index]).css('animation-delay', index * 100 + 100 + 'ms');
							}

							$products.addClass('orocoFadeInUp orocoAnimation');

							$wrapper.children('div.woocommerce, .load-more').remove();
							$button.css('animation-delay', $products.length * 100 + 100 + 'ms').addClass('orocoFadeIn orocoAnimation');
							$wrapper.append($data);
						}
					}
				);
			});
		});
	};

    var productsGridAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.or-products-grid');

		if (!$selector.hasClass('no-infinite')) {
			productGridInfinite($selector);
		} else {
			loadProductsGrid($selector);
		}

		function productGridInfinite($selector) {
			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductGridlAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductGridlAjax($selector) {

			if (typeof or_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.or-products-grid-loading').data('settings'),
				ajax_url = or_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'or_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsGrid'
				},
				success: function (response) {
					$selector.html(response.data);
					loadProductsGrid($selector)
					$(document.body).trigger('oroco_get_products_ajax_success');
				}
			});
		}
	};

    var getProductCarousel = function ($selector, extendOptions = {}) {
		var $slider = $selector.find('ul.products,ul.product-list,ul.product-metro'),
			dataSettings = $selector.data('settings');

		var data = JSON.stringify(dataSettings);

		var options = {
			prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
			nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
        };
        
        if( $selector.data('carousel') == '' ) {
            return;
        }

		$.extend(true, options, extendOptions);

		$slider.attr('data-slick', data);
		$slider.not('.slick-initialized').slick(options);
    };

    /**
	 * Get Product AJAX
	 */
	var getProductsAJAXHandler = function ($el, $tabs) {
		var tab = $el.data('href'),
			$content = $tabs.find('.tabs-' + tab);

		if ($content.hasClass('tab-loaded')) {
			return;
		}

		var data = {},
			elementSettings = $content.data('settings'),
			ajax_url = orocoData.ajax_url.toString().replace('%%endpoint%%', 'or_elementor_load_products');

		$.each(elementSettings, function (key, value) {
			data[key] = value;
		});

		$.post(
			ajax_url,
			data,
			function (response) {
				if (!response) {
					return;
				}

				$content.find('.tab-content').html(response.data);

				getProductCarousel($content.closest('.or-elementor-product-carousel'));

				$content.addClass('tab-loaded');
			}
		);
    };
    
    var productTabsActive = function ($selector) {
		var $el     = $selector.find( '.tabs-nav a' ),
			$panels = $selector.find( '.tabs-panel' );

			$el.filter( ':first' ).addClass( 'active' );
            $panels.filter( ':first' ).addClass( 'active' );
            
		$selector.find('.tabs-nav').on('click', 'a', function (e) {
			e.preventDefault();
			var $this = $(this),
				currentTab = $this.data('href');

			if ($this.hasClass('active')) {
				return;
			}

			$selector.find('.tabs-nav a').removeClass('active');
			$this.addClass('active');
			$selector.find('.tabs-panel').removeClass('active');
			$selector.find('.tabs-' + currentTab).addClass('active');

			$selector.find('.slick-slider').slick('setPosition');
		});
	};

    var productsTabAjaxHandle = function ($scope, $) {
		var $selector = $scope.find('.or-products-tabs');

		if (!$selector.hasClass('no-infinite')) {
			productCarouselInfinite($selector);
		} else {
			getProductCarousel($selector);
			productTabsActive($selector);
			$selector.find('.tabs-nav').on('click', 'a', function (e) {
				getProductsAJAXHandler($(this), $selector);
			});
		}

		function productCarouselInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductAjax($selector) {

			if (typeof or_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.or-product-tab-loading').data('settings'),
				ajax_url = or_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'or_elementor_get_elements');

			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsTab'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					productTabsActive($selector);
					$(document.body).trigger('oroco_get_products_ajax_success');
				}
			});

			$( document.body ).on( 'oroco_get_products_ajax_success', function () {
				$selector.find('.slick-slider').slick('setPosition');
			});
		}
    };
    
    /**
	 * Product Metro
	 */
	var productsMetro = function ($scope, $) {
		var $selector = $scope.find('.or-product-metro');

		if (!$selector.hasClass('no-infinite')) {
			productMetroInfinite($selector);
		} else {
			getProductCarousel($selector);
		}

		function productMetroInfinite($selector) {

			$(window).on('scroll', function () {

				if ($selector.hasClass('no-infinite')) {
					return;
				}

				var offSet = 0;

				if ($selector.is(':in-viewport(' + offSet + ')')) {
					getProductMetrolAjax($selector);
					$selector.addClass('no-infinite');
				}

			}).trigger('scroll');
		}

		function getProductMetrolAjax($selector) {

			if (typeof or_elementor_data === 'undefined') {
				return;
			}

			var dataSettings = $selector.find('.or-product-metro-loading').data('settings'),
				ajax_url = or_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'or_elementor_get_elements');


			$.ajax({
				url: ajax_url,
				dataType: 'json',
				method: 'post',
				data: {
					params: JSON.stringify(dataSettings),
					element: 'productsMetro'
				},
				success: function (response) {
					$selector.html(response.data);
					getProductCarousel($selector);
					$(document.body).trigger('oroco_get_products_ajax_success');
				}
			});
        }

        var $container = $selector.find( 'ul.products-metro-list.layout-masonry' ),
            $window = $( window );
        
        initMansonry();

        $window.on( 'resize', initMansonry );

        $( document.body ).on( 'post-load oroco_products_loaded', function( event, products, appended ) {
            if ( $window.width() < 992 ) {
                return;
            }

            if ( appended ) {
                $container.masonry( 'appended', products ).masonry();
            } else {
                $container = $( 'ul.products.layout-masonry' );

                initMansonry();
            }
        } );

        /**
         * Init masonry layout
         */
        function initMansonry() {
            if ( $container.children().length <= 1 ) {
                return;
            }

            if ( $window.width() < 992 ) {
                $container.each( function() {
                    var $this = $( this );

                    if ( $this.hasClass( 'masonry' ) ) {
                        $this.masonry( 'destroy' );
                    }
                } );
            } else {
                $container.each( function() {
                    var $this = $( this ),
                        options = {
                            itemSelector:       'li.product',
                            columnWidth:        $container.hasClass('masonry-v2') ? 'li.product:nth-child(2)' : 'li.product:nth-child(3)',
                            percentPosition:    true,
                            transitionDuration: 100
                        };

                    if ( ! $this.hasClass( 'masonry' ) ) {
                        $this.masonry( options );

                        $this.imagesLoaded().progress( function() {
                            $this.masonry( 'layout' );
                        } );
                    }
                } );
            }
        }
	};

     /**
     * Ajax Handle
     */
    var productsCarouselAjaxHandle = function ($scope, $) {
        var $selector = $scope.find('.oroco-products-carousel'),
            extendOptions = {};

        if (!$selector.hasClass('no-infinite')) {
            productCarouselInfinite($selector);
        } else {
            getProductCarousel($selector,extendOptions);
        }

        function productCarouselInfinite($selector) {

            $(window).on('scroll', function () {

                if ($selector.hasClass('no-infinite')) {
                    return;
                }

                var offSet = 0;

                if ($selector.is(':in-viewport(' + offSet + ')')) {
                    getProductCarouselAjax($selector);
                    $selector.addClass('no-infinite');
                }

            }).trigger('scroll');
        }

        function getProductCarouselAjax($selector) {

            if (typeof or_elementor_data === 'undefined') {
                return;
            }

            var dataSettings = $selector.find('.or-product-carousel-loading').data('settings'),
                ajax_url = or_elementor_data.ajax_url.toString().replace('%%endpoint%%', 'or_elementor_get_elements');

            $.ajax({
                url: ajax_url,
                dataType: 'json',
                method: 'post',
                data: {
                    params: JSON.stringify(dataSettings),
                    element: 'productsCarousel'
                },
                success: function (response) {
                    $selector.html(response.data);
                    getProductCarousel($selector,extendOptions);
                    $(document.body).trigger('oroco_get_products_ajax_success');
                }
            });
        }
    };

    /**
     * Elementor JS Hooks
     */
    $(window).on("elementor/frontend/init", function () {

        // portfolio
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-portfolio-carousel.default",
            portfolioCarousel
        ); 

        // post
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-blog-shortcode.default",
            postsCarousel
        ); 

        // testi
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-testimonials-carousel.default",
            testimonialsCarousel
        );

        // img caarousel
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-image-carousel.default",
            imgCarousel
        );

        // insta
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-instagram-carousel.default",
            instagramCarousel
        );

        // FAQ
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-faq.default",
            faq
        );

        // map
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-map.default",
            map
        );

        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-tab-shortcode.default",
            tab
        );

        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-banner-video.default",
            bannerVideo
        );
        
        // Slider
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/oroco-slides.default",
			slideCarousel
        );
        
        // Slider 2
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/oroco-slides-2.default",
			slideCarousel2
        );

        // Banner Parallax
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/oroco-banners-parallax.default",
			bannersParallax
        );

        // Product grid
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/oroco-product-grid.default",
			productsGridAjaxHandle
        );
        
        // Product tab carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/oroco-product-tab.default",
			productsTabAjaxHandle
        );
        
        // Product Metro
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/oroco-product-metro.default",
			productsMetro
		);

        elementorFrontend.hooks.addAction(
            "frontend/element_ready/oroco-products-carousel.default",
            productsCarouselAjaxHandle
        );

    });
})
(jQuery);