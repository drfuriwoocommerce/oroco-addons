(function ($) {
    'use strict';
    
    /*******************************
    ******* Section Sticky *********
    *******************************/
    var sectionSticky = function( $scope ) {
        if ( ! $scope.hasClass( 'fm-sticky-on' ) ) {
            return;
        }

        var data = $scope.data('settings'),
            $wpAdminBar = elementorFrontend.elements.$wpAdminBar;

        var offSet = data.fm_sticky_offset ? data.fm_sticky_offset : 0;
        
        if ($wpAdminBar.length && 'top' === data.fm_sticky && 'fixed' === $wpAdminBar.css('position')) {
            offSet += $wpAdminBar.height();
        }

        var options = {
            getWidthFrom : $('body'),
            responsiveWidth : true,
            topSpacing : offSet,
            wrapperClassName: 'fm-sticky-wrapper'
        };

        if ( data.fm_sticky_z_index ) {
            options.zIndex = data.fm_sticky_z_index;
        }

        if ( data.fm_transparent === 'yes' ) {
            options.wrapperClassName = 'fm-sticky-wrapper fm-transparent-wrapper';
        }

        $scope.sticky(options);

        $scope.on( 'sticky-end', function() {
            $scope.removeClass('fm-is-sticky');

            $(window).on('resize').trigger('resize');
        });

        $scope.on( 'sticky-start', function() {
            $scope.addClass('fm-is-sticky');
        });

        window.addEventListener('resize', function (event) {
            $scope.unstick();

            var newOffSet = data.fm_sticky_offset ? data.fm_sticky_offset : 0;

            if ($wpAdminBar.length && 'top' === data.fm_sticky && 'fixed' === $wpAdminBar.css('position')) {
                newOffSet += $wpAdminBar.height();
            }

            options.topSpacing = newOffSet;

            $scope.sticky(options);

            $scope.sticky('update');
        })
    };

    /**
     * Elementor JS Hooks
     */
    $(window).on("elementor/frontend/init", function () {
        elementorFrontend.hooks.addAction( 'frontend/element_ready/section', sectionSticky );
    });
})
(jQuery);