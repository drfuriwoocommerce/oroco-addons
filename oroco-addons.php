<?php
/**
 * Plugin Name: Oroco Addons
 * Plugin URI: http://drfuri.com/plugins/oroco-addons.zip
 * Description: Extra elements for Elementor. It was built for Oroco theme.
 * Version: 1.0.0
 * Author: Drfuri
 * Author URI: http://drfuri.com/
 * License: GPL2+
 * Text Domain: oroco
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'OROCO_ADDONS_DIR' ) ) {
	define( 'OROCO_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'OROCO_ADDONS_URL' ) ) {
	define( 'OROCO_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}
require_once OROCO_ADDONS_DIR . '/inc/functions.php';
require_once OROCO_ADDONS_DIR . '/inc/backend/theme-builder.php';
require_once OROCO_ADDONS_DIR . '/inc/backend/portfolio.php';
require_once OROCO_ADDONS_DIR . '/inc/widgets/widget.php';

if ( is_admin() ) {
	require_once OROCO_ADDONS_DIR . '/inc/backend/importer.php';
}

/**
 * Init
 */
function oroco_vc_addons_init() {
	load_plugin_textdomain( 'THEME_DOMAIN', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new Oroco_Theme_Builder;
	new Oroco_Portfolio;
}

add_action( 'after_setup_theme', 'oroco_vc_addons_init', 20 );

/**
 * Undocumented function
 */
function oroco_init_elementor() {
	// Check if Elementor installed and activated
	if ( ! did_action( 'elementor/loaded' ) ) {
		return;
	}

	// Check for required Elementor version
	if ( ! version_compare( ELEMENTOR_VERSION, '2.0.0', '>=' ) ) {
		return;
	}

	// Check for required PHP version
	if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
		return;
	}

	// Once we get here, We have passed all validation checks so we can safely include our plugin
	include_once( OROCO_ADDONS_DIR . 'inc/elementor/elementor.php' );
	include_once( OROCO_ADDONS_DIR . 'inc/elementor/elementor-ajaxloader.php' );
	include_once( OROCO_ADDONS_DIR . 'inc/elementor/controls.php' );
}

add_action( 'plugins_loaded', 'oroco_init_elementor' );

