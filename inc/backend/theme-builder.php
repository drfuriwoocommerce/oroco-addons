<?php

/**
 * Register portfolio support
 */
class Oroco_Theme_Builder {

	/**
	 * Class constructor.
	 */
	public function __construct() {

		if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
			return;
		}

		add_action( 'admin_menu', array( $this, 'register_admin_menu' ), 50 );

		// Make sure the post types are loaded for imports
		add_action( 'import_start', array( $this, 'register_post_type' ) );

		// Register custom post type and custom taxonomy
		add_action( 'init', array( $this, 'register_post_type' ) );

		add_filter( 'single_template', array( $this, 'load_canvas_template' ) );

	}

	/**
	 * Register portfolio post type
	 */
	public function register_post_type() {

		$labels = array(
			'name'               => esc_html__( 'Header Builder Template', 'oroco' ),
			'singular_name'      => esc_html__( 'Elementor Header', 'oroco' ),
			'menu_name'          => esc_html__( 'Header Template', 'oroco' ),
			'name_admin_bar'     => esc_html__( 'Elementor Header', 'oroco' ),
			'add_new'            => esc_html__( 'Add New', 'oroco' ),
			'add_new_item'       => esc_html__( 'Add New Header', 'oroco' ),
			'new_item'           => esc_html__( 'New Header Template', 'oroco' ),
			'edit_item'          => esc_html__( 'Edit Header Template', 'oroco' ),
			'view_item'          => esc_html__( 'View Header Template', 'oroco' ),
			'all_items'          => esc_html__( 'All Elementor Header', 'oroco' ),
			'search_items'       => esc_html__( 'Search Header Templates', 'oroco' ),
			'parent_item_colon'  => esc_html__( 'Parent Header Templates:', 'oroco' ),
			'not_found'          => esc_html__( 'No Header Templates found.', 'oroco' ),
			'not_found_in_trash' => esc_html__( 'No Header Templates found in Trash.', 'oroco' ),
		);

		$args = array(
			'labels'              => $labels,
			'public'              => true,
			'rewrite'             => false,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'menu_icon'           => 'dashicons-editor-kitchensink',
			'supports'            => array( 'title', 'thumbnail', 'elementor' ),
		);

		if ( ! post_type_exists( 'or_header' ) ) {
			register_post_type( 'or_header', $args );
		}

		// Footer Builder
		$labels = array(
			'name'               => esc_html__( 'Footer Builder Template', 'oroco' ),
			'singular_name'      => esc_html__( 'Elementor Footer', 'oroco' ),
			'menu_name'          => esc_html__( 'Footer Template', 'oroco' ),
			'name_admin_bar'     => esc_html__( 'Elementor Footer', 'oroco' ),
			'add_new'            => esc_html__( 'Add New', 'oroco' ),
			'add_new_item'       => esc_html__( 'Add New Footer', 'oroco' ),
			'new_item'           => esc_html__( 'New Footer Template', 'oroco' ),
			'edit_item'          => esc_html__( 'Edit Footer Template', 'oroco' ),
			'view_item'          => esc_html__( 'View Footer Template', 'oroco' ),
			'all_items'          => esc_html__( 'All Elementor Footer', 'oroco' ),
			'search_items'       => esc_html__( 'Search Footer Templates', 'oroco' ),
			'parent_item_colon'  => esc_html__( 'Parent Footer Templates:', 'oroco' ),
			'not_found'          => esc_html__( 'No Footer Templates found.', 'oroco' ),
			'not_found_in_trash' => esc_html__( 'No Footer Templates found in Trash.', 'oroco' ),
		);

		$args = array(
			'labels'              => $labels,
			'public'              => true,
			'rewrite'             => false,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'menu_icon'           => 'dashicons-editor-kitchensink',
			'supports'            => array( 'title', 'thumbnail', 'elementor' ),
		);


		if ( ! post_type_exists( 'or_footer' ) ) {
			register_post_type( 'or_footer', $args );
		}

		// Mega Menu Builder
		$labels = array(
			'name'               => esc_html__( 'Mega Menu Builder Template', 'oroco' ),
			'singular_name'      => esc_html__( 'Elementor Mega Menu', 'oroco' ),
			'menu_name'          => esc_html__( 'Mega Menu Template', 'oroco' ),
			'name_admin_bar'     => esc_html__( 'Elementor Mega Menu', 'oroco' ),
			'add_new'            => esc_html__( 'Add New', 'oroco' ),
			'add_new_item'       => esc_html__( 'Add New Mega Menu', 'oroco' ),
			'new_item'           => esc_html__( 'New Mega Menu Template', 'oroco' ),
			'edit_item'          => esc_html__( 'Edit Mega Menu Template', 'oroco' ),
			'view_item'          => esc_html__( 'View Mega Menu Template', 'oroco' ),
			'all_items'          => esc_html__( 'All Elementor Mega Menu', 'oroco' ),
			'search_items'       => esc_html__( 'Search Mega Menu Templates', 'oroco' ),
			'parent_item_colon'  => esc_html__( 'Parent Mega Menu Templates:', 'oroco' ),
			'not_found'          => esc_html__( 'No Mega Menu Templates found.', 'oroco' ),
			'not_found_in_trash' => esc_html__( 'No Mega Menu Templates found in Trash.', 'oroco' ),
		);

		$args = array(
			'labels'              => $labels,
			'public'              => true,
			'rewrite'             => false,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'menu_icon'           => 'dashicons-editor-kitchensink',
			'supports'            => array( 'title', 'thumbnail', 'elementor' ),
		);


		if ( ! post_type_exists( 'or_mega_menu' ) ) {
			register_post_type( 'or_mega_menu', $args );
		}

	}

	public function register_admin_menu() {
		add_submenu_page(
			'edit.php?post_type=elementor_library',
			esc_html__( 'Header Builder', 'oroco' ),
			esc_html__( 'Header Builder', 'oroco' ),
			'edit_pages',
			'edit.php?post_type=or_header'
		);

		add_submenu_page(
			'edit.php?post_type=elementor_library',
			esc_html__( 'Footer Builder', 'oroco' ),
			esc_html__( 'Footer Builder', 'oroco' ),
			'edit_pages',
			'edit.php?post_type=or_footer'
		);

	}


	function load_canvas_template( $single_template ) {

		global $post;

		if ( 'or_header' == $post->post_type || 'or_footer' == $post->post_type || 'or_mega_menu' == $post->post_type ) {

			return ELEMENTOR_PATH . '/modules/page-templates/templates/canvas.php';
		}

		return $single_template;
	}

}