<?php
/**
 * Hooks for importer
 *
 * @package Oroco
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function oroco_vc_addons_importer() {
	return array(
		array(
			'name'       => 'Home Supermarket',
			'preview'    => 'https://demo4.drfuri.com/wp-content/soo-importer/oroco/preview.jpg',
			'content'    => 'https://demo4.drfuri.com/wp-content/soo-importer/oroco/demo-content.xml',
			'customizer' => 'https://demo4.drfuri.com/wp-content/soo-importer/oroco/customizer.dat',
			'widgets'    => 'https://demo4.drfuri.com/wp-content/soo-importer/oroco/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Default',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 400,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
	);
}

add_filter( 'soo_demo_packages', 'oroco_vc_addons_importer', 20 );
