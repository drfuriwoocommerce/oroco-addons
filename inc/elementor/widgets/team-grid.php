<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Team Grid widget
 */
class Team_Grid extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-team-grid';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Team Grid', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-posts-grid';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}
	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	protected function section_content() {
		$this->section_setting_content();
	}

	protected function section_setting_content( ) {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'full',
				'separator' => 'none',
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Image', 'oroco' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/270x345/f5f5f5?text=270x345',
				],
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'description'   => esc_html__( 'Just works if the value of Lightbox is No', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		
		$this->add_control(
			'name',
			[
				'label'       => esc_html__( 'Name', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Member\'s Name', 'oroco' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'job',
			[
				'label'       => esc_html__( 'Job', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Member\'s Job', 'oroco' ),
				'label_block' => true,
			]
		);

		$icon = new \Elementor\Repeater();
		$icon->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'oroco' ),
				'type'    => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'fab fa-facebook-f',
					'library' => 'fa-brands',
				],
			]
		);
		$icon->add_control(
			'link_icon', [
				'label'         => esc_html__( 'Link', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'socials',
			[
				'label'         => esc_html__( 'Socials', 'oroco' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $icon->get_controls(),
				'default'       => [
					[
						'icon'  => [
							'value'   => 'fab fa-facebook-f',
						],
						'link_icon'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'icon'  => [
							'value'   => 'fab fa-twitter',
						],
						'link_icon'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'icon'  => [
							'value'   => 'fab fa-instagram',
						],
						'link_icon'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],[
						'icon'  => [
							'value'   => 'fab fa-pinterest-p',
						],
						'link_icon'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					]
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();
	}

	// Style
	protected function section_style() {
		$this->section_content_style();
		$this->section_element_style();
	}

	protected function section_content_style() {
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'position',
			[
				'label'     => esc_html__( 'Align', 'oroco' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'padding',
			[
				'label' => esc_html__( 'Padding ', 'oroco' ),
				'type'  => Controls_Manager::DIMENSIONS,
				'size_units' => ['px','%','em' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-team-grid' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

		$this->add_control(
			'content_border_style',
			[
				'label'        => __( 'Border', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'border_style',
			[
				'label'     => esc_html__( 'Border Style', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'oroco' ),
					'dashed' => esc_html__( 'Dashed', 'oroco' ),
					'solid'  => esc_html__( 'Solid', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid' => 'border-style: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'border_width',
			[
				'label' => __( 'Border Width', 'oroco' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->add_control(
			'bk_color',
			[
				'label'     => __( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid'     => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'label'         => esc_html__( 'Shadow', 'oroco' ),
				'name'          => 'item_shadow',
				'selector'      => '{{WRAPPER}} .oroco-team-grid',
			]
		);

		$this->end_controls_section();
	}

	protected function section_element_style() {
		$this->start_controls_section(
			'section_element_style',
			[
				'label' => esc_html__( 'Elements', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'img_style',
			[
				'label' => esc_html__( 'Image', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		$this->add_responsive_control(
			'img_spacing',
			[
				'label'      => __( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'default'    => [],
				'selectors'  => [
					'{{WRAPPER}} .oroco-team-grid .team-image' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'text_style',
			[
				'label' => esc_html__( 'Text', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
				'separator'  => 'before',
			]
		);

		$this->start_controls_tabs('content_subtext');

		$this->start_controls_tab(
			'content_text_name',
			[
				'label'         => esc_html__( 'Name', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'sub_spacing',
			[
				'label'      => __( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'default'    => [],
				'selectors'  => [
					'{{WRAPPER}} .oroco-team-grid .team-name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_sub_typography',
				'selector' => '{{WRAPPER}} .oroco-team-grid .team-name',
			]
		);

		$this->add_control(
			'sub_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid .team-name, {{WRAPPER}} .oroco-team-grid .team-name a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'content_text_job',
			[
				'label'         => esc_html__( 'Job', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'job_spacing',
			[
				'label'      => __( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'default'    => [],
				'selectors'  => [
					'{{WRAPPER}} .oroco-team-grid .team-job' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'job_typography',
				'selector' => '{{WRAPPER}} .oroco-team-grid .team-job',
			]
		);

		$this->add_control(
			'job_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid .team-job'  => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'icon_style',
			[
				'label' => esc_html__( 'Socials', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
				'separator'  => 'before',
			]
		);

		$this->add_control(
			'icon_style_setting',
			[
				'label'        => __( 'Setting', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_responsive_control(
			'icon_spacing',
			[
				'label'      => __( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'default'    => [],
				'selectors'  => [
					'{{WRAPPER}} .oroco-team-grid .oroco-icon' => 'margin: 0 {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'icon_size',
			[
				'label'      => __( 'Font Size', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'default'    => [],
				'selectors'  => [
					'{{WRAPPER}} .oroco-team-grid .oroco-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-team-grid .oroco-icon'  => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_section();
	}
	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'oroco-team-grid',
			]
		);

		$els = $settings['socials'];

		$image = Group_Control_Image_Size::get_attachment_image_html( $settings );

		if ( $settings['link']['url'] ) :
			$image = $this->get_link_control( 'link', $settings['link'], $image, [ 'class' => 'has-link' ] );
		endif;
		$output_content = [];

		$image_html =  $image ? '<div class="team-image">'.$image.'</div>' : '';

		$output_content[] = '<div class="team-summary">';
		if ( $settings['name'] ) {
			$output_content[] = sprintf( '<h6 class="team-name">%s</h6>', $this->get_link_control( 'link-2', $settings['link'], $settings['name'], '' ) );
		}

		if ( $settings['job'] ) {
			$output_content[] = sprintf( '<div class="team-job">%s</div>', $settings['job'] );
		}

		if ( ! empty ( $els ) ) {

			$output_content[] = '<div class="member-socials">';
			foreach ( $els as $index => $item ) {
				$link_key = 'link_' . $index;
				$icon     = '';
				if ( $item['icon'] ) {
					if ( $item['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
						ob_start();
						\Elementor\Icons_Manager::render_icon( $item['icon'], [ 'aria-hidden' => 'true' ] );
						$icon = '<span class="oroco-icon">' . ob_get_clean() . '</span>';
					}
				}

				$link = $item['link_icon'] ? $this->get_link_control( $link_key, $item['link_icon'], $icon, ['class' => 'has-link' ] ) : $icon;

				$output_content[] = sprintf( '%s', $link );
			}
			$output_content[] = '</div>';
		}
		$output_content[] = '</div>';

		?>
        <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
	        <?php echo $image_html ?>
	        <?php echo implode( '', $output_content ) ?>
        </div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {

	}

	protected function get_link_control( $link_key, $url, $content, $attr = [] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}

}