<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Widget_Base;
use OrocoAddons\Elementor;
use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Product_Categories_Masonry extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-product-categories-masonry';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Product Categories Masonry', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-inner-section';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->_register_categories_settings_controls();
		$this->_register_section_style();
	}

	protected function _register_categories_settings_controls() {
		// Cats Content
		$this->start_controls_section(
			'cats_content',
			[
				'label' => esc_html__( 'Categories', 'oroco' ),
			]
		);

		$this->add_control(
			'columns',
			[
				'label'        => esc_html__( 'Columns', 'oroco' ),
				'type'         => Controls_Manager::SELECT,
				'options'      => [
					'3' => esc_html__( '3 Columns', 'oroco' ),
					'4' => esc_html__( '4 Columns', 'oroco' ),
				],
				'default'      => '3',
				'toggle'       => false,
				'prefix_class' => 'columns-',
			]
		);

		$this->add_control(
			'cats_source',
			[
				'label'       => esc_html__( 'Source', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'default' => esc_html__( 'Default', 'oroco' ),
					'custom'  => esc_html__( 'Custom', 'oroco' ),
				],
				'default'     => 'default',
				'label_block' => false,
			]
		);
		$this->add_control(
			'cats_display',
			[
				'label'       => esc_html__( 'Categories Display', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'all'    => esc_html__( 'All', 'oroco' ),
					'parent' => esc_html__( 'Parent Categories Only', 'oroco' ),
				],
				'default'     => 'parent',
				'label_block' => true,
				'condition'   => [
					'cats_source' => 'default',
				],
			]
		);
		$this->add_control(
			'cats_number',
			[
				'label'       => esc_html__( 'Categories to show', 'oroco' ),
				'description' => esc_html__( 'Set 0 to show all categories', 'oroco' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 8,
				'condition'   => [
					'cats_source' => 'default',
				],
			]
		);
		$this->add_control(
			'cats_orderby',
			[
				'label'       => esc_html__( 'Order by', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'name'  => esc_html__( 'Name', 'oroco' ),
					'id'    => esc_html__( 'ID', 'oroco' ),
					'count' => esc_html__( 'Count', 'oroco' ),
					'menu_order' => esc_html__( 'Menu Order', 'oroco' ),
				],
				'default'     => 'name',
				'label_block' => false,
				'condition'   => [
					'cats_source' => 'default',
				],
			]
		);

		$this->add_control(
			'cats_order',
			[
				'label'       => esc_html__( 'Order', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'ASC'  => esc_html__( 'ASC', 'oroco' ),
					'DESC' => esc_html__( 'DESC', 'oroco' ),
				],
				'default'     => 'ASC',
				'label_block' => false,
				'condition'   => [
					'cats_source' => 'default',
				],
			]
		);

		$this->add_control(
			'cats_selected',
			[
				'label'       => esc_html__( 'Categories', 'oroco' ),
				'placeholder' => esc_html__( 'Click here and start typing...', 'oroco' ),
				'type'        => 'orautocomplete',
				'default'     => '',
				'label_block' => true,
				'multiple'    => true,
				'source'      => 'product_cat',
				'sortable'    => true,
				'condition'   => [
					'cats_source' => 'custom',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'full',
				'separator' => 'before',
			]
		);

		$this->end_controls_section(); // End Cats Content
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function _register_section_style() {
		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Button', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'vertical_position',
			[
				'label'                => esc_html__( 'Vertical Position', 'oroco' ),
				'type'                 => Controls_Manager::CHOOSE,
				'label_block'          => false,
				'options'              => [
					'top'    => [
						'title' => esc_html__( 'Top', 'oroco' ),
						'icon'  => 'eicon-v-align-top',
					],
					'middle' => [
						'title' => esc_html__( 'Middle', 'oroco' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'bottom' => [
						'title' => esc_html__( 'Bottom', 'oroco' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'selectors'            => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-item.has-thumbnail .cat-text' => 'bottom: {{VALUE}}',
				],
				'selectors_dictionary' => [
					'top'    => 'auto; top: 50px;',
					'middle' => '50%; transform: translate(-50%, 50%)',
					'bottom' => '50px',
				],
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'cat_typography',
				'selector' => '{{WRAPPER}} .oroco-product-categories-masonry .cat-text',
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'selector' => '{{WRAPPER}} .oroco-product-categories-masonry .cat-text',
			]
		);

		$this->add_responsive_control(
			'text_spacing',
			[
				'label'     => __( 'Spacing Bottom', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					]
				],
				'size_units' => [ 'px', 'em', '%' ],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-text' => 'bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'style_switch_color' );

		$this->start_controls_tab( 'switch_color_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'cat_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-text' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'cat_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-text' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'switch_color_hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'cat_bk_color_hover',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-item:hover .cat-text' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'cat_color_hover',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-product-categories-masonry .cat-item:hover .cat-text' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_render_attribute(
			'wrapper', 'class', [
				'oroco-product-categories-masonry woocommerce',
			]
		);

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php $this->get_categories_content( $settings ); ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Get Categories
	 */
	protected function get_categories_content( $settings ) {
		$source = $settings['cats_source'];

		$term_args = [
			'taxonomy' => 'product_cat',
		];

		if ( $source == 'default' ) {
			$display = $settings['cats_display'];
			$parent  = '';
			if ( $display == 'parent' ) {
				$parent = 0;
			}

			$term_args['orderby'] = $settings['cats_orderby'];
			$term_args['order']   = $settings['cats_order'];
			$term_args['number']  = $settings['cats_number'];
			$term_args['parent']  = $parent;

		} else {
			$cats                 = explode( ',', $settings['cats_selected'] );
			$term_args['slug']    = $cats;
			$term_args['orderby'] = 'slug__in';
		}

		$terms = get_terms( $term_args );

		$this->add_render_attribute( 'slick', 'class', [ 'product-cats' ] );

		$output = [ ];

		if ( ! is_wp_error( $terms ) && $terms ) {
			$output[] = '<ul ' . $this->get_render_attribute_string( 'slick' ) . '>';

			foreach ( $terms as $term ) {
				$thumbnail_id = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );

				$settings['image']['url'] = wp_get_attachment_image_src( $thumbnail_id );
				$settings['image']['id']  = $thumbnail_id;
				$image = Group_Control_Image_Size::get_attachment_image_html( $settings );

				$add_class = $thumbnail_html = '';
				if ($thumbnail_id) {
					$thumbnail_html = '<span class="cat-thumb">' . $image . '</span>';
					$add_class = 'has-thumbnail';
				}

				$output[] = sprintf(
					'<li class="cat-item %s">
						<a href="%s">
							%s
							<span class="cat-text oroco-button oroco-button--regular">
								%s
							</span>
						</a>
					</li>',
					esc_attr( $add_class ),
					esc_url( get_term_link( $term->term_id, 'product_cat' ) ),
					$thumbnail_html,
					esc_html( $term->name )
				);
			}

			$output[] = '</ul>';
		}

		echo implode( '', $output );
	}
}