<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Category Banner widget
 */
class Category_Banner extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-category-banner';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Category Banner', 'oroco' );
	}


	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-featured-image';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	// Tab Content
	protected function section_content() {
		$this->section_content_banner();
	}

	protected function section_content_banner() {
		$this->start_controls_section(
			'section_banner',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$this->add_control(
			'elements',
			[
				'label' => esc_html__( 'Elements', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'banner_content_settings' );

		$this->start_controls_tab( 'content_text', [ 'label' => esc_html__( 'Title', 'oroco' ) ] );

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'oroco' ),
				'type'        => Controls_Manager::TEXTAREA,
				'placeholder' => esc_html__( 'Enter your text', 'oroco' ),
				'label_block' => true,
				'default'     => __( 'This is the title', 'oroco' ),
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'content_btn', [ 'label' => esc_html__( 'Button', 'oroco' ) ] );

		$this->add_control(
			'button_text', [
				'label'         => esc_html__( 'Button Text', 'oroco' ),
				'type'          => Controls_Manager::TEXT,
				'show_external' => true,
				'default'       => esc_html__( 'SHOP NOW', 'oroco' ),
			]
		);

		$this->add_control(
			'button_link', [
				'label'         => esc_html__( 'Link URL', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'mode',
			[
				'label'       => esc_html__( 'Mode', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'lager' 		=> esc_html__( 'Large', 'oroco' ),
					'regular'  		=> esc_html__( 'Regular', 'oroco' ),
					'outline'  		=> esc_html__( 'Outline', 'oroco' ),
					'underlined'  	=> esc_html__( 'Underlined', 'oroco' ),
					'custom'  		=> esc_html__( 'Custom', 'oroco' ),
				],
				'default'     => 'underlined',
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'background', [ 'label' => esc_html__( 'Image', 'oroco' ) ] );

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				'default'   => 'full',
				'separator' => 'none',
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Image', 'oroco' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/1170x500/f5f5f5?text=image',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	// Tab Style
	protected function section_style(){
		$this->section_style_banner();
		$this->section_style_text();
		$this->section_style_button();
	}

	protected function section_style_banner(){
		// Banner
		$this->start_controls_section(
			'section_style_banner',
			[
				'label' => esc_html__( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'content_max_width',
			[
				'label'          => esc_html__( 'Content Width', 'oroco' ),
				'type'           => Controls_Manager::SLIDER,
				'range'          => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units'     => [ '%', 'px' ],
				'default'        => [],
				'tablet_default' => [
					'unit' => '%',
				],
				'mobile_default' => [
					'unit' => '%',
				],
				'selectors'      => [
					'{{WRAPPER}} .oroco-category-banner' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'banner_padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'default'    => [],
				'size_units' => [ 'px', 'em', '%' ],
				'placeholder' => [
					'top' => '40',
					'right' => '50',
					'bottom' => '40',
					'left' => '50',
				],
				'selectors'  => [
					'{{WRAPPER}} .oroco-category-banner .banner-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'horizontal_position',
			[
				'label'                => esc_html__( 'Horizontal Position', 'oroco' ),
				'type'                 => Controls_Manager::CHOOSE,
				'label_block'          => false,
				'options'              => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'eicon-h-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'              => '',
				'selectors'            => [
					'{{WRAPPER}} .oroco-category-banner' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'left'   => 'margin-right: auto',
					'center' => 'margin: 0 auto',
					'right'  => 'margin-left: auto',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_text(){

		// Title
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);	

		$this->start_controls_tabs( 'style_text' );

		$this->start_controls_tab( 'title_text', [ 'label' => esc_html__( 'Title', 'oroco' ) ] );

		$this->add_responsive_control(
			'heading_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units'  => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .main-title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'heading_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .main-title' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_typography',
				'selector' => '{{WRAPPER}} .oroco-category-banner .main-title',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'divider_style', [ 'label' => esc_html__( 'Divider', 'oroco' ) ] );

		$this->add_responsive_control(
			'divider_offset',
			[
				'label'     => esc_html__( 'Vertical Offset', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .banner-image + .banner-content:before' => 'top: {{VALUE}}px;',
				],
			]
		);

		$this->add_responsive_control(
			'divider_height',
			[
				'label'     => esc_html__( 'Height', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units'  => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .banner-image + .banner-content:before' => 'height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'divider_width',
			[
				'label'     => esc_html__( 'Width', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units'  => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .banner-image + .banner-content:before' => 'width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'divider_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .banner-image + .banner-content:before' => 'background-color: {{VALUE}}',

				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_style_button(){
		// Button
		$this->start_controls_section(
			'section_style_button',
			[
				'label' => esc_html__( 'Button', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'btn_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-category-banner .oroco-button--selector' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'btn_typography',
				'selector' => '{{WRAPPER}} .oroco-category-banner .oroco-button--selector',
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'btn_shadow',
				'selector' => '{{WRAPPER}} .oroco-category-banner .oroco-button--selector',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'btn_border',
				'label'    => esc_html__( 'Border', 'oroco' ),
				'selector' => '{{WRAPPER}} .oroco-category-banner .oroco-button--custom',
				'fields_options'  => [
					'border' => [
						'default' => 'none',
					],
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'mode',
							'value' => 'custom',
						]
					]
				],
			]
		);


		$this->add_control(
			'content_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .oroco-button--selector' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-category-banner .oroco-button--selector' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$class = array('oroco-category-banner');


		if ( $settings['button_link']['is_external'] ) {
			$this->add_render_attribute( 'link', 'target', '_blank' );
		}

		if ( $settings['button_link']['nofollow'] ) {
			$this->add_render_attribute( 'link', 'rel', 'nofollow' );
		}

		if ( $settings['button_link']['url'] ) {
			$this->add_render_attribute( 'link', 'href', $settings['button_link']['url'] );
			$this->add_render_attribute( 'only-link', 'href', $settings['button_link']['url'] );
		}

		$classes = [
			$settings['mode'] == 'custom' ? '' : ' oroco-button',
		 	'oroco-button--selector',
		];

		switch ($settings['mode']) {
			case 'regular':
				$classes[] = 'oroco-button--regular';
				break;

			case 'outline':
				$classes[] = 'oroco-button--regular oroco-button--outline';
				break;

			case 'underlined':
				$classes[] = 'oroco-button--regular oroco-button--underlined';
				break;

			case 'custom':
				$classes[] = 'oroco-button--custom';
				break;
			
			default:
				break;
		}

		$title = $settings['title'] ? sprintf('<h2 class="main-title">%s</h2>',$settings['title']) : '';

		$image = Group_Control_Image_Size::get_attachment_image_html( $settings );

		$this->add_render_attribute( 'link', 'class', $classes );

		$this->add_render_attribute( 'wrapper', 'class', $class );

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php if ( ! empty( $image ) ) : ?>
			<div class="banner-image">
				<?php if ( $settings['button_link']['url'] ) : ?>
					<a <?php echo $this->get_render_attribute_string( 'only-link' ); ?>><?php echo $image?></a>
				<?php else: echo $image; ?>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			
			<div class="banner-content">
				<?php echo $title?>
				<?php if ( ! empty( $settings['button_text'] ) ) : ?>
					<div class="button-wrapper"><a <?php echo $this->get_render_attribute_string( 'link' ); ?>><?php echo $settings['button_text']; ?></a></div>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {

	}
}