<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Background;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Image Box widget
 */
class Image_Box extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-image-box';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Image Box', 'oroco' );
	}


	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-image';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	// Tab Content
	protected function section_content() {
		$this->section_content_banner();
	}

	protected function section_content_banner() {
		$this->start_controls_section(
			'section_banner',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$this->add_responsive_control(
			'height',
			[
				'label'     => esc_html__( 'Height', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 100,
						'max' => 600,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-image-box .oroco-image-box__content' => 'height: {{SIZE}}{{UNIT}};',
				]
			]
		);

		$this->add_control(
			'elements',
			[
				'label' => esc_html__( 'Elements', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'banner_content_settings' );

		$this->start_controls_tab( 'content_btn', [ 'label' => esc_html__( 'Button', 'oroco' ) ] );

		$this->add_control(
			'button_text', [
				'label'         => esc_html__( 'Button Text', 'oroco' ),
				'type'          => Controls_Manager::TEXT,
				'show_external' => true,
				'default'       => esc_html__( 'Shop Now', 'oroco' ),
			]
		);

		$this->add_control(
			'button_link', [
				'label'         => esc_html__( 'Link URL', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'mode',
			[
				'label'       => esc_html__( 'Mode', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'lager' 		=> esc_html__( 'Large', 'oroco' ),
					'regular'  		=> esc_html__( 'Regular', 'oroco' ),
					'outline'  		=> esc_html__( 'Outline', 'oroco' ),
					'underlined'  	=> esc_html__( 'Underlined', 'oroco' ),
					'custom'  		=> esc_html__( 'Custom', 'oroco' ),
				],
				'default'     => 'lager',
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'background', [ 'label' => esc_html__( 'Background', 'oroco' ) ] );

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'banners_background',
				'label'    => __( 'Background', 'oroco' ),
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .oroco-image-box .featured-image',
				'fields_options'  => [
					'background' => [
						'default' => 'classic',
					],
					'image' => [
						'default'   => [
							'url' => 'https://via.placeholder.com/1170X240/cccccc?text=600x240+Image',
						],
					],
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	// Tab Style
	protected function section_style(){
		$this->section_style_banner();
		$this->section_style_button();
	}

	protected function section_style_banner(){
		// Banner
		$this->start_controls_section(
			'section_style_banner',
			[
				'label' => esc_html__( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'content_max_width',
			[
				'label'          => esc_html__( 'Content Width', 'oroco' ),
				'type'           => Controls_Manager::SLIDER,
				'range'          => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units'     => [ '%', 'px' ],
				'default'        => [],
				'tablet_default' => [
					'unit' => '%',
				],
				'mobile_default' => [
					'unit' => '%',
				],
				'selectors'      => [
					'{{WRAPPER}} .oroco-image-box .oroco-image-box__content' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'banner_padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'default'    => [],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-image-box .oroco-image-box__content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'banner_horizontal_position',
			[
				'label'        => esc_html__( 'Horizontal Position', 'oroco' ),
				'type'         => Controls_Manager::CHOOSE,
				'label_block'  => false,
				'default'      => 'center',
				'options'      => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'eicon-h-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'prefix_class' => 'oroco--h-position-',
				'separator'    => 'before',
			]
		);

		$this->add_control(
			'banner_vertical_position',
			[
				'label'        => esc_html__( 'Vertical Position', 'oroco' ),
				'type'         => Controls_Manager::CHOOSE,
				'label_block'  => false,
				'default'      => 'middle',
				'options'      => [
					'top'    => [
						'title' => esc_html__( 'Top', 'oroco' ),
						'icon'  => 'eicon-v-align-top',
					],
					'middle' => [
						'title' => esc_html__( 'Middle', 'oroco' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'bottom' => [
						'title' => esc_html__( 'Bottom', 'oroco' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'prefix_class' => 'oroco--v-position-',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_button(){
		// Button
		$this->start_controls_section(
			'section_style_button',
			[
				'label' => esc_html__( 'Button', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-image-box .oroco-button--selector' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .oroco-image-box .oroco-button--selector',
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'selector' => '{{WRAPPER}} .oroco-image-box .oroco-button--selector',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'heading_border',
				'label'    => esc_html__( 'Border', 'oroco' ),
				'selector' => '{{WRAPPER}} .oroco-image-box .oroco-button--custom',
				'fields_options'  => [
					'border' => [
						'default' => 'none',
					],
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'mode',
							'value' => 'custom',
						]
					]
				],
			]
		);

		$this->start_controls_tabs( 'style_switch_color' );

		$this->start_controls_tab( 'switch_color_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'cat_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-image-box .oroco-button--selector' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'cat_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-image-box .oroco-button--selector' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'switch_color_hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'cat_bk_color_hover',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-image-box:hover .oroco-button--selector' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'cat_color_hover',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-image-box:hover .oroco-button--selector' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute( 'wrapper', 'class', [
			'oroco-image-box',
		] );

		if ( $settings['button_link']['is_external'] ) {
			$this->add_render_attribute( 'link', 'target', '_blank' );
		}

		if ( $settings['button_link']['nofollow'] ) {
			$this->add_render_attribute( 'link', 'rel', 'nofollow' );
		}

		if ( $settings['button_link']['url'] ) {
			$this->add_render_attribute( 'link', 'href', $settings['button_link']['url'] );
		}

		$classes = [
			$settings['mode'] == 'custom' ? '' : ' oroco-button',
			'oroco-button--selector',
		];

		switch ($settings['mode']) {
			case 'regular':
				$classes[] = 'oroco-button--regular';
				break;

			case 'outline':
				$classes[] = 'oroco-button--regular oroco-button--outline';
				break;

			case 'underlined':
				$classes[] = 'oroco-button--regular oroco-button--underlined';
				break;

			case 'custom':
				$classes[] = 'oroco-button--custom';
				break;
			
			default:
				break;
		}

		$this->add_render_attribute( 'link', 'class', $classes );

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="featured-image"></div>
			<?php if ( $settings['button_link']['url'] ) : ?>
				<a class="link" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
			<?php endif; ?>
			<div class="oroco-image-box__content">
				<?php if ( ! empty( $settings['button_text'] ) ) : ?>
					<a <?php echo $this->get_render_attribute_string( 'link' ); ?>><?php echo $settings['button_text']; ?></a>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {

	}
}