<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Controls_Stack;
use OrocoAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Instagram_Grid widget
 */
class Instagram_Grid extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-instagram-grid';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Instagram Grid', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-instagram-gallery';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$this->add_control(
			'access_token',
			[
				'label'       => esc_html__( 'Access Token', 'oroco' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => esc_html__( 'Enter your access token', 'oroco' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'content_type',
			[
				'label'     => esc_html__( 'Content Type ', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Block', 'oroco' ),
					'flex' => esc_html__( 'Flex', 'oroco' ),
				],
				'default'   => 'block',
				'selectors' => [
					'{{WRAPPER}} .oroco-instagram-grid .instagram-wrapper' => 'display: {{VALUE}};',
				],
			]
		);


		$this->add_control(
			'limit',
			[
				'label'       => esc_html__( 'Limit', 'oroco' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 9,
				'label_block' => true,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label'   => esc_html__( 'Columns', 'farmart' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 3,
				'prefix_class' => 'columns-%s',
				'condition'   => [
					'content_type' => 'flex',
				],
			]
		);

		$this->add_responsive_control(
			'img_width',
			[
				'label'     => esc_html__( 'Image Width', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 1,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-instagram-grid .instagram-item' => 'max-width: {{SIZE}}{{UNIT}};',
				],
				'condition'   => [
					'content_type' => 'block',
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */

	protected function section_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'item_padding',
			[
				'label'      => esc_html__( 'Padding Item', 'farmart' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-instagram-grid .instagram-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],

			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'oroco-instagram-grid',
				'oroco-instagram-grid--'.$settings['content_type'],
			]
		);

		$output    = [];
		$instagram = Elementor::instagram_get_photos_by_token( $settings['limit'],$settings['access_token'] );

		if ( is_wp_error( $instagram ) ) {
			return $instagram->get_error_message();
		}

		$count = 1;

		$output[] = sprintf('<ul class="instagram-wrapper">');

		foreach ( $instagram as $data ) {

			if ( $count > intval( $settings['limit'] ) ) {
				break;
			}

			$output[] = '<li class="instagram-item"><a target="_blank" href="' . esc_url( $data['link'] ) . '"><img src="' . esc_url( $data['images']['thumbnail'] ) . '" alt="' . esc_attr( $data['caption'] ) . '"></a></li>';

			$count ++;
		}
		$output[] = sprintf('</ul>');

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);

	}
}