<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Controls_Stack;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Links List widget
 */
class Links_List extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-links-list';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Links List', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-animation-text';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'oroco' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the title', 'oroco' ),
				'placeholder' => esc_html__( 'Enter your title', 'oroco' ),
				'separator' => 'after',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'name',
			[
				'label'       => esc_html__( 'Name', 'oroco' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'tag', 'oroco' ),
				'placeholder' => esc_html__( 'Enter your name link', 'oroco' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'elements',
			[
				'label'         => esc_html__( 'Links Group', 'oroco' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'name' => __( 'name', 'oroco' ),
					],[
						'name' => __( 'name 2', 'oroco' ),
					],[
						'name' => __( 'name 3', 'oroco' ),
					],[
						'name' => __( 'name 4', 'oroco' ),
					],[
						'name' => __( 'name 5', 'oroco' ),
					],[
						'name' => __( 'name 6', 'oroco' ),
					],[
						'name' => __( 'name 7', 'oroco' ),
					],
				],
				'title_field'   => '{{{ name }}}',
				'prevent_empty' => false,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */

	protected function section_style() {
		$this->section_style_content();
		$this->section_style_title();
		$this->section_style_name();
	}

	protected function section_style_name(){
		// Item
		$this->start_controls_section(
			'section_item_style',
			[
				'label' =>esc_html__( 'Link Name', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'tag_typography',
				'selector' => '{{WRAPPER}} .oroco-links-list .link-name',
				'separator'  => 'before',
			]
		);
		
		$this->start_controls_tabs( 'tag_content_settings' );

		$this->start_controls_tab( 'tab_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'name_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-links-list .link-name' => 'color: {{VALUE}}',
				],
			]
		);

		$this ->end_controls_tab();

		$this->start_controls_tab( 'tab_hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'name_color_hover',
			[
				'label'     => esc_html__( 'Text Color Hover', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-links-list .link-name:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_style_title(){
		// Title
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-links-list .oroco-links-list__title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'heading_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-links-list .oroco-links-list__title' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_typography',
				'selector' => '{{WRAPPER}} .oroco-links-list .oroco-links-list__title',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_content() {
		$this->start_controls_section(
			'section_content_style',
			[
				'label' =>esc_html__( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'name_spacing',
			[
				'label'      => __( 'Item Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-links-list .link-item' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
				'separator'  => 'after',
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label'      =>esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-links-list' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_margin',
			[
				'label'      =>esc_html__( 'Margin', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-links-list' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'oroco-links-list',
			]
		);



		$els = $settings['elements'];

		$output = [ ];

		$output [] = $settings['title'] ? sprintf( '<h5 class="oroco-links-list__title">%s</h5>', $settings['title'] ) : '';

		if ( ! empty ( $els ) ) {

			$output[] = sprintf('<ul class="oroco-links-list__inner">');

			foreach ( $els as $index => $item ) {
				$name    = $item['name'] ? sprintf('<span class="name">%s</span>',$item['name']) : '';

				$link_key = 'link_' . $index;
				$tag_html = $item['link']['url'] ? $this->get_link_control( $link_key, $item['link'], $item['name'], [ 'class' => 'link-name' ] ) : $name;

				$output[] = '<li class="link-item">' . $tag_html . '</li>';

			}
			$output[] = sprintf('</ul>');

		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);

	}

	protected function get_link_control( $link_key, $url, $content, $attr = [] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}