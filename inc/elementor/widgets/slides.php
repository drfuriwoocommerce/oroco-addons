<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use OrocoAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Slides extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-slides';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Slides', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-post-slider';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	// Tab Content
	protected function section_content() {
		$this->section_content_slides();
		$this->section_content_option();
	}

	protected function section_content_slides() {
		$this->start_controls_section(
			'section_slides',
			[
				'label' => esc_html__( 'Slides', 'oroco' ),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->start_controls_tabs( 'slides_repeater' );

		$repeater->start_controls_tab( 'background', [ 'label' => esc_html__( 'Background', 'oroco' ) ] );

		$repeater->add_control(
			'background_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '#f4f5f5',
				'selectors' => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-bg' => 'background-color: {{VALUE}}',
				],
			]
		);

		$repeater->add_responsive_control(
			'background_image',
			[
				'label'     => _x( 'Image', 'Background Control', 'oroco' ),
				'type'      => Controls_Manager::MEDIA,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-bg' => 'background-image: url({{URL}})',
				],
			]
		);

		$repeater->add_responsive_control(
			'background_size',
			[
				'label'     => _x( 'Background Size', 'Background Control', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'cover',
				'options'   => [
					'cover'   => _x( 'Cover', 'Background Control', 'oroco' ),
					'contain' => _x( 'Contain', 'Background Control', 'oroco' ),
					'auto'    => _x( 'Auto', 'Background Control', 'oroco' ),
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-bg' => 'background-size: {{VALUE}}',
				],
			]
		);

		$repeater->add_responsive_control(
			'background_position',
			[
				'label'     => esc_html__( 'Background Position', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''              => esc_html__( 'Default', 'oroco' ),
					'left top'      => esc_html__( 'Left Top', 'oroco' ),
					'left center'   => esc_html__( 'Left Center', 'oroco' ),
					'left bottom'   => esc_html__( 'Left Bottom', 'oroco' ),
					'right top'     => esc_html__( 'Right Top', 'oroco' ),
					'right center'  => esc_html__( 'Right Center', 'oroco' ),
					'right bottom'  => esc_html__( 'Right Bottom', 'oroco' ),
					'center top'    => esc_html__( 'Center Top', 'oroco' ),
					'center center' => esc_html__( 'Center Center', 'oroco' ),
					'center bottom' => esc_html__( 'Center Bottom', 'oroco' ),
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-bg' => 'background-position: {{VALUE}};',
				],

			]
		);

		$repeater->add_responsive_control(
			'background_position_xy',
			[
				'label'              => esc_html__( 'Custom Background Position', 'oroco' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'left' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-bg' => 'background-position: {{LEFT}}{{UNIT}} {{TOP}}{{UNIT}};',
				],
			]
		);

		$repeater->add_control(
			'background_ken_burns',
			[
				'label'      => esc_html__( 'Ken Burns Effect', 'oroco' ),
				'type'       => Controls_Manager::SWITCHER,
				'default'    => '',
				'separator'  => 'before',
				'conditions' => [
					'terms' => [
						[
							'name'     => 'background_image[url]',
							'operator' => '!=',
							'value'    => '',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'zoom_direction',
			[
				'label'      => esc_html__( 'Zoom Direction', 'oroco' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'in',
				'options'    => [
					'in'  => esc_html__( 'In', 'oroco' ),
					'out' => esc_html__( 'Out', 'oroco' ),
				],
				'conditions' => [
					'terms' => [
						[
							'name'     => 'background_ken_burns',
							'operator' => '!=',
							'value'    => '',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'background_overlay',
			[
				'label'      => esc_html__( 'Background Overlay', 'oroco' ),
				'type'       => Controls_Manager::SWITCHER,
				'default'    => '',
				'separator'  => 'before',
				'conditions' => [
					'terms' => [
						[
							'name'     => 'background_image[url]',
							'operator' => '!=',
							'value'    => '',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'background_overlay_color',
			[
				'label'      => esc_html__( 'Color', 'oroco' ),
				'type'       => Controls_Manager::COLOR,
				'default'    => 'rgba(0,0,0,0.5)',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'background_overlay',
							'value' => 'yes',
						],
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-background-overlay' => 'background-color: {{VALUE}}',
				],
			]
		);

		$repeater->add_control(
			'background_overlay_blend_mode',
			[
				'label'      => esc_html__( 'Blend Mode', 'oroco' ),
				'type'       => Controls_Manager::SELECT,
				'options'    => [
					''            => esc_html__( 'Normal', 'oroco' ),
					'multiply'    => 'Multiply',
					'screen'      => 'Screen',
					'overlay'     => 'Overlay',
					'darken'      => 'Darken',
					'lighten'     => 'Lighten',
					'color-dodge' => 'Color Dodge',
					'color-burn'  => 'Color Burn',
					'hue'         => 'Hue',
					'saturation'  => 'Saturation',
					'color'       => 'Color',
					'exclusion'   => 'Exclusion',
					'luminosity'  => 'Luminosity',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'background_overlay',
							'value' => 'yes',
						],
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-background-overlay' => 'mix-blend-mode: {{VALUE}}',
				],
			]
		);

		$repeater->end_controls_tab();

		$repeater->start_controls_tab( 'content', [ 'label' => esc_html__( 'Content', 'oroco' ) ] );

		$repeater->add_control(
			'heading',
			[
				'label'       => esc_html__( 'Title', 'oroco' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => esc_html__( 'Slide Heading', 'oroco' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'subtitle',
			[
				'label'       => esc_html__( 'Subtitle', 'oroco' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => esc_html__( 'Slide Subtitle', 'oroco' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'description',
			[
				'label'   => esc_html__( 'Description', 'oroco' ),
				'type'    => Controls_Manager::WYSIWYG,
				'default' => esc_html__( 'I am slide content. Click edit button to change this text. 
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'oroco' ),
			]
		);

		$repeater->add_control(
			'button_text',
			[
				'label'   => esc_html__( 'Button Text', 'oroco' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Click Here', 'oroco' ),
			]
		);

		$repeater->add_control(
			'link',
			[
				'label'       => esc_html__( 'Link', 'oroco' ),
				'type'        => Controls_Manager::URL,
				'placeholder' => esc_html__( 'https://your-link.com', 'oroco' ),
				'default'     => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$repeater->add_control(
			'link_click',
			[
				'label'      => esc_html__( 'Apply Link On', 'oroco' ),
				'type'       => Controls_Manager::SELECT,
				'options'    => [
					'button' => esc_html__( 'Button Only', 'oroco' ),
					'slide'  => esc_html__( 'Whole Slide', 'oroco' ),
				],
				'default'    => 'button',
				'conditions' => [
					'terms' => [
						[
							'name'     => 'link[url]',
							'operator' => '!=',
							'value'    => '',
						],
					],
				],
			]
		);

		$repeater->end_controls_tab();

		$repeater->start_controls_tab( 'price_box', [ 'label' => esc_html__( 'Price', 'oroco' ) ] );
		$control = apply_filters( 'oroco_slider_section_number', 3 );
		for ( $i = 1; $i <= $control; $i ++ ) {
			$repeater->add_control(
				'price_box_heading_' . $i,
				[
					'label' => __( 'Price Item ' . $i, 'oroco' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$repeater->add_control(
				'price_box_enable_' . $i,
				[
					'label'       => esc_html__( 'Enable', 'oroco' ),
					'type'        => Controls_Manager::SWITCHER,
					'description' => esc_html__( 'Show dots product in slider', 'oroco' ),
					'default'      => 'no',
					'return_value' => 'yes',
					'selectors_dictionary' => [
						'no'   => 'display: none',
						'yes'   => 'display: block',
					],
					'selectors'            => [
						'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-slide-price-box' => '{{VALUE}}',
					],
				]
			);

			$repeater->add_control(
				'price_box_text_' . $i,
				[
					'label'       => esc_html__( 'Title', 'oroco' ),
					'type'        => Controls_Manager::TEXT,
					'default'     => sprintf( '%s %s', esc_html__( 'Title ', 'oroco' ), $i ),
					'label_block' => true,
					'conditions' => [
						'terms' => [
							[
								'name'  => 'price_box_enable_' . $i,
								'value' => 'yes',
							],
						],
					],
				]
			);

			$repeater->add_control(
				'price_box_product_ids_' . $i,
				[
					'label'       => esc_html__( 'Products', 'oroco' ),
					'placeholder' => esc_html__( 'Click here and start typing...', 'oroco' ),
					'type'        => 'orautocomplete',
					'default'     => '',
					'label_block' => true,
					'multiple'    => false,
					'source'      => 'product',
					'sortable'    => true,
					'conditions' => [
						'terms' => [
							[
								'name'  => 'price_box_enable_' . $i,
								'value' => 'yes',
							],
						],
					],
				]
			);

			$repeater->add_responsive_control(
				'price_box_position_x_'. $i,
				[
					'label'          => esc_html__( 'Items Position X', 'oroco' ),
					'type'           => Controls_Manager::SLIDER,
					'range'          => [
						'px' => [
							'min' => 0,
							'max' => 1000,
						],
						'%'  => [
							'min' => 0,
							'max' => 100,
						],
					],
					'size_units'     => [ '%', 'px' ],
					'selectors'      => [
						'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-price-box-item.item-'. $i . ' .text' => 'left: {{SIZE}}{{UNIT}};',
					],
					'conditions' => [
						'terms' => [
							[
								'name'  => 'price_box_enable_' . $i,
								'value' => 'yes',
							],
						],
					],
					'separator'  => 'before',
				]
			);

			$repeater->add_responsive_control(
				'price_box_position_y_'. $i,
				[
					'label'          => esc_html__( 'Items Position Y', 'oroco' ),
					'type'           => Controls_Manager::SLIDER,
					'range'          => [
						'px' => [
							'min' => 0,
							'max' => 1000,
						],
						'%'  => [
							'min' => 0,
							'max' => 100,
						],
					],
					'size_units'     => [ '%', 'px' ],
					'selectors'      => [
						'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-price-box-item.item-'. $i . ' .text' => 'top: {{SIZE}}{{UNIT}};',
					],
					'conditions' => [
						'terms' => [
							[
								'name'  => 'price_box_enable_' . $i,
								'value' => 'yes',
							],
						],
					],
				]
			);

			$repeater->add_responsive_control(
				'price_box_content_position_x_'. $i,
				[
					'label'          => esc_html__( 'Content Position X', 'oroco' ),
					'type'           => Controls_Manager::SLIDER,
					'range'          => [
						'px' => [
							'min' => 0,
							'max' => 1000,
						],
						'%'  => [
							'min' => 0,
							'max' => 100,
						],
					],
					'size_units'     => [ '%', 'px' ],
					'selectors'      => [
						'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-price-box-item.item-'. $i . ' .product-item' => 'left: {{SIZE}}{{UNIT}};',
					],
					'conditions' => [
						'terms' => [
							[
								'name'  => 'price_box_enable_' . $i,
								'value' => 'yes',
							],
						],
					],
					'separator'  => 'before',
				]
			);

			$repeater->add_responsive_control(
				'price_box_content_position_y_'. $i,
				[
					'label'          => esc_html__( 'Content Position Y', 'oroco' ),
					'type'           => Controls_Manager::SLIDER,
					'range'          => [
						'px' => [
							'min' => 0,
							'max' => 1000,
						],
						'%'  => [
							'min' => 0,
							'max' => 100,
						],
					],
					'size_units'     => [ '%', 'px' ],
					'selectors'      => [
						'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-price-box-item.item-'. $i . ' .product-item' => 'top: calc({{SIZE}}{{UNIT}})',
					],
					'conditions' => [
						'terms' => [
							[
								'name'  => 'price_box_enable_' . $i,
								'value' => 'yes',
							],
						],
					],
				]
			);
		}
		
		$repeater->end_controls_tab();

		$repeater->start_controls_tab( 'style', [ 'label' => esc_html__( 'Style', 'oroco' ) ] );

		$repeater->add_control(
			'custom_style',
			[
				'label'       => esc_html__( 'Custom', 'oroco' ),
				'type'        => Controls_Manager::SWITCHER,
				'description' => esc_html__( 'Set custom style that will only affect this specific slide.', 'oroco' ),
			]
		);

		$repeater->add_responsive_control(
			'custom_style_max_width',
			[
				'label'          => esc_html__( 'Content Width', 'oroco' ),
				'type'           => Controls_Manager::SLIDER,
				'range'          => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units'     => [ '%', 'px' ],
				'default'        => [
					'size' => '66',
					'unit' => '%',
				],
				'tablet_default' => [
					'unit' => '%',
				],
				'mobile_default' => [
					'unit' => '%',
				],
				'selectors'      => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-slide-content' => 'max-width: {{SIZE}}{{UNIT}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'slide_margin',
			[
				'label'      => esc_html__( 'Margin', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-slide-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'slide_padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .oroco-slide-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'horizontal_position',
			[
				'label'                => esc_html__( 'Horizontal Position', 'oroco' ),
				'type'                 => Controls_Manager::CHOOSE,
				'label_block'          => false,
				'options'              => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'eicon-h-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'              => 'left',
				'selectors'            => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-content' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'left'   => 'margin-right: auto',
					'center' => 'margin: 0 auto',
					'right'  => 'margin-left: auto',
				],
				'conditions'           => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'vertical_position',
			[
				'label'                => esc_html__( 'Vertical Position', 'oroco' ),
				'type'                 => Controls_Manager::CHOOSE,
				'label_block'          => false,
				'options'              => [
					'top'    => [
						'title' => esc_html__( 'Top', 'oroco' ),
						'icon'  => 'eicon-v-align-top',
					],
					'middle' => [
						'title' => esc_html__( 'Middle', 'oroco' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'bottom' => [
						'title' => esc_html__( 'Bottom', 'oroco' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'selectors'            => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner' => 'align-items: {{VALUE}}',
				],
				'selectors_dictionary' => [
					'top'    => 'flex-start',
					'middle' => 'center',
					'bottom' => 'flex-end',
				],
				'conditions'           => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'text_align',
			[
				'label'       => esc_html__( 'Text Align', 'oroco' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'selectors'   => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner' => 'text-align: {{VALUE}}',
				],
				'conditions'  => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'heading_custom_color',
			[
				'label'      => esc_html__( 'Heading Color', 'oroco' ),
				'type'       => Controls_Manager::COLOR,
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-heading' => 'color: {{VALUE}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
				'separator'  => 'before',
			]
		);

		$repeater->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_custom_typography',
				'selector' => '{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-heading',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'heading_custom_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-heading:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'subtitle_custom_color',
			[
				'label'      => esc_html__( 'Subtitle Color', 'oroco' ),
				'type'       => Controls_Manager::COLOR,
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-subtitle' => 'color: {{VALUE}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
				'separator'  => 'before',
			]
		);

		$repeater->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'subtitle_custom_typography',
				'selector' => '{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-subtitle',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'subtitle_custom_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-subtitle:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'content_custom_color',
			[
				'label'      => esc_html__( 'Content Color', 'oroco' ),
				'type'       => Controls_Manager::COLOR,
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-description' => 'color: {{VALUE}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
				'separator'  => 'before',
			]
		);

		$repeater->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'content_custom_typography',
				'selector' => '{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-description',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_responsive_control(
			'content_custom_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-description:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->add_control(
			'button_custom_color',
			[
				'label'      => esc_html__( 'Button Color', 'oroco' ),
				'type'       => Controls_Manager::COLOR,
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-button' => 'color: {{VALUE}}; border-color: {{VALUE}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
				'separator'  => 'before',
			]
		);

		$repeater->add_control(
			'button_custom_bg_color',
			[
				'label'      => esc_html__( 'Button Background Color', 'oroco' ),
				'type'       => Controls_Manager::COLOR,
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides {{CURRENT_ITEM}} .slick-slide-inner .oroco-slide-button' => 'background-color: {{VALUE}};',
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'custom_style',
							'value' => 'yes',
						],
					],
				],
			]
		);

		$repeater->end_controls_tab();

		$repeater->end_controls_tabs();

		$this->add_control(
			'slides',
			[
				'label'      => esc_html__( 'Slides', 'oroco' ),
				'type'       => Controls_Manager::REPEATER,
				'show_label' => true,
				'fields'     => $repeater->get_controls(),
				'default'    => [
					[
						'subtitle'         => esc_html__( 'Slide 1 Subtitle', 'oroco' ),
						'heading'          => esc_html__( 'Slide 1 Heading', 'oroco' ),
						'description'      => esc_html__( 'Click edit button to change this text. Lorem ipsum dolor sit amet consectetur adipiscing elit dolor', 'oroco' ),
						'button_text'      => esc_html__( 'Click Here', 'oroco' ),
						'background_color' => '#f4f5f5',
					],
					[
						'subtitle'         => esc_html__( 'Slide 2 Subtitle', 'oroco' ),
						'heading'          => esc_html__( 'Slide 2 Heading', 'oroco' ),
						'description'      => esc_html__( 'Click edit button to change this text. Lorem ipsum dolor sit amet consectetur adipiscing elit dolor', 'oroco' ),
						'button_text'      => esc_html__( 'Click Here', 'oroco' ),
						'background_color' => '#f4f5f5',
					],
					[
						'subtitle'         => esc_html__( 'Slide 3 Subtitle', 'oroco' ),
						'heading'          => esc_html__( 'Slide 3 Heading', 'oroco' ),
						'description'      => esc_html__( 'Click edit button to change this text. Lorem ipsum dolor sit amet consectetur adipiscing elit dolor', 'oroco' ),
						'button_text'      => esc_html__( 'Click Here', 'oroco' ),
						'background_color' => '#f4f5f5',
					],
				],
			]
		);

		$this->add_responsive_control(
			'slides_height',
			[
				'label'      => esc_html__( 'Height', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 100,
						'max' => 1000,
					],
					'vh' => [
						'min' => 10,
						'max' => 100,
					],
				],
				'default'    => [
					'size' => 400,
				],
				'size_units' => [ 'px', 'vh', 'em' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-slide' => 'height: {{SIZE}}{{UNIT}};',
				],
				'separator'  => 'before',
			]
		);

		$this->end_controls_section();
	}

	protected function section_content_option() {
		$this->start_controls_section(
			'section_slider_options',
			[
				'label' => esc_html__( 'Slider Options', 'oroco' ),
				'type'  => Controls_Manager::SECTION,
			]
		);

		$this->add_responsive_control(
			'navigation',
			[
				'label'           => esc_html__( 'Navigation', 'oroco' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					'both'   => esc_html__( 'Arrows and Dots', 'oroco' ),
					'arrows' => esc_html__( 'Arrows', 'oroco' ),
					'dots'   => esc_html__( 'Dots', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'desktop_default' => 'both',
				'tablet_default'  => 'dots',
				'mobile_default'  => 'dots',
			]
		);

		$this->add_control(
			'pause_on_hover',
			[
				'label'   => esc_html__( 'Pause on Hover', 'oroco' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'   => esc_html__( 'Autoplay', 'oroco' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'     => esc_html__( 'Autoplay Speed', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 5000,
				'condition' => [
					'autoplay' => 'yes',
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-slide-bg' => 'animation-duration: calc({{VALUE}}ms*1.2); transition-duration: calc({{VALUE}}ms)',
				],
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'   => esc_html__( 'Infinite Loop', 'oroco' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'transition',
			[
				'label'   => esc_html__( 'Transition', 'oroco' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'slide',
				'options' => [
					'slide' => esc_html__( 'Slide', 'oroco' ),
					'fade'  => esc_html__( 'Fade', 'oroco' ),
				],
			]
		);

		$this->add_control(
			'transition_speed',
			[
				'label'   => esc_html__( 'Transition Speed', 'oroco' ) . ' (ms)',
				'type'    => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);

		$this->add_control(
			'content_animation',
			[
				'label'   => esc_html__( 'Content Animation', 'oroco' ),
				'type'    => Controls_Manager::SELECT2,
				'default' => 'fadeInUp',
				'options' => [
					''                  => esc_html__( 'None', 'oroco' ),
					'fadeIn'            => esc_html__( 'Fade In', 'oroco' ),
					'fadeInDown'        => esc_html__( 'Fade In Down', 'oroco' ),
					'fadeInUp'          => esc_html__( 'Fade In Up', 'oroco' ),
					'fadeInRight'       => esc_html__( 'Fade In Right', 'oroco' ),
					'fadeInLeft'        => esc_html__( 'Fade In Left', 'oroco' ),
					'zoomIn'            => esc_html__( 'Zoom', 'oroco' ),
					'zoomInDown'        => esc_html__( 'Zoom In Down', 'oroco' ),
					'zoomInLeft'        => esc_html__( 'Zoom In Left', 'oroco' ),
					'zoomInRight'       => esc_html__( 'Zoom In Righ', 'oroco' ),
					'zoomInUp'          => esc_html__( 'Zoom In Up', 'oroco' ),
					'slideInDown'       => esc_html__( 'Slide In Down', 'oroco' ),
					'slideInLeft'       => esc_html__( 'Slide In Left', 'oroco' ),
					'slideInRight'      => esc_html__( 'Slide In Right', 'oroco' ),
					'slideInUp'         => esc_html__( 'Slide In Up', 'oroco' ),
					'rotateIn'          => esc_html__( 'Rotate In', 'oroco' ),
					'rotateInDownLeft'  => esc_html__( 'Rotate In Down Left', 'oroco' ),
					'rotateInDownRight' => esc_html__( 'Rotate In Down Right', 'oroco' ),
					'rotateInUpLeft'    => esc_html__( 'Rotate In Up Left', 'oroco' ),
					'rotateInUpRight'   => esc_html__( 'Rotate In Up Right', 'oroco' ),
					'bounce'            => esc_html__( 'Bounce', 'oroco' ),
					'flash'             => esc_html__( 'Flash', 'oroco' ),
					'pulse'             => esc_html__( 'Pulse', 'oroco' ),
					'rubberBand'        => esc_html__( 'Rubber Band', 'oroco' ),
					'shake'             => esc_html__( 'Shake', 'oroco' ),
					'headShake'         => esc_html__( 'Head Shake', 'oroco' ),
					'swing'             => esc_html__( 'Swing', 'oroco' ),
					'tada'              => esc_html__( 'Tada', 'oroco' ),
					'wobble'            => esc_html__( 'Wobble', 'oroco' ),
					'jello'             => esc_html__( 'Jello', 'oroco' ),
					'lightSpeedIn'      => esc_html__( 'Light Speed In', 'oroco' ),
					'rollIn'            => esc_html__( 'Roll In', 'oroco' ),
				],
			]
		);

		$this->end_controls_section();

	}

	// Tab Style
	protected function section_style() {
		$this->section_style_slides();
		$this->section_style_title();
		$this->section_style_subtitle();
		$this->section_style_desc();
		$this->section_style_button();
		$this->section_style_box();
		$this->section_style_arrow();
		$this->section_style_dot();
	}

	protected function section_style_slides() {
		$this->start_controls_section(
			'section_style_slides',
			[
				'label' => esc_html__( 'Slides', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'content_max_width',
			[
				'label'          => esc_html__( 'Content Width', 'oroco' ),
				'type'           => Controls_Manager::SLIDER,
				'range'          => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units'     => [ '%', 'px' ],
				'default'        => [
					'size' => '66',
					'unit' => '%',
				],
				'tablet_default' => [
					'unit' => '%',
				],
				'mobile_default' => [
					'unit' => '%',
				],
				'selectors'      => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-content' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'slides_margin',
			[
				'label'      => esc_html__( 'Margin', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'slides_padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'banner_border',
				'label'     => esc_html__( 'Border', 'oroco' ),
				'selector'  => '{{WRAPPER}} .oroco-slides',
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'slides_horizontal_position',
			[
				'label'           => __( 'Horizontal Position', 'oroco' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'      => [
					'flex-start'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'eicon-h-align-center',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'desktop_default' => '',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner' => 'justify-content: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_position',
			[
				'label'           => __( 'Vertical Align', 'oroco' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'      => [
					'flex-start'    => [
						'title' => esc_html__( 'Top', 'oroco' ),
						'icon'  => 'eicon-v-align-top',
					],
					'center' => [
						'title' => esc_html__( 'Middle', 'oroco' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'flex-end' => [
						'title' => esc_html__( 'Bottom', 'oroco' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'desktop_default' => '',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner' => 'align-items: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'slides_text_align',
			[
				'label'       => esc_html__( 'Text Align', 'oroco' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'     => 'left',
				'selectors'   => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'mobile_version',
			[
				'label'     => __( 'Mobile Version', 'oroco' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'oroco' ),
				'label_on'  => __( 'On', 'oroco' ),
				'default'   => 'no',
				'separator' => 'before',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_subtitle() {
		// Subtitle
		$this->start_controls_section(
			'section_style_subtitle',
			[
				'label' => esc_html__( 'Subtitle', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'subtitle_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-subtitle:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'subtitle_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-subtitle' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'subtitle_typography',
				'selector' => '{{WRAPPER}} .oroco-slide-subtitle',
			]
		);

		$this->end_controls_section();

	}

	protected function section_style_title() {
		// Title
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-heading:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'heading_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-heading' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_typography',
				'selector' => '{{WRAPPER}} .oroco-slides .oroco-slide-heading',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_desc() {
		// Description
		$this->start_controls_section(
			'section_style_description',
			[
				'label' => esc_html__( 'Description', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'description_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-description:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'description_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-description' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_typography',
				'selector' => '{{WRAPPER}} .oroco-slides .oroco-slide-description',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_button() {
		$this->start_controls_section(
			'section_style_button',
			[
				'label' => esc_html__( 'Button', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'button_typography',
				'selector' => '{{WRAPPER}} .oroco-slides .oroco-slide-button',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'slide_button_border',
				'label'     => esc_html__( 'Border', 'oroco' ),
				'selector'  => '{{WRAPPER}} .oroco-slides .oroco-slide-button',
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'button_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'button_tabs' );

		$this->start_controls_tab( 'normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'button_text_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button' => 'color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-icon'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-icon svg'     => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'button_hover_text_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:hover'                    => 'color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:focus'                    => 'color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:hover .oroco-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:hover .oroco-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:hover' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:focus' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:hover' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides .oroco-slide-button:focus' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_style_box() {
		// Price Box
		$this->start_controls_section(
			'section_style_price_box',
			[
				'label' => esc_html__( 'Price Box', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'price_box_width',
			[
				'label'      => esc_html__( 'Width', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 50,
						'max' => 200,
					],
				],
				'default'    => [ ],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .text' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'price_box_height',
			[
				'label'      => esc_html__( 'Height', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 50,
						'max' => 200,
					],
				],
				'default'    => [ ],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .text' => 'height: {{SIZE}}{{UNIT}};',
				],
				'separator'  => 'after'
			]
		);

		$position = [
			'top'  => esc_html__( 'Vertical', 'oroco' ),
			'left' => esc_html__( 'Horizontal', 'oroco' ),
		];

		$this->add_control(
			'heading_price_box',
			[
				'label' => esc_html__( 'Position', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		foreach ( $position as $key => $label ) {
			$this->add_responsive_control(
				'price_box_' . $key,
				[
					'label'      => $label,
					'type'       => Controls_Manager::SLIDER,
					'size_units' => [ '%', 'px' ],
					'range'      => [
						'%'  => [
							'min' => 0,
							'max' => 100,
						],
						'px' => [
							'min' => 0,
							'max' => 1170,
						],
					],
					'default'    => [ ],
					'selectors'  => [
						'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .text' => "$key: {{SIZE}}{{UNIT}};",
						'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .product-item ' => "$key: {{SIZE}}{{UNIT}};",
					],
				]
			);
		}

		$this->add_control(
			'price_box_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .text' => 'color: {{VALUE}}',
				],
				'separator' => 'before'
			]
		);

		$this->add_control(
			'price_box_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .text' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'price_box_typography',
				'selector' => '{{WRAPPER}} .oroco-slides .slick-slide-inner .oroco-slide-price-box .text',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_arrow() {
		// Arrows
		$this->start_controls_section(
			'section_style_arrows',
			[
				'label' => esc_html__( 'Arrows', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label'   => esc_html__( 'Position', 'oroco' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'inside'  => esc_html__( 'Inside Container', 'oroco' ),
					'outside' => esc_html__( 'Outside Container', 'oroco' ),
				],
				'default' => 'inside',
				'toggle'  => false,
			]
		);

		$this->add_responsive_control(
			'arrows_spacing_horizontal',
			[
				'label'      => esc_html__( 'Spacing Horizontal', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-prev-arrow' => 'left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .oroco-slides-wrapper .slick-next-arrow' => 'right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'arrows_spacing_vertical',
			[
				'label'      => esc_html__( 'Spacing Vertical', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-prev-arrow' => 'bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .oroco-slides-wrapper .slick-next-arrow' => 'bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'arrow_text_color',
			[
				'label'     => esc_html__( 'Text Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-prev-arrow .text, .oroco-slides-wrapper .slick-next-arrow .text' => 'color: {{VALUE}}',
				],
				'separator'  => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'arrow_text_typography',
				'selector' => '{{WRAPPER}} .oroco-slides-wrapper .slick-prev-arrow .text, .oroco-slides-wrapper .slick-next-arrow .text',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_dot() {
		// Dots
		$this->start_controls_section(
			'section_style_dots',
			[
				'label' => esc_html__( 'Dots', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'dots_type',
			[
				'label'           => __( 'Type', 'oroco' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'   => [
					'horizontal'   => [
						'title' => esc_html__( 'Horizontal', 'oroco' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
					'vertical' => [
						'title' => esc_html__( 'Vertical', 'oroco' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
				],
				'desktop_default' => 'horizontal',
				'tablet_default'  => 'horizontal',
				'mobile_default'  => 'horizontal',
			]
		);

		$this->add_responsive_control(
			'dots_position',
			[
				'label'           => __( 'Position', 'oroco' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'desktop_default' => 'center',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .oroco-slides .slick-dots' => 'text-align: {{VALUE}}',
				],
				'condition'  => [
					'dots_type' => 'horizontal',
				],
			]
		);

		$this->add_responsive_control(
			'dots_vertical_position',
			[
				'label'           => __( 'Position', 'oroco' ),
				'type'            => Controls_Manager::CHOOSE,
				'options'   => [
					'flex-start'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'desktop_default' => 'center',
				'tablet_default'  => 'center',
				'mobile_default'  => 'center',
				'selectors'       => [
					'{{WRAPPER}} .oroco-slides-wrapper.dots-type-vertical .slick-dots' => 'justify-content: {{VALUE}}',
				],
				'condition'  => [
					'dots_type' => 'vertical',
				],
			]
		);

		$this->add_responsive_control(
			'dots_vertical_offset',
			[
				'label'      => esc_html__( 'Vertical Offset', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots' => 'bottom:{{SIZE}}{{UNIT}};',
				],
				'condition'  => [
					'dots_type' => 'horizontal',
				],
			]
		);

		$this->add_responsive_control(
			'dots_vertical_offset_type_vertical',
			[
				'label'      => esc_html__( 'Vertical Offset', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots' => 'left:{{SIZE}}{{UNIT}};',
				],
				'condition'  => [
					'dots_type' => 'vertical',
				],
			]
		);

		$this->add_responsive_control(
			'dots_horizontal_offset',
			[
				'label'      => esc_html__( 'Horizontal Offset', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}};',
				],
				'condition'  => [
					'dots_position' => [ 'left', 'right' ],
					'dots_type' => 'horizontal',
				],
			]
		);

		$this->add_responsive_control(
			'dots_horizontal_offset_type_vertical',
			[
				'label'      => esc_html__( 'Horizontal Offset', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}};',
				],
				'condition'  => [
					'dots_type' => 'vertical',
				],
			]
		);

		$this->add_responsive_control(
			'dots_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots li'            => 'padding-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .oroco-slides .slick-dots li:last-child' => 'padding-right: 0;',
				],
				'condition'  => [
					'dots_type' => 'horizontal',
				],
				'separator'  => 'before',
			]
		);

		$this->add_responsive_control(
			'dots_spacing_type_vertical',
			[
				'label'      => esc_html__( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots li'            => 'padding-bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .oroco-slides .slick-dots li:last-child' => 'padding-bottom: 0;',
				],
				'condition'  => [
					'dots_type' => 'vertical',
				],
				'separator'  => 'before',
			]
		);

		$this->add_responsive_control(
			'dots_width',
			[
				'label'      => esc_html__( 'Width', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots li button' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'dots_height',
			[
				'label'      => esc_html__( 'Height', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'size_units' => [ 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-slides .slick-dots li button' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'dots_border_radius',
			[
				'label'     => esc_html__( 'Border Radius', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-slides .slick-dots li button' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'dots_tabs' );

		$this->start_controls_tab( 'dots_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'dots_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li button' => 'background-color: {{VALUE}};',
				],

			]
		);

		$this->add_control(
			'dots_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li button' => 'border-color: {{VALUE}};',
				],

			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'dots_hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'dots_hover_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li button:hover:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li button:hover' => 'background-color: {{VALUE}};',
				],
				'default'   => '',
			]
		);

		$this->add_control(
			'dots_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li button:hover:before' => 'border-color: {{VALUE}};',
				],
				'default'   => '',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'dots_active', [ 'label' => esc_html__( 'Active', 'oroco' ) ] );

		$this->add_control(
			'dots_active_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li.slick-active button:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li.slick-active button' => 'background-color: {{VALUE}};'
				],
				'default'   => '',
			]
		);

		$this->add_control(
			'dots_active_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-slides-wrapper .slick-dots li.slick-active button:before' => 'border-color: {{VALUE}};'
				],
				'default'   => '',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( empty( $settings['slides'] ) ) {
			return;
		}

		$nav = $settings['navigation'];
		$nav_tablet = empty( $settings['navigation_tablet'] ) ? $nav : $settings['navigation_tablet'];
		$nav_mobile = empty( $settings['navigation_mobile'] ) ? $nav : $settings['navigation_mobile'];

		$classes = [
			'oroco-slides-wrapper loading',
			'navigation-' . $nav,
			'navigation-tablet-' . $nav_tablet,
			'navigation-mobile-' . $nav_mobile,
			$settings['dots_type'] == 'vertical' ? 'dots-type-vertical' : '',
			$settings['mobile_version'] == 'yes' ? 'oroco-slide-mobile' : ''
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$this->add_render_attribute( 'button', 'class', [
			'oroco-button',
			'oroco-slide-button'
		] );

		$slides      = [ ];
		$slide_count = 0;

		foreach ( $settings['slides'] as $slide ) {
			$slide_html       = '';
			$btn_attributes   = '';
			$slide_attributes = '';
			$slide_element    = 'div';
			$btn_element      = 'div';
			$slide_url        = $slide['link']['url'];

			if ( ! empty( $slide_url ) ) {
				$this->add_render_attribute( 'slide_link' . $slide_count, 'href', $slide_url );

				if ( $slide['link']['is_external'] ) {
					$this->add_render_attribute( 'slide_link' . $slide_count, 'target', '_blank' );
				}

				if ( 'button' === $slide['link_click'] ) {
					$btn_element    = 'a';
					$btn_attributes = $this->get_render_attribute_string( 'slide_link' . $slide_count );
				} else {
					$slide_element    = 'a';
					$slide_attributes = $this->get_render_attribute_string( 'slide_link' . $slide_count );
				}
			}

			if ( 'yes' === $slide['background_overlay'] ) {
				$slide_html .= '<div class="oroco-background-overlay"></div>';
			}

			$slide_html .= '<div class="oroco-slide-content">';

			if ( $slide['heading'] ) {
				$slide_html .= '<div class="oroco-slide-heading">' . $slide['heading'] . '</div>';
			}

			if ( $slide['subtitle'] ) {
				$slide_html .= '<div class="oroco-slide-subtitle">' . $slide['subtitle'] . '</div>';
			}

			if ( $slide['description'] ) {
				$slide_html .= '<div class="oroco-slide-description">' . $slide['description'] . '</div>';
			}

			if ( $slide['button_text'] ) {
				$slide_html .= '<' . $btn_element . ' ' . $btn_attributes . ' ' . $this->get_render_attribute_string( 'button' ) . '>' . $slide['button_text'] . '</' . $btn_element . '>';
			}

			$ken_class = '';

			if ( '' != $slide['background_ken_burns'] ) {
				$ken_class = ' oroco-ken-' . $slide['zoom_direction'] . ' oroco-ken-' . $slide['zoom_direction'];
			}

			$slide_html .= '</div>';

			$slide_html .= '<div class="oroco-slide-price-box">';

			$control   = apply_filters( 'oroco_slider_section_number', 3 );

			for ( $i = 1; $i <= $control; $i ++ ) {
				
				$price_box_text = $product = '';
	
				if ( $slide["price_box_text_$i"] ) {
					$price_box_text = sprintf( '<span class="text">%s</span>', $slide["price_box_text_$i"] );
				}

				$products_html 	= [ ];
				$product_id 	= $slide["price_box_product_ids_$i"];
				$product 		= new \WC_product($product_id);
				   
				if ( $product_id ) {
					$products_html[] = sprintf( 
						'<div class="product-item">
							<div class="product-image">%s</div>
							<div class="product-bottom">
								<div class="product-name">
								%s
								</div>
								<div class="product-price">
								%s
								</div>
							</div>
							<div class="product-button">
							<a class="oroco-button oroco-slide-button" href="%s">%s%s</a>
							</div>
						</div>', 
						$product->get_image(),
						$product->get_name(),
						$product->get_price_html(),
						get_permalink( $product_id ),
						oroco_get_theme_svg('cart'),
						esc_html('Shop Now', 'oroco')
					);
				}

				$slide_html .= sprintf( 
								'<div class="oroco-price-box-item item-%s">%s %s</div>', 
								esc_attr( $i ), 
								$price_box_text,
								implode( '', $products_html )
							);
			}

			$slide_html .= '</div>';

			$slide_html = '<div class="slick-slide-bg' . $ken_class . '"></div><' . $slide_element . ' ' . $slide_attributes . ' class="slick-slide-inner">' . $slide_html . '</' . $slide_element . '>';
			$slides[]   = '<div class="elementor-repeater-item-' . $slide['_id'] . ' slick-slide">' . $slide_html . '</div>';
			$slide_count ++;
		}

		$is_rtl    = is_rtl();
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$fade      = $settings['transition'] == 'fade' ? true : false;

		$show_dots   = ( in_array( $settings['navigation'], [ 'dots', 'both' ] ) );
		$show_arrows = ( in_array( $settings['navigation'], [ 'arrows', 'both' ] ) );

		$slick_options = [
			'slidesToShow'  => absint( 1 ),
			'autoplaySpeed' => absint( $settings['autoplay_speed'] ),
			'autoplay'      => ( 'yes' === $settings['autoplay'] ),
			'infinite'      => ( 'yes' === $settings['infinite'] ),
			'pauseOnHover'  => ( 'yes' === $settings['pause_on_hover'] ),
			'speed'         => absint( $settings['transition_speed'] ),
			'arrows'        => $show_arrows,
			'dots'          => $show_dots,
			'rtl'           => $is_rtl,
			'fade'          => $fade,
			'btn_text_prev' => esc_html__('Back', 'oroco'),
			'btn_text_next' => esc_html__('Next', 'oroco')
		];

		$carousel_classes = [
			'oroco-slides',
		];

		$this->add_render_attribute(
			'slides', [
				'class'               => $carousel_classes,
				'data-slider_options' => wp_json_encode( $slick_options ),
				'data-animation'      => $settings['content_animation'],
			]
		);

		$this->add_render_attribute( 'wrapper', 'dir', $direction );

		$arrows_container = $settings['arrows_position'] == 'inside' ? 'container' : 'container-fluid';

		echo sprintf(
			'<div %s>
				<div class="oroco-loading"></div>
				<div class="arrows-wrapper"><div class="arrows-inner arrows-%s"></div></div>
				<div %s>%s</div>
			</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			esc_attr( $arrows_container ),
			$this->get_render_attribute_string( 'slides' ),
			implode( '', $slides )
		);
	}
}