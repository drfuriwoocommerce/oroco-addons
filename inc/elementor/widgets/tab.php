<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Tab widget
 */
class Tab extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-tab-shortcode';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Tab', 'oroco' );
	}

	/**
	 * Retrieve the widget circle.
	 *
	 * @return string Widget circle.
	 */
	public function get_icon() {
		return 'eicon-tabs';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {

		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is title', 'oroco' ),
				'label_block' => true,
			]
		);

		$repeater -> add_control(
			'desc',
			[
				'label'       => esc_html__( 'Content', 'oroco' ),
				'type'        => Controls_Manager::WYSIWYG ,
				'default'     => esc_html__( 'Event Note', 'oroco' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'elements',
			[
				'label'   => '',
				'type'    => Controls_Manager::REPEATER,
				'fields'  => $repeater->get_controls(),
				'default' => [
					[
						'title'                 => esc_html__( 'This is title', 'oroco' ),
						'desc'                  => esc_html__( 'This is description', 'oroco' ),
					],
					[
						'title'         => esc_html__( 'This is title', 'oroco' ),
						'desc'                  => esc_html__( 'This is description', 'oroco' ),
					],
					[
						'title'         => esc_html__( 'This is title', 'oroco' ),
						'desc'                  => esc_html__( 'This is description', 'oroco' ),
					],
					[
						'title'         => esc_html__( 'This is title', 'oroco' ),
						'desc'                  => esc_html__( 'This is description', 'oroco' ),
					],
					[
						'title'         => esc_html__( 'This is title', 'oroco' ),
						'desc'                  => esc_html__( 'This is description', 'oroco' ),
					],
				],
				'title_field'   => '{{{ title }}}',

			]
		);

		$this->end_controls_section();

	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_style() {
		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs(
			'style_tabs_icons'
		);

		// Title
		$this->start_controls_tab(
			'content_style_title',
			[
				'label' => __( 'Title', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'title_align',
			[
				'label'       => esc_html__( 'Align', 'oroco' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'end'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				
				'default'   => '',
				'selectors'   => [
					'{{WRAPPER}} .oroco-tab-shortcode__title' => 'justify-content: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'title_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-tab-shortcode__title .tab-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'title_border',
			[
				'label'        => __( 'Border', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
				'separator' => 'before',
			]
		);

		$this->start_popover();

		$this->add_control(
			'title_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'oroco' ),
					'dashed' => esc_html__( 'Dashed', 'oroco' ),
					'solid'  => esc_html__( 'Solid', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
					''       => esc_html__( 'Default', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_border_width',
			[
				'label'     => esc_html__( 'Border Width', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'title_border_radius',
			[
				'label'     => esc_html__( 'Border Radius', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->add_responsive_control(
			'title_spacing',
			[
				'label'     => __( 'Gap', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title' => 'margin:0 {{SIZE}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .oroco-tab-shortcode .tab-title',
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'active_style',
			[
				'label'     => esc_html__( 'Active', 'oroco' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'tit_ac_color',
			[
				'label'     => __( 'Title Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title.active' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'tit_ac_border_color',
			[
				'label'     => __( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-title.active' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Desc
		$this->start_controls_tab(
			'content_desc',
			[
				'label' => __( 'Description', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'content_align',
			[
				'label'       => esc_html__( 'Align', 'oroco' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				
				'default'   => '',
				'selectors'   => [
					'{{WRAPPER}} .oroco-tab-shortcode__content' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-tab-shortcode .oroco-tab-shortcode__content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'note_typography',
				'selector' => '{{WRAPPER}} .oroco-tab-shortcode .tab-desc',
			]
		);

		$this->add_responsive_control(
			'note_spacing',
			[
				'label'     => __( 'Spacing Top', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .oroco-tab-shortcode__content' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-tab-shortcode .tab-desc' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	/**
	 * Render circle box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'oroco-tab-shortcode'
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$output_title = $output_desc =  array();

		foreach (  $settings["elements"]  as $index => $item ) {

			$title     = $item["title"] ? '<h5 class="tab-title tab-nav">' . $item["title"] . '</h5>' : '';
			$desc = $item['desc'] ? sprintf( '<div class="tab-panel tab-desc">%s</div>', $item["desc"]  ) : '';


			$output_title[] = $title == ''  ? '' : $title ;
			$output_desc[] = $desc == ''  ? '' : $desc ;
		}

		$output_events = sprintf('<div class="oroco-tab-shortcode__title tabs-nav">%s</div><div class="oroco-tab-shortcode__content">%s</div>',implode( '', $output_title ), implode( '', $output_desc ));

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$output_events
		);
	}

	/**
	 * Render circle box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}