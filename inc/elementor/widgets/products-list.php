<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use OrocoAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Products_List extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-products-list';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Products List', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-bullet-list';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		$this->_register_heading_settings_controls();

		$this->start_controls_section(
			'section_products',
			[ 'label' => esc_html__( 'Products', 'oroco' ) ]
		);

		$this->add_control(
			'product_cats',
			[
				'label'       => esc_html__( 'Product Categories', 'oroco' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy(),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);

		$this->add_control(
			'per_page',
			[
				'label'   => esc_html__( 'Total Products', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3,
				'min'     => 1,
				'max'     => 50,
				'step'    => 1,
			]
		);

		$this->add_control(
			'products',
			[
				'label'   => esc_html__( 'Products', 'oroco' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'recent'       => esc_html__( 'Recent', 'oroco' ),
					'featured'     => esc_html__( 'Featured', 'oroco' ),
					'best_selling' => esc_html__( 'Best Selling', 'oroco' ),
					'top_rated'    => esc_html__( 'Top Rated', 'oroco' ),
					'sale'         => esc_html__( 'On Sale', 'oroco' ),
				],
				'default' => 'recent',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'     => esc_html__( 'Order By', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''           => esc_html__( 'Default', 'oroco' ),
					'date'       => esc_html__( 'Date', 'oroco' ),
					'title'      => esc_html__( 'Title', 'oroco' ),
					'menu_order' => esc_html__( 'Menu Order', 'oroco' ),
					'rand'       => esc_html__( 'Random', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'condition' => [
					'products' => [ 'recent', 'top_rated', 'sale', 'featured' ],
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'     => esc_html__( 'Order', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''     => esc_html__( 'Default', 'oroco' ),
					'asc'  => esc_html__( 'Ascending', 'oroco' ),
					'desc' => esc_html__( 'Descending', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'condition' => [
					'products' => [ 'recent', 'top_rated', 'sale', 'featured' ],
				],
			]
		);

		$this->end_controls_section();

		/**
		 * Tab Style
		 */
		// General
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .or-products-list' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);


		$this->end_controls_section();

		// Products
		$this->start_controls_section(
			'section_products_style',
			[
				'label' => esc_html__( 'Products', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'product_items_space',
			[
				'label'     => esc_html__( 'Items Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .or-products-list .products .product' => 'margin-bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .or-products-list .products .product' => 'padding-bottom: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .or-products-list .products .product:last-child' => 'padding: 0; margin: 0;',
				],
			]
		);

		$this->add_control(
			'product_items_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'' 		=> esc_html__( 'Default', 'oroco' ),
					'dotted' => esc_html__( 'Dotted', 'oroco' ),
					'dashed' => esc_html__( 'Dashed', 'oroco' ),
					'solid'  => esc_html__( 'Solid', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .or-products-list .products .product' => 'border-style: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'product_items_border_width',
			[
				'label'     => esc_html__( 'Border Width', 'oroco' ),
				'type'      => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .or-products-list .products .product' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'product_items_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .or-products-list .products .product' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function _register_heading_settings_controls() {
		// Heading Settings
		$this->start_controls_section(
			'section_heading',
			[ 'label' => esc_html__( 'Heading', 'oroco' ) ]
		);

		// Title
		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is the title', 'oroco' ),
				'placeholder' => esc_html__( 'Enter your title', 'oroco' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'view_all_text',
			[
				'label'   => esc_html__( 'Text', 'oroco' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Shop all', 'oroco' ),
				'separator' => 'before',
			]
		);

		$this->add_control(
			'view_all_link', [
				'label'         => esc_html__( 'Link', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'view_all_icon',
			[
				'label'   => esc_html__( 'Icon', 'oroco' ),
				'type'    => Controls_Manager::ICONS,
			]
		);

		$this->end_controls_section(); // End Heading Settings

		// Heading Style
		$this->start_controls_section(
			'section_style_heading',
			[
				'label' => esc_html__( 'Heading', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .or-products-list .product-header' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'heading_title_style_settings' );

		$this->start_controls_tab( 'heading_title_style_tab', [ 'label' => esc_html__( 'Title', 'oroco' ) ] );
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_title_typography',
				'selector' => '{{WRAPPER}} .or-products-list .product-header h2',
			]
		);

		$this->add_control(
			'heading_title_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .or-products-list .product-header h2' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'heading_link_style_tab', [ 'label' => esc_html__( 'Link', 'oroco' ) ] );

		$this->add_control(
			'heading_links_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .or-products-list .product-header .header-link span' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'heading_links_hover_color',
			[
				'label'     => esc_html__( 'Hover Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .or-products-list .product-header .header-link:hover span'       => 'color: {{VALUE}}',
					'{{WRAPPER}} .or-products-list .product-header .header-link:hover .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_links_typography',
				'selector' => '{{WRAPPER}} .or-products-list .product-header .header-link .link-text',
			]
		);

		// Icon
		$this->add_control(
			'header_link_icon_style',
			[
				'label'        => __( 'Icon', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'header_link_icon_font_size',
			[
				'label'     => __( 'Font Size', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .or-products-list .product-header .header-link .oroco-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .or-products-list .product-header .header-link .oroco-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'header_link_icon_spacing',
			[
				'label'     => __( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 20,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .or-products-list .product-header .header-link .oroco-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();

		$this->end_controls_tabs();
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'or-products-list woocommerce',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$title = $link = $icon = '';

		if ( $settings['title'] ) {
			$title = '<h2>' . $settings['title'] . '</h2>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="oroco-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $icon;
			$link = $this->get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
		}

		$atts = [
			'per_page' 		=> $settings['per_page'],
			'type'     		=> $settings['products'],
			'product_cats'  => $settings['product_cats'],
			'orderby'       => $settings['orderby'],
			'order'         => $settings['order'],
		];

		$query_args = self::get_query_args( $atts );

		$products    = get_posts( $query_args );
		$product_ids = [];
		$output      = [];
		foreach ( $products as $product ) {
			$id = $product->ID;

			if ( ! in_array( $id, $product_ids ) ) {
				$product_ids[] = $id;

				$productw = new \WC_Product( $id );

				$output[] = sprintf(
					'<li class="product">
						<div class="product-item">
							<div class="product-thumbnail">
								<a href="%s">%s</a>
							</div>

							<div class="product-inners">
								<h2>
									<a href="%s">%s</a>
								</h2>
								%s
								<span class="price">%s</span>
							</div>
						</div>
					</li>',
					esc_url( $productw->get_permalink() ),
					wp_get_attachment_image( get_post_thumbnail_id( $id ), 'shop_thumbnail' ),
					esc_url( $productw->get_permalink() ),
					$productw->get_title(),
					wc_get_rating_html( $productw->get_average_rating() ),
					wp_kses_post( $productw->get_price_html() )
				);
			}
		}

		$content = sprintf( 
			'<div class="product-header">%s%s</div>
			<div class="products-content">
				<ul class="products">%s</ul>
			</div>', 
			wp_kses_post( $title ),
			$link ,
			implode( '', $output ) );

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$content
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Build query args from shortcode attributes
	 *
	 * @param array $atts
	 *
	 * @return array
	 */
	protected static function get_query_args( $atts ) {
		$args = array(
			'post_type'              => 'product',
			'post_status'            => 'publish',
			'orderby'                => $atts['orderby'],
			'order'                  => $atts['order'],
			'ignore_sticky_posts'    => 1,
			'posts_per_page'         => $atts['per_page'],
			'meta_query'             => WC()->query->get_meta_query(),
			'update_post_term_cache' => false,
			'update_post_meta_cache' => false,
		);

		if ( version_compare( WC()->version, '3.0.0', '>=' ) ) {
			$args['tax_query'] = WC()->query->get_tax_query();
		}

		if ( ! empty( $atts['product_cats'] ) ) {
			$args['tax_query'] = array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'slug',
					'terms'    => $atts['product_cats'],
					'operator' => 'IN',
				),
			);
		}

		if ( ! empty( $atts['category'] ) ) {
			$args['product_cat'] = $atts['category'];
			unset( $args['update_post_term_cache'] );
		}

		if ( isset( $atts['type'] ) ) {
			switch ( $atts['type'] ) {
				case 'recent':
					unset( $args['update_post_meta_cache'] );
					break;

				case 'featured':
					if ( version_compare( WC()->version, '3.0.0', '<' ) ) {
						$args['meta_query'][] = array(
							'key'   => '_featured',
							'value' => 'yes',
						);
					} else {
						$args['tax_query'][] = array(
							'taxonomy' => 'product_visibility',
							'field'    => 'name',
							'terms'    => 'featured',
							'operator' => 'IN',
						);
					}

					unset( $args['update_post_meta_cache'] );
					break;

				case 'sale':
					$args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
					break;

				case 'best_selling':
					$args['meta_key'] = 'total_sales';
					unset( $args['update_post_meta_cache'] );

					break;

				case 'top_rated':
					$args['meta_key'] = '_wc_average_rating';
					unset( $args['update_post_meta_cache'] );
					break;
			}
		}

		return $args;
	}

	/**
	 * Adds a tax_query index to the query to filter by category.
	 *
	 * @param array $args
	 * @param string $category
	 *
	 * @return array;
	 */
	protected static function _maybe_add_category_args( $args, $category ) {
		if ( ! empty( $category ) ) {
			if ( empty( $args['tax_query'] ) ) {
				$args['tax_query'] = array();
			}
			$args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_cat',
					'terms'    => array_map( 'sanitize_title', explode( ',', $category ) ),
					'field'    => 'slug',
					'operator' => 'IN',
				),
			);
		}

		return $args;
	}

	/**
	 * WP Core doens't let us change the sort direction for invidual orderby params - https://core.trac.wordpress.org/ticket/17065.
	 *
	 * This lets us sort by meta value desc, and have a second orderby param.
	 *
	 * @access public
	 *
	 * @param array $args
	 *
	 * @return array
	 */
	public static function order_by_popularity_post_clauses( $args ) {
		global $wpdb;
		$args['orderby'] = "$wpdb->postmeta.meta_value+0 DESC, $wpdb->posts.post_date DESC";

		return $args;
	}

	/**
	 * Render link control output
	 *
	 * @param       $link_key
	 * @param       $url
	 * @param       $content
	 * @param array $attr
	 *
	 * @return string
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$tag = 'a';

		if ( empty( $attr_default['href'] ) ) {
			$tag = 'span';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $this->get_render_attribute_string( $link_key ), $content );
	}
}