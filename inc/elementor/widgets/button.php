<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Button widget
 */
class Button extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-button-shortcode';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Button', 'oroco' );
	}

	/**
	 * Retrieve the widget circle.
	 *
	 * @return string Widget circle.
	 */
	public function get_icon() {
		return 'eicon-button';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {

		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);


		$this->add_control(
			'text',
			[
				'label'       => esc_html__( 'Text', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is text', 'oroco' ),
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'oroco' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'oroco' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'mode',
			[
				'label'       => esc_html__( 'Mode', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'lager' 		=> esc_html__( 'Large', 'oroco' ),
					'regular'  		=> esc_html__( 'Regular', 'oroco' ),
					'outline'  		=> esc_html__( 'Outline', 'oroco' ),
					'underlined'  	=> esc_html__( 'Underlined', 'oroco' ),
					'custom'  		=> esc_html__( 'Custom', 'oroco' ),
				],
				'separator' => 'before',
				'default'     => 'lager',
				'label_block' => true,
			]
		);

		$this->add_responsive_control(
			'content_align',
			[
				'label'       => esc_html__( 'Align', 'oroco' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				
				'default'   => '',
				'selectors'   => [
					'{{WRAPPER}} .oroco-button-shortcode' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_style() {
		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-button-shortcode .oroco-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .oroco-button-shortcode .oroco-button',
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'selector' => '{{WRAPPER}} .oroco-button-shortcode .oroco-button',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'heading_border',
				'label'    => esc_html__( 'Border', 'oroco' ),
				'selector' => '{{WRAPPER}} .oroco-button-shortcode .oroco-button',
				'fields_options'  => [
					'border' => [
						'default' => 'none',
					],
				],
				'conditions' => [
					'terms' => [
						[
							'name'  => 'mode',
							'value' => 'custom',
						]
					]
				],
			]
		);


		$this->add_control(
			'content_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-button-shortcode .oroco-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-button-shortcode .oroco-button' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render circle box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			$settings['mode'] == 'custom' ? '' : ' oroco-button'
		];

		switch ($settings['mode']) {
			case 'regular':
				$classes[] = 'oroco-button--regular';
				break;

			case 'outline':
				$classes[] = 'oroco-button--regular oroco-button--outline';
				break;

			case 'underlined':
				$classes[] = 'oroco-button--regular oroco-button--underlined';
				break;

			case 'custom':
				$classes[] = 'oroco-button--custom';
				break;
			
			default:
				# code...
				break;
		}
		

		$this->add_render_attribute( 'wrapper', 'class', 'oroco-button-shortcode' );
		$this->add_render_attribute( 'inner', 'class', $classes );

		$name    = $settings['text'] ? sprintf('<span class="oroco-button__text">%s</span>',$settings['text']) : '';

		$btn_html = $settings['link']['url'] ? $this->get_link_control( 'key-button', $settings['link'], $settings['text'], [ 'class' => 'oroco-button__text' ] ) : $name;

		echo sprintf(
			'<div %s><div %s>%s</div></div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$this->get_render_attribute_string( 'inner' ),
			$btn_html
		);
	}

	/**
	 * Render circle box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}

	protected function get_link_control( $link_key, $url, $content, $attr = [] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}