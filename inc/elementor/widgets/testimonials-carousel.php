<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Control_Date_Time ;
use Elementor\Group_Control_Image_Size;
use OrocoAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Testimonials Carousel widget
 */
class Testimonials_Carousel extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-testimonials-carousel';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Oroco - Testimonials Carousel', 'oroco' );
	}

	/**
	 * Retrieve the widget circle.
	 *
	 * @return string Widget circle.
	 */
	public function get_icon() {
		return 'eicon-testimonial-carousel';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}


	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->content_settings_controls();
		$this->carousel_settings_controls();
	}

	protected function content_settings_controls() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'oroco' ) ]
		);

		$now = new \DateTime();

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icons', 'oroco' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'fas fa-quote-right',
					'library' => 'fa-solid',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'full',
			]
		);

		$this->add_control(
			'show_elements',
			[
				'label' => __( 'Show Elements', 'oroco' ),
				'type' => Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => [
					'date'  => __( 'Date', 'oroco' ),
					'job'   => __( 'Job', 'oroco' ),
				],
				'default' => [ 'date'],
			]
		);



		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label'   => esc_html__( 'Image', 'oroco' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/60x60/f5f5f5?text=60x60',
				],
			]
		);

		$repeater->add_control(
			'own_date',
			[
				'label' => __( 'Release Date', 'oroco' ),
				'type' => Controls_Manager::DATE_TIME,
				'default' =>  $now->format('Y-m-d'),
				// 'picker_options' => [
				// 	'mode'    => 'range',
				// ]
			]
		);

		$repeater->add_control(
			'name',
			[
				'label'       => esc_html__( 'Name', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is title', 'oroco' ),
			]
		);

		$repeater->add_control(
			'job',
			[
				'label'       => esc_html__( 'Job', 'oroco' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is job', 'oroco' ),
			]
		);

		$repeater -> add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'oroco' ),
				'type'        => Controls_Manager::WYSIWYG ,
				'default'     => esc_html__( 'This is desc', 'oroco' ),
			]
		);

		$this->add_control(
			'elements',
			[
				'label'         => '',
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'name' => esc_html__( 'This is the name', 'oroco' ),
						'desc' => esc_html__( 'This is the description', 'oroco' ),
					]

				],
				'title_field'   => '{{{ name }}}',
				'prevent_empty' => false
			]
		);
		
		$this->end_controls_section();
	}

	protected function carousel_settings_controls() {
		// Carousel Settings
		$this->start_controls_section(
			'section_carousel_settings',
			[ 'label' => esc_html__( 'Carousel Settings', 'oroco' ) ]
		);
		$this->add_responsive_control(
			'slidesToShow',
			[
				'label'   => esc_html__( 'Slides to show', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 7,
				'default' => 1,
			]
		);
		$this->add_responsive_control(
			'slidesToScroll',
			[
				'label'   => esc_html__( 'Slides to scroll', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 5,
				'default' => 1,
			]
		);
		$this->add_responsive_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'oroco' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'oroco' ),
					'arrows' => esc_html__( 'Arrows', 'oroco' ),
					'dots'   => esc_html__( 'Dots', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default' => 'both',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'oroco' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'oroco' ),
				'label_on'  => __( 'On', 'oroco' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'oroco' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 50,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'oroco' ),
			]
		);

		// Additional Settings
		$this->_register_responsive_settings_controls();

		$this->end_controls_section(); // End Carousel Settings
	}

	protected function _register_responsive_settings_controls() {
		$this->add_control(
			'responsive_settings_divider',
			[
				'label' => __( 'Additional Settings', 'oroco' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
	
		$repeater = new \Elementor\Repeater();
	
		$repeater->add_control(
			'responsive_breakpoint', [
				'label' => __( 'Breakpoint', 'oroco' ) . ' (px)',
				'description' => __( 'Below this breakpoint the options below will be triggered', 'oroco' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 1200,
				'min'             => 320,
				'max'             => 1920,
			]
		);
		$repeater->add_control(
			'responsive_slidesToShow',
			[
				'label'           => esc_html__( 'Slides to show', 'oroco' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'max'             => 7,
				'default' => 1,
			]
		);
		$repeater->add_control(
			'responsive_slidesToScroll',
			[
				'label'           => esc_html__( 'Slides to scroll', 'oroco' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'max'             => 7,
				'default' => 1,
			]
		);
		$repeater->add_control(
			'responsive_navigation',
			[
				'label'           => esc_html__( 'Navigation', 'oroco' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					'both'   => esc_html__( 'Arrows and Dots', 'oroco' ),
					'arrows' => esc_html__( 'Arrows', 'oroco' ),
					'dots'   => esc_html__( 'Dots', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default' => 'dots',
				'toggle'          => false,
			]
		);
	
		$this->add_control(
			'carousel_responsive_settings',
			[
				'label' => __( 'Settings', 'oroco' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default' => [
					
				],
				'title_field' => '{{{ responsive_breakpoint }}}' . 'px',
				'prevent_empty' => false,
			]
		);
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_content_style();
		$this->section_carousel_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_content_style() {
		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'header_type',
			[
				'label'     => esc_html__( 'Header Type ', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Block', 'oroco' ),
					'flex' => esc_html__( 'Flex', 'oroco' ),
				],
				'default'   => 'flex',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel__header' => 'display: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'position',
			[
				'label'     => esc_html__( 'Align Text', 'oroco' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel' => 'text-align: {{VALUE}};',
				],
				'condition'   => [
					'header_type' => 'block',
				],
			]
		);

		$this->add_responsive_control(
			'position_img',
			[
				'label'     => esc_html__( 'Align Image', 'oroco' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-header__img img' => '{{VALUE}};',
				],
				'condition'   => [
					'header_type' => 'block',
				],
				'selectors_dictionary' => [
					'left' 		=> '',
					'center'   	=> 'margin:auto',
					'right'   	=> 'margin-left:auto; margin-right:0;',
				],
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'border_style',
			[
				'label'        => __( 'Border', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_control(
			'content_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'oroco' ),
					'dashed' => esc_html__( 'Dashed', 'oroco' ),
					'solid'  => esc_html__( 'Solid', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-item' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_border_width',
			[
				'label'     => __( 'Border Width', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 7,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-item' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_border',
			[
				'label'     => esc_html__( 'Border Radius', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-item' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'content_border_color',
			[
				'label'     => __( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-item' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->add_control(
			'content_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'oroco' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-item' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control( 
			'separator_1',
			[
				'type'      => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs(
			'style_tabs_content_1'
		);

		// Img
		$this->start_controls_tab(
			'img_style',
			[
				'label' => __( 'Image', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'img_spacing_1',
			[
				'label'     => __( 'Spacing Right', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-header__img' => 'margin-right: {{SIZE}}{{UNIT}};margin-bottom:0;',
				],
				'condition'   => [
					'header_type' => 'flex',
				],
			]
		);

		$this->add_responsive_control(
			'img_spacing_2',
			[
				'label'     => __( 'Spacing Bottom', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-header__img' => 'margin-bottom: {{SIZE}}{{UNIT}};margin-right:0;',
				],
				'condition'   => [
					'header_type' => 'block',
				],
			]
		);



		$this->add_responsive_control(
			'img_size',
			[
				'label'     => esc_html__( 'Border Radius (%)', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'%' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .box-header__img img' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		// Icon
		$this->start_controls_tab(
			'icon_style',
			[
				'label' => __( 'Icon', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'icon_spacing',
			[
				'label'              => esc_html__( 'Position ', 'oroco' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [],
				'selectors'          => [
					'{{WRAPPER}} .oroco-testimonials-carousel .oroco-icon' => ' top:{{TOP}}{{UNIT}};right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => esc_html__( 'Font Size', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .oroco-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .oroco-testimonials-carousel .oroco-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .oroco-icon' => 'color: {{VALUE}}',
					'{{WRAPPER}} .oroco-testimonials-carousel .oroco-icon svg' => 'fill: {{VALUE}};',

				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->add_control( 
			'separator_2',
			[
				'type'      => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs(
			'style_tabs_content_2'
		);
		// Name
		$this->start_controls_tab(
			'content_style_title',
			[
				'label' => __( 'Name', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'title_spacing',
			[
				'label'     => __( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .item-name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .oroco-testimonials-carousel .item-name',
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .item-name' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Date
		$this->start_controls_tab(
			'content_style_date',
			[
				'label' => __( 'Date', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'date_spacing',
			[
				'label'     => __( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .item-date' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'date_typography',
				'selector' => '{{WRAPPER}} .oroco-testimonials-carousel .item-date',
			]
		);

		$this->add_control(
			'date_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .item-date' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Job
		$this->start_controls_tab(
			'content_style_job',
			[
				'label' => __( 'Job', 'oroco' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'job_typography',
				'selector' => '{{WRAPPER}} .oroco-testimonials-carousel .item-job',
			]
		);

		$this->add_control(
			'job_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .item-job' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Desc
		$this->start_controls_tab(
			'content_desc',
			[
				'label' => __( 'Description', 'oroco' ),
			]
		);

		$this->add_responsive_control(
			'desc_spacing',
			[
				'label'     => __( 'Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel__content' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'note_typography',
				'selector' => '{{WRAPPER}} .oroco-testimonials-carousel__content',
			]
		);

		

		$this->add_control(
			'desc_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel__content' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	protected function section_carousel_style() {
		$this->start_controls_section(
			'section_carousel_style',
			[
				'label' => esc_html__( 'Carousel Settings', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'arrows_style_divider',
			[
				'label' => esc_html__( 'Arrows', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		// Arrows
		$this->add_control(
			'arrows_style',
			[
				'label'        => __( 'Options', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sliders_arrows_size',
			[
				'label'     => __( 'Size', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-arrow' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'sliders_arrows_offset',
			[
				'label'     => esc_html__( 'Horizontal Offset', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-prev-arrow' => 'left: {{VALUE}}px;',
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-next-arrow' => 'right: {{VALUE}}px;',
				],
			]
		);

		$this->end_popover();

		$this->start_controls_tabs( 'sliders_normal_settings' );

		$this->start_controls_tab( 'sliders_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'sliders_arrow_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-arrow' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sliders_hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'sliders_arrow_hover_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-arrow:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'dots_style_divider',
			[
				'label'     => esc_html__( 'Dots', 'oroco' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'dots_style',
			[
				'label'        => __( 'Options', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_responsive_control(
			'sliders_dots_gap',
			[
				'label'     => __( 'Gap', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-dots li' => 'margin: 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'sliders_dots_offset',
			[
				'label'     => esc_html__( 'Vertical Offset', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-dots' => 'bottom: {{VALUE}}px;',
				],
			]
		);

		$this->add_control(
			'dots_background_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-dots li:not(.slick-active) button' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-testimonials-carousel .slick-dots li.slick-active button:before' => 'border-color: {{VALUE}};',
				],
			]
		);
		
		$this->end_popover();

		$this->end_controls_section();
	}

	/**
	 * Render circle box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'oroco-testimonials-carousel'
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$this->add_render_attribute(
			'wrapper', 'data-show', [
				intval($settings['slidesToShow'])
			]
		);

		$settings['infinite'] = false;
		$this->add_render_attribute('wrapper','data-slick',wp_json_encode(Elementor::get_data_slick($settings)) );

		$icon = '';
		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="oroco-icon">' . ob_get_clean() . '</span>';
		}

		$output = array();

		$els = $settings['elements'];

		if ( ! empty ( $els ) ) {
			foreach ( $els as $index => $item ) {

				$date_html = $item['own_date'] && in_array('date', $settings['show_elements']) ? sprintf( '<div class="item-date">%s</div>', date("d F Y", strtotime( $item['own_date'] )) ) : '' ;
				$job_html = $item['job'] && in_array( 'job' ,$settings['show_elements'])? sprintf( '<div class="item-job">%s</div>', $item['job']) : '' ;
				$name     = $item["name"] ? '<h6 class="item-name">' . $item["name"] .'</h6>' : '';

				$settings['image']      = $item['image'];
				$image     = $item["image"] ? '<div class="box-header__img">' . Group_Control_Image_Size::get_attachment_image_html( $settings ) .'</div>' : '';
				
				$output_content = [];

				$output_content[] = '<div class="oroco-testimonials-carousel__header">';
				$output_content[] = $image ;
				$output_content[] = '<div class="box-header__text">';
				$output_content[] = $name ;
				$output_content[] = $date_html ;
				$output_content[] = $job_html ;
				$output_content[] = '</div>';
				$output_content[] = '</div>';
				$output_content[] = '<div class="oroco-testimonials-carousel__content">';
				$output_content[] = $item["desc"];
				$output_content[] = '</div>';

				$output[] = sprintf( '<div class="box-item">%s%s</div>', implode('', $output_content), $icon );
			}

		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode('', $output)
		);
	}

	/**
	 * Render circle box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}