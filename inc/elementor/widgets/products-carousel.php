<?php

namespace OrocoAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use OrocoAddons\Elementor;
use OrocoAddons\Elementor_AjaxLoader;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Product Carousel widget
 */
class Products_Carousel extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'oroco-products-carousel';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Oroco - Products Carousel', 'oroco' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-post-slider';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor',
			'isInViewport',
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->_register_products_settings_controls();
		$this->_register_carousel_settings_controls();
		$this->_register_lazy_load_controls();
	}

	protected function _register_products_settings_controls() {
		// Products Settings
		$this->start_controls_section(
			'section_products',
			[ 'label' => esc_html__( 'Products', 'oroco' ) ]
		);

		$this->add_control(
			'source',
			[
				'label'       => esc_html__( 'Source', 'oroco' ),
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'default' => esc_html__( 'Default', 'oroco' ),
					'custom'  => esc_html__( 'Custom', 'oroco' ),
				],
				'default'     => 'default',
				'label_block' => true,
			]
		);
		$this->add_control(
			'ids',
			[
				'label'       => esc_html__( 'Products', 'oroco' ),
				'placeholder' => esc_html__( 'Click here and start typing...', 'oroco' ),
				'type'        => 'orautocomplete',
				'default'     => '',
				'label_block' => true,
				'multiple'    => true,
				'source'      => 'product',
				'sortable'    => true,
				'condition'   => [
					'source' => 'custom',
				],
			]
		);

		$this->add_control(
			'product_cats',
			[
				'label'       => esc_html__( 'Product Categories', 'oroco' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy(),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
				'condition'   => [
					'source' => 'default',
				],
			]
		);

		$this->add_control(
			'per_page',
			[
				'label'     => esc_html__( 'Total Products', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 8,
				'min'       => 2,
				'max'       => 50,
				'step'      => 1,
				'condition' => [
					'source' => 'default',
				],
			]
		);

		$this->add_control(
			'products',
			[
				'label'     => esc_html__( 'Product', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'recent'       => esc_html__( 'Recent', 'oroco' ),
					'featured'     => esc_html__( 'Featured', 'oroco' ),
					'best_selling' => esc_html__( 'Best Selling', 'oroco' ),
					'top_rated'    => esc_html__( 'Top Rated', 'oroco' ),
					'sale'         => esc_html__( 'On Sale', 'oroco' ),
				],
				'default'   => 'recent',
				'toggle'    => false,
				'condition' => [
					'source' => 'default',
				],
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'      => esc_html__( 'Order By', 'oroco' ),
				'type'       => Controls_Manager::SELECT,
				'options'    => [
					''           => esc_html__( 'Default', 'oroco' ),
					'date'       => esc_html__( 'Date', 'oroco' ),
					'title'      => esc_html__( 'Title', 'oroco' ),
					'menu_order' => esc_html__( 'Menu Order', 'oroco' ),
					'rand'       => esc_html__( 'Random', 'oroco' ),
				],
				'default'    => '',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'products',
							'value' => [ 'recent', 'top_rated', 'sale', 'featured' ],
						],
						[
							'name'  => 'source',
							'value' => 'default',
						]
					]
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'      => esc_html__( 'Order', 'oroco' ),
				'type'       => Controls_Manager::SELECT,
				'options'    => [
					''     => esc_html__( 'Default', 'oroco' ),
					'asc'  => esc_html__( 'Ascending', 'oroco' ),
					'desc' => esc_html__( 'Descending', 'oroco' ),
				],
				'default'    => '',
				'conditions' => [
					'terms' => [
						[
							'name'  => 'products',
							'value' => [ 'recent', 'top_rated', 'sale', 'featured' ],
						],
						[
							'name'  => 'source',
							'value' => 'default',
						]
					]
				],
			]
		);

		$this->end_controls_section(); // End Products Settings

		// Products Style
		$this->start_controls_section(
			'section_products_style',
			[
				'label' => esc_html__( 'Products', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'content_background',
			[
				'label'     => esc_html__( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .products-content' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'products_padding',
			[
				'label'      => esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-products-carousel .products-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function _register_carousel_settings_controls() {
		// Carousel Settings
		$this->start_controls_section(
			'section_carousel_settings',
			[ 'label' => esc_html__( 'Carousel Settings', 'oroco' ) ]
		);

		$this->add_control(
			'slidesToShow',
			[
				'label'   => esc_html__( 'Slides to show', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 4,
			]
		);
		$this->add_control(
			'slidesToScroll',
			[
				'label'   => esc_html__( 'Slides to scroll', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 1,
			]
		);
		$this->add_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'oroco' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'oroco' ),
					'arrows' => esc_html__( 'Arrows', 'oroco' ),
					'dots'   => esc_html__( 'Dots', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default' => 'dots',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'oroco' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'oroco' ),
				'label_on'  => __( 'On', 'oroco' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'oroco' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'oroco' ),
				'label_on'  => __( 'On', 'oroco' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'oroco' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'oroco' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 50,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'oroco' ),
			]
		);

		// Additional Settings
		$this->_register_responsive_settings_controls();

		$this->end_controls_section(); // End Carousel Settings

		// Carousel Style
		// Carousel Settings
		$this->start_controls_section(
			'section_carousel_style',
			[
				'label' => esc_html__( 'Carousel Settings', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'arrows_style_divider',
			[
				'label' => esc_html__( 'Arrows', 'oroco' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		// Arrows
		$this->add_control(
			'arrows_style',
			[
				'label'        => __( 'Options', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sliders_arrows_size',
			[
				'label'     => __( 'Size', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-arrow' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'sliders_arrows_offset',
			[
				'label'     => esc_html__( 'Horizontal Offset', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-prev-arrow' => 'left: {{VALUE}}px;',
					'{{WRAPPER}} .oroco-products-carousel .slick-next-arrow' => 'right: {{VALUE}}px;',
				],
			]
		);

		$this->end_popover();

		$this->start_controls_tabs( 'sliders_normal_settings' );

		$this->start_controls_tab( 'sliders_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );

		$this->add_control(
			'sliders_arrow_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-arrow' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sliders_hover', [ 'label' => esc_html__( 'Hover', 'oroco' ) ] );

		$this->add_control(
			'sliders_arrow_hover_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-arrow:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'dots_style_divider',
			[
				'label'     => esc_html__( 'Dots', 'oroco' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'dots_style',
			[
				'label'        => __( 'Options', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_responsive_control(
			'sliders_dots_gap',
			[
				'label'     => __( 'Gap', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-dots li' => 'margin: 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'sliders_dots_offset',
			[
				'label'     => esc_html__( 'Vertical Offset', 'oroco' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-dots' => 'bottom: {{VALUE}}px;',
				],
			]
		);

		$this->add_control(
			'dots_background_color',
			[
				'label'     => esc_html__( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-products-carousel .slick-dots li:not(.slick-active) button' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-products-carousel .slick-dots li.slick-active button:before' => 'border-color: {{VALUE}};',
				],
			]
		);
		
		$this->end_popover();

		$this->end_controls_section();
	}

	protected function _register_responsive_settings_controls() {
		$this->add_control(
			'responsive_settings_divider',
			[
				'label' => __( 'Additional Settings', 'oroco' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
	
		$repeater = new \Elementor\Repeater();
	
		$repeater->add_control(
			'responsive_breakpoint', [
				'label' => __( 'Breakpoint', 'oroco' ) . ' (px)',
				'description' => __( 'Below this breakpoint the options below will be triggered', 'oroco' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 1200,
				'min'             => 320,
				'max'             => 1920,
			]
		);
		$repeater->add_control(
			'responsive_slidesToShow',
			[
				'label'           => esc_html__( 'Slides to show', 'oroco' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'max'             => 7,
				'default' => 1,
			]
		);
		$repeater->add_control(
			'responsive_slidesToScroll',
			[
				'label'           => esc_html__( 'Slides to scroll', 'oroco' ),
				'type'            => Controls_Manager::NUMBER,
				'min'             => 1,
				'max'             => 7,
				'default' => 1,
			]
		);
		$repeater->add_control(
			'responsive_navigation',
			[
				'label'           => esc_html__( 'Navigation', 'oroco' ),
				'type'            => Controls_Manager::SELECT,
				'options'         => [
					'both'   => esc_html__( 'Arrows and Dots', 'oroco' ),
					'arrows' => esc_html__( 'Arrows', 'oroco' ),
					'dots'   => esc_html__( 'Dots', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default' => 'dots',
				'toggle'          => false,
			]
		);
	
		$this->add_control(
			'carousel_responsive_settings',
			[
				'label' => __( 'Settings', 'oroco' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default' => [
					[
						'responsive_breakpoint' => 1025,
						'responsive_slidesToShow' => 3,
						'responsive_slidesToScroll' => 3,
						'responsive_navigation' => 'dots',
					],
					[
						'responsive_breakpoint' => 768,
						'responsive_slidesToShow' => 2,
						'responsive_slidesToScroll' => 2,
						'responsive_navigation' => 'dots',
					],
				],
				'title_field' => '{{{ responsive_breakpoint }}}' . 'px',
				'prevent_empty' => false,
			]
		);
	}

	/**
	 * Lazy Load
	 */
	protected function _register_lazy_load_controls() {
		// Content
		$this->start_controls_section(
			'section_lazy_load',
			[ 'label' => esc_html__( 'Lazy Load', 'oroco' ) ]
		);
		$this->add_control(
			'lazy_load',
			[
				'label'        => esc_html__( 'Enable', 'oroco' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'oroco' ),
				'label_off'    => esc_html__( 'No', 'oroco' ),
				'default'      => '',
			]
		);
		$this->add_responsive_control(
			'lazy_load_height',
			[
				'label'     => esc_html__( 'Height', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 10,
						'max' => 1000,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .or-elementor-ajax-wrapper .oroco-loading-wrapper' => 'min-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section(); // End
	
		// Style
		$this->start_controls_section(
			'section_lazy_load_style',
			[
				'label' => esc_html__( 'Lazy Load', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'loading_border_color',
			[
				'label'     => esc_html__( 'Loading Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .or-elementor-ajax-wrapper .oroco-loading:after' => 'border-color: {{VALUE}} transparent {{VALUE}} transparent;',
					'{{WRAPPER}} .or-elementor-ajax-wrapper .oroco-loading:before' => 'background-color: {{VALUE}}',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'oroco-products-carousel or-elementor-product-carousel woocommerce',
			$settings['lazy_load'] == 'yes' ? '' : 'no-infinite',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$is_rtl    = is_rtl();
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$this->add_render_attribute( 'wrapper', 'dir', $direction );

		$this->add_render_attribute( 'wrapper', 'data-settings', wp_json_encode( Elementor::get_data_slick( $settings ) ) );

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php if ( $settings['lazy_load'] == 'yes' ) : ?>
				<?php
				// AJAX settings
				$this->add_render_attribute(
					'ajax_wrapper', 'class', [
						'or-product-carousel-loading or-elementor-ajax-wrapper'
					]
				);
				$ajax_settings = [
					'slidesToShow' => $settings['slidesToShow'],
					'ids'          => $settings['ids'],
					'product_cats' => $settings['product_cats'],
					'per_page'     => $settings['per_page'],
					'products'     => $settings['products'],
					'orderby'      => $settings['orderby'],
					'order'        => $settings['order'],
				];
				$this->add_render_attribute( 'ajax_wrapper', 'data-settings', wp_json_encode( $ajax_settings ) );
				?>
                <div <?php echo $this->get_render_attribute_string( 'ajax_wrapper' ); ?>>
                    <div class="oroco-loading-wrapper"><div class="oroco-loading"></div></div>
                </div>
			<?php else : ?>
				<?php Elementor_AjaxLoader::get_products_carousel( $settings ); ?>
			<?php endif; ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}
}