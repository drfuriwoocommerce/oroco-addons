<?php

namespace FarmartAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Menu extends Widget_Base {

	public function get_name() {
		return 'oroco-menu-widget';
	}

	public function get_title() {
		return esc_html__( 'Menu', 'oroco' );
	}

	public function get_icon() {
		return 'icon-menu';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'oroco' ];
	}

	public function get_script_depends() {
		return [
			'oroco-elementor'
		];
	}

	protected $nav_menu_index = 1;

	protected function get_nav_menu_index() {
		return $this->nav_menu_index ++;
	}

	private function get_available_menus() {
		$menus = wp_get_nav_menus();

		$options = [];

		foreach ( $menus as $menu ) {
			$options[ $menu->slug ] = $menu->name;
		}

		return $options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'menu_department_section',
			[
				'label' => esc_html__( 'Menu', 'oroco' ),
			]
		);

		$menus = $this->get_available_menus();

		if ( ! empty( $menus ) ) {
			$this->add_control(
				'menu',
				[
					'label'        => __( 'Menu', 'oroco' ),
					'type'         => Controls_Manager::SELECT,
					'options'      => $menus,
					'default'      => array_keys( $menus )[0],
					'save_default' => true,
					'separator'    => 'after',
					'description'  => sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'oroco' ), admin_url( 'nav-menus.php' ) ),
				]
			);
		} else {
			$this->add_control(
				'menu',
				[
					'type'            => Controls_Manager::RAW_HTML,
					'raw'             => '<strong>' . __( 'There are no menus in your site.', 'oroco' ) . '</strong><br>' . sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'oroco' ), admin_url( 'nav-menus.php?action=edit&menu=0' ) ),
					'separator'       => 'after',
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
				]
			);
		}

		$this->add_responsive_control(
			'general_align',
			[
				'label'                => esc_html__( 'Horizontal Position', 'oroco' ),
				'type'                 => Controls_Manager::CHOOSE,
				'label_block'          => false,
				'options'              => [
					'default'   => [
						'title' => esc_html__( 'Default', 'oroco' ),
						'icon'  => 'eicon-ellipsis-h',
					],
					'flex-start'   => [
						'title' => esc_html__( 'Left', 'oroco' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'oroco' ),
						'icon'  => 'eicon-h-align-center',
					],
					'flex-end'  => [
						'title' => esc_html__( 'Right', 'oroco' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'              => 'left',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget' => 'justify-content: {{VALUE}};',
				],
				'separator'       => 'after',
			]
		);

		$this->end_controls_section();

		$this->section_style();

	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_menu_style();
		$this->section_submenu_style();
		$this->section_line_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function section_menu_style() {
		$this->start_controls_section(
			'section_menu_style',
			[
				'label' => __( 'Menu', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'menu_horizontal',
			[
				'label'     => esc_html__( 'Horizontal Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a' => 'margin-right: {{SIZE}}{{UNIT}};margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'menu_vertical',
			[
				'label'     => esc_html__( 'Vertical Spacing', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a' => 'padding-top: {{SIZE}}{{UNIT}};padding-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_menu_typography',
				'selector' => '{{WRAPPER}} .oroco-menu-widget > ul > li > a',
			]
		);

		$this->add_control(
			'section_item_border',
			[
				'label'        => __( 'Border', 'oroco' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'oroco' ),
				'label_on'     => __( 'Custom', 'oroco' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'item_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'oroco' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'oroco' ),
					'dashed' => esc_html__( 'Dashed', 'oroco' ),
					'solid'  => esc_html__( 'Solid', 'oroco' ),
					'none'   => esc_html__( 'None', 'oroco' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'item_border_width',
			[
				'label'     => esc_html__( 'Border Width', 'oroco' ),
				'type'      => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'item_border_radius',
			[
				'label'     => esc_html__( 'Border Radius', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);
		$this->end_popover();

		$this->start_controls_tabs( 'settings_menu_item' );

		$this->start_controls_tab( 'style_item_normal', [ 'label' => esc_html__( 'Normal', 'oroco' ) ] );
		$this->add_control(
			'menu_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a'     => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'menu_hover_color',
			[
				'label'     => __( 'Hover Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a:hover'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'item_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget .fm-nav-menu > li > a' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_item_active', [ 'label' => esc_html__( 'Active', 'oroco' ) ] );

		$this->add_control(
			'menu_active_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget li.current-menu-item > a'     => 'color: {{VALUE}} ! important;',
					'{{WRAPPER}} .oroco-menu-widget li:hover > a'     => 'color: {{VALUE}} ! important;',
				],
			]
		);

		$this->add_control(
			'item_border_color_active',
			[
				'label'     => esc_html__( 'Border Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget li.current-menu-item > a' => 'border-color: {{VALUE}} ',
					'{{WRAPPER}} .oroco-menu-widget li:hover > a' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_submenu_style() {
		$this->start_controls_section(
			'section_submenu_style',
			[
				'label' => __( 'Sub Menu', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'settings_submenu' );

		$this->start_controls_tab( 'style_submenu_wrapper', [ 'label' => esc_html__( 'Wrapper', 'oroco' ) ] );

		$this->add_responsive_control(
			'submenu_wrapper_padding',
			[
				'label'      =>esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'drmenu_bk_color',
			[
				'label'     => __( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu'     => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .oroco-menu-widget li.dropdown > a:before'     => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_submenu_item', [ 'label' => esc_html__( 'Item', 'oroco' ) ] );

		$this->add_responsive_control(
			'submenu_padding',
			[
				'label'      =>esc_html__( 'Padding', 'oroco' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu > li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_submenu_typography',
				'selector' => '{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu > li a',
			]
		);

		$this->add_control(
			'submenu_color',
			[
				'label'     => __( 'Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu > li a'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'submenu_active_color',
			[
				'label'     => __( 'Active Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu li.current-menu-item > a'     => 'color: {{VALUE}} ! important;',
					'{{WRAPPER}} .oroco-menu-widget ul.dropdown-submenu li:hover > a'     => 'color: {{VALUE}} ! important;',
				],
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Line
	 */
	protected function section_line_style() {
		$this->start_controls_section(
			'section_line_style',
			[
				'label' => __( 'Line', 'oroco' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'line_width',
			[
				'label'      => __( 'Width', 'oroco' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 5,
					],
				],
				'default'    => [],
				'selectors'  => [
					'{{WRAPPER}} .oroco-menu-widget ul.fm-nav-menu > li:after' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_height',
			[
				'label'     => __( 'Height', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
					'%'  => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget ul.fm-nav-menu > li:after' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'line_background_color',
			[
				'label'     => __( 'Background Color', 'oroco' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .oroco-menu-widget ul.fm-nav-menu > li:after' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$class = [
			'oroco-menu-widget',
			'main-navigation'

		];

		$this->add_render_attribute( 'wrapper', 'class',$class );

		$args = [
			'echo'        => false,
			'menu'        => $settings['menu'],
			'menu_class'  => 'or-nav-menu',
			'menu_id'     => 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
			'fallback_cb' => '__return_empty_string',
			'container'   => '',
			'walker'         => new \Oroco_Mega_Menu_Walker(),
		];

		// General Menu.
		$menu_html = wp_nav_menu( $args );

		if ( empty( $menu_html ) ) {
			return;
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$menu_html
		);
	}

	protected function _content_template() {

	}
}