<?php

use Elementor\Controls_Stack;
use Elementor\Controls_Manager;
use Elementor\Element_Column;
use Elementor\Core\Base\Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OR_Element_Column extends Module {

	public function __construct() {

		$this->add_actions();
	}

	public function get_name() {
		return 'or-content-align';
	}

	/**
	 * @param $element    Controls_Stack
	 * @param $section_id string
	 */
	public function register_controls( Controls_Stack $element, $section_id ) {
		if ( ! $element instanceof Element_Column ) {
			return;
		}
		$required_section_id = 'layout';

		if ( $required_section_id !== $section_id ) {
			return;
		}

		$element->add_responsive_control(
			'_or_column_min_width',
			[
				'label'        => esc_html__( 'Min Width (px)', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'device_args'     => [
					Controls_Stack::RESPONSIVE_DESKTOP => [
						'selectors' => [
							'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}}',
						],
					],
					Controls_Stack::RESPONSIVE_TABLET => [
						'selectors' => [
							'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}}',
						],
					],
					Controls_Stack::RESPONSIVE_MOBILE => [
						'selectors' => [
							'{{WRAPPER}}' => 'min-width: {{SIZE}}{{UNIT}}',
						],
					],
				],
				'separator' => 'before',
			]
		);

		$element->add_control(
			'_or_content_align',
			[
				'label'        => esc_html__( 'Content Align', 'oroco' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'vertical'   => [
						'title' => esc_html__( 'Vertical', 'oroco' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
					'horizontal' => [
						'title' => esc_html__( 'Horizontal', 'oroco' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
				],
				'default'      => 'vertical',
				'prefix_class' => 'or-flex-column-',
			]
		);

		$element->update_control(
			'content_position',
			['prefix_class' => 'or-column-items-',]
		);

		$element->add_control(
			'responsive_settings_custom',
			[
				'label' => __( 'Custom Responsive [ Column Width (%) ]  ', 'oroco' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$element->add_control(
			'_or_custom_responsive_1',
			[
				'label'        => esc_html__( 'From 1367px', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => '%',
				],
				'size_units' => ['%'],
				'selectors' => [
							'@media(min-width:1367px){{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} }',
				],
			]
		);

		$element->add_control(
			'_or_custom_responsive_2',
			[
				'label'        => esc_html__( 'From 1200px', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => '%',
				],
				'size_units' => ['%'],
				'selectors' => [
							'@media(min-width:1200px) and (max-width:1366px) {{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} }',
				],
			]
		);

		$element->add_control(
			'_or_custom_responsive_3',
			[
				'label'        => esc_html__( 'From 1025px ', 'oroco' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => '%',
				],
				'size_units' => ['%'],
				'selectors' => [
							'@media(min-width:1025px) and (max-width:1199px) {{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} }',
				],
			]
		);

		// $element->add_responsive_control(
		// 	'_or_custom_responsive_4',
		// 	[
		// 		'label'        => esc_html__( 'Column Width(%)', 'oroco' ),
		// 		'type'      => Controls_Manager::SLIDER,
		// 		'default'   => [
		// 			'unit' => '%',
		// 		],
		// 		'size_units' => ['%'],
		// 		'device_args'     => [
		// 			Controls_Stack::RESPONSIVE_DESKTOP => [
		// 				'selectors' => [
		// 					//'@media(min-width:1200px) and (max-width:1366px) {{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} }',
		// 					'{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} ',
		// 				],
		// 			],
		// 			Controls_Stack::RESPONSIVE_TABLET => [
		// 				'selectors' => [
		// 					'@media(min-width:1025px) and (max-width:1199px) {{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} }',
		// 				],
		// 			],
		// 			Controls_Stack::RESPONSIVE_MOBILE => [
		// 				'selectors' => [
		// 					'@media(min-width:1025px) and (max-width:1199px) {{{WRAPPER}}.elementor-column' => 'width: {{SIZE}}{{UNIT}} }',
		// 				],
		// 			],
		// 		],

		// 	]
		// );

	}

	protected function add_actions() {
		add_action( 'elementor/element/before_section_end', [ $this, 'register_controls' ], 10, 2 );
	}
}

new OR_Element_Column();
