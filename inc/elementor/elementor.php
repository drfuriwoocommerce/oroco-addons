<?php

namespace OrocoAddons;

use Elementor\Core\Responsive\Responsive;

/**
 * Integrate with Elementor.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor {
	/**
	 * Instance
	 *
	 * @access private
	 */
	private static $_instance = null;


	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @return Farmart_Addons_Elementor An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();
	}

	/**
	 * Auto load widgets
	 */
	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		if ( false === strpos( $class, 'Widgets' ) ) {
			return;
		}

		$path     = explode( '\\', $class );
		$filename = strtolower( array_pop( $path ) );

		$folder = array_pop( $path );

		if ( ! in_array( $folder, array( 'Widgets' ) ) ) {
			return;
		}

		$filename = str_replace( '_', '-', $filename );
		$filename = OROCO_ADDONS_DIR . 'inc/elementor/widgets/' . $filename . '.php';

		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	/**
	 * Includes files which are not widgets
	 */
	private function _includes() {
		include_once( OROCO_ADDONS_DIR . 'inc/elementor/widgets/ajaxloader.php' );
	}

	/**
	 * Hooks to init
	 */
	protected function add_actions() {
		if ( ! empty( $_REQUEST['action'] ) && 'elementor' === $_REQUEST['action'] && is_admin() ) {
			add_action( 'init', [ $this, 'register_wc_hooks' ], 5 );
		}

		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'styles' ] );
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'scripts' ] );

		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

		add_action( 'elementor/elements/categories_registered', [ $this, 'add_category' ] );

		add_action( 'post_class', [ $this, 'get_product_classes' ], 20, 3 );

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles' ] );

		// Products without Load more button
		add_action( 'wc_ajax_nopriv_or_elementor_load_products', [ $this, 'elementor_load_products' ] );
		add_action( 'wc_ajax_or_elementor_load_products', [ $this, 'elementor_load_products' ] );

		// Products with Load more button
		add_action( 'wc_ajax_nopriv_oroco_ajax_load_products', [ $this, 'ajax_load_products' ] );
		add_action( 'wc_ajax_oroco_ajax_load_products', [ $this, 'ajax_load_products' ] );

	}

	public function enqueue_styles() {
		\Elementor\Plugin::$instance->frontend->enqueue_styles();
	}


	public function register_wc_hooks() {
		if( ! class_exists('WooCommerce') ) {
			return;
		}
		wc()->frontend_includes();
	}

	public function get_product_classes( $classes, $class, $post_id ) {
		if ( is_admin() && \Elementor\Plugin::$instance->preview->is_preview_mode() ) {
			$post      = get_post( $post_id );
			$classes[] = $post->post_type;
		}

		return $classes;
	}

	/**
	 * Register styles
	 */
	public function styles() {

		wp_register_style( 'mapbox', OROCO_ADDONS_URL . 'assets/css/mapbox.css', array(), '1.0' );
		wp_register_style( 'mapboxgl', OROCO_ADDONS_URL . 'assets/css/mapbox-gl.css', array(), '1.0' );

	}

	/**
	 * Register after scripts
	 */
	public function scripts() {
		wp_register_script( 'oroco-elementor', OROCO_ADDONS_URL . '/assets/js/elementor.js', array( 'jquery' ), '20170530', true );

		wp_register_script( 'mapbox', OROCO_ADDONS_URL  . '/assets/js/plugins/mapbox.min.js', array(), '1.0', true );
		wp_register_script( 'mapboxgl', OROCO_ADDONS_URL  . '/assets/js/plugins/mapbox-gl.min.js', array(), '1.0', true );
		wp_register_script( 'mapbox-sdk', OROCO_ADDONS_URL  . '/assets/js/plugins/mapbox-sdk.min.js', array(), '1.0', true );
		wp_register_script( 'parallax', OROCO_ADDONS_URL  . '/assets/js/plugins/parallax.js', array(), '1.0', true );

		wp_register_script( 'isInViewport', OROCO_ADDONS_URL . '/assets/js/plugins/isInViewport.min.js', array(), '1.1.0', true );


		wp_localize_script(
			'oroco-elementor', 'or_elementor_data', array(
			'ajax_url' => $this->get_endpoint( '%%endpoint%%' ),
		) );

	}

	public function get_endpoint( $request = '' ) {
		return esc_url_raw( apply_filters( 'woocommerce_ajax_get_endpoint', add_query_arg( 'wc-ajax', $request, remove_query_arg( array(
			'remove_item',
			'add-to-cart',
			'added-to-cart',
			'order_again',
			'_wpnonce'
		), home_url( '/', 'relative' ) ) ), $request ) );
	}

	/**
	 * Init Widgets
	 */
	public function init_widgets() {
		$widgets_manager = \Elementor\Plugin::instance()->widgets_manager;

		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Portfolio_Carousel() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Newsletter() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Text_List() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Instagram_Grid() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Instagram_Carousel() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Links_List() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Main_Menu() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Team_Grid() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Faq() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Icon_Box() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Testimonials_Carousel() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Image_Carousel() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Map() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Contact_Form_7() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Button() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Blog() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Tab() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Banner_Video() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Image_Box() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Banner_Small() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Banner_Medium_2() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Banner_Large() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Category_Banner() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Product() );
		
		if ( class_exists( 'WooCommerce' ) ) {
			$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Product_Categories_Masonry() );
			$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Products_Grid() );
			$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Products_Tab() );
			$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Products_Metro() );
			$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Products_Carousel() );
			$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Products_List() );
		}
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Slides() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Slides_2() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Banners_Parallax() );
		$widgets_manager->register_widget_type( new \OrocoAddons\Elementor\Widgets\Social() );

	}

	/**
	 * Add Farmart category
	 */
	public function add_category( $elements_manager ) {

		$elements_manager->add_category(
			'oroco',
			[
				'title' => esc_html__( 'Oroco', 'oroco' )
			]
		);
	}

	public static function get_asset_url( $filename, $ext_type = 'css', $add_suffix = true ) {

		$url = OROCO_ADDONS_URL . 'assets/' . $ext_type . '/' . $filename;

		if ( $add_suffix ) {
			$url .= '.min';
		}

		return $url . '.' . $ext_type;
	}

	/**
	 * Retrieve the list of taxonomy
	 *
	 * @return array Widget categories.
	 */
	public static function get_taxonomy( $taxonomy = 'product_cat' ) {

		$output = array();

		$categories = get_categories(
			array(
				'taxonomy' => $taxonomy
			)
		);

		foreach ( $categories as $category ) {
			$output[$category->slug] = $category->name;
		}

		return $output;
	}

	/**
	 * @param array $settings
	 *
	 * @return array Slick Options
	 */
	public static function get_data_slick( $settings = array() ) {
		$carousel_settings = [];

		if ( isset($settings['navigation']) ) {
			$show_dots   = ( in_array( $settings['navigation'], [ 'dots', 'both' ] ) );
			$show_arrows = ( in_array( $settings['navigation'], [ 'arrows', 'both' ] ) );

			$carousel_settings['arrows'] = $show_arrows;
			$carousel_settings['dots']   = $show_dots;
		}
		
		if ( isset($settings['autoplay']) ) {
			$carousel_settings['autoplay'] = ( 'yes' === $settings['autoplay'] );
		}

		if ( isset($settings['infinite']) ) {
			$carousel_settings['infinite'] = ( 'yes' === $settings['infinite'] );
		}

		if ( isset($settings['autoplay_speed']) ) {
			$carousel_settings['autoplaySpeed'] = absint( $settings['autoplay_speed'] );
		}

		if ( isset($settings['speed']) ) {
			$carousel_settings['speed'] = absint( $settings['speed'] );
		}

		if ( isset($settings['rows']) && $settings['rows'] ) {
			$carousel_settings['rows'] = absint( $settings['rows'] );
		}

		if ( isset($settings['slidesToShow']) && $settings['slidesToShow'] ) {
			$carousel_settings['slidesToShow'] = absint( $settings['slidesToShow'] );
		}

		if ( isset($settings['slidesToScroll']) && $settings['slidesToScroll'] ) {
			$carousel_settings['slidesToScroll'] = absint( $settings['slidesToScroll'] );
		}

		$is_rtl = is_rtl();
		$carousel_settings['rtl'] = $is_rtl;

		// Responsive
		$responsive_args = [];

		$breakpoints = Responsive::get_breakpoints();

		// Tablet Handler
		$tablet_params = [];
		$tablet_breakpoint = $breakpoints['lg'] - 1;
		if ( isset($settings['navigation_tablet']) && $settings['navigation_tablet'] ) {
			$show_dots_tablet = ( in_array( $settings['navigation_tablet'], [ 'dots', 'both' ] ) );
			$show_arrows_tablet = ( in_array( $settings['navigation_tablet'], [ 'arrows', 'both' ] ) );

			$tablet_params['arrows'] = $show_arrows_tablet;
			$tablet_params['dots']   = $show_dots_tablet;
		}

		if (isset($settings['slidesToShow_tablet']) && $settings['slidesToShow_tablet']) {
			$tablet_params['slidesToShow'] = absint( $settings['slidesToShow_tablet'] );
		}

		if (isset($settings['slidesToScroll_tablet']) && $settings['slidesToScroll_tablet']) {
			$tablet_params['slidesToScroll'] = absint( $settings['slidesToScroll_tablet'] );
		}

		if (isset($settings['rows_tablet']) && $settings['rows_tablet']) {
			$tablet_params['rows'] = absint( $settings['rows_tablet'] );
		}

		if ( ! empty($tablet_params) ) {
			$responsive_args[$tablet_breakpoint] = [
				'breakpoint' => absint($tablet_breakpoint),
				'settings'   => $tablet_params
			];
		}

		// Mobile Handler
		$mobile_params = [];
		$mobile_breakpoint = $breakpoints['md'] - 1;
		if ( isset($settings['navigation_mobile']) && $settings['navigation_mobile'] ) {
			$show_dots_mobile = ( in_array( $settings['navigation_mobile'], [ 'dots', 'both' ] ) );
			$show_arrows_mobile = ( in_array( $settings['navigation_mobile'], [ 'arrows', 'both' ] ) );

			$mobile_params['arrows'] = $show_arrows_mobile;
			$mobile_params['dots']   = $show_dots_mobile;
		}
		
		if (isset($settings['slidesToShow_mobile']) && $settings['slidesToShow_mobile']) {
			$mobile_params['slidesToShow'] = absint( $settings['slidesToShow_mobile'] );
		}

		if (isset($settings['slidesToScroll_mobile']) && $settings['slidesToScroll_mobile']) {
			$mobile_params['slidesToScroll'] = absint( $settings['slidesToScroll_mobile'] );
		}

		if (isset($settings['rows_mobile']) && $settings['rows_mobile']) {
			$mobile_params['rows'] = absint( $settings['rows_mobile'] );
		}

		if ( ! empty($mobile_params) ) {
			$responsive_args[$mobile_breakpoint] = [
				'breakpoint' => absint($mobile_breakpoint),
				'settings'   => $mobile_params
			];
		}

		// Advance Settings
		if ( isset($settings['carousel_responsive_settings']) && ! empty($settings['carousel_responsive_settings']) ) {
			
			$responsive_settings = $settings['carousel_responsive_settings'];
			
			foreach ($responsive_settings as $setting) {
				$params = [];

				if ( isset($setting['responsive_navigation']) && $setting['responsive_navigation'] ) {
					$show_dots_res = ( in_array( $setting['responsive_navigation'], [ 'dots', 'both' ] ) );
					$show_arrows_res = ( in_array( $setting['responsive_navigation'], [ 'arrows', 'both' ] ) );

					$params['arrows'] = $show_arrows_res;
					$params['dots']   = $show_dots_res;
				}
				
				if (isset($setting['responsive_slidesToShow']) && $setting['responsive_slidesToShow']) {
					$params['slidesToShow'] = absint( $setting['responsive_slidesToShow'] );
				}

				if (isset($setting['responsive_slidesToScroll']) && $setting['responsive_slidesToScroll']) {
					$params['slidesToScroll'] = absint( $setting['responsive_slidesToScroll'] );
				}

				if (isset($setting['responsive_rows']) && $setting['responsive_rows']) {
					$params['rows'] = absint( $setting['responsive_rows'] );
				}
				
				if ( isset($setting['responsive_breakpoint']) && $setting['responsive_breakpoint'] ) {
					$responsive_args[$setting['responsive_breakpoint']] = [
						'breakpoint' => absint($setting['responsive_breakpoint']),
						'settings'   => $params
					];
				}
			}
		}

		if ( ! empty($responsive_args) ) {
			krsort($responsive_args);

			foreach ($responsive_args as $value) {
				$carousel_settings['responsive'][] = $value;
			}
		}

		return $carousel_settings;
	}

	public static function oroco_get_portfolio( $atts ) {

		if ($atts['source'] == 'default'){
			$query_args = array(
				'post_status' => 'publish',
				'orderby' => $atts['orderby'],
				'order' => $atts['order'],
				'ignore_sticky_posts' => true,
				'posts_per_page' => $atts['limit'],
				'post_type' => 'portfolio',
			);

			if ( ! empty( $atts['portfolio_cats'] ) ) {
				$query_args['tax_query'] = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'portfolio_category',
						'field'    => 'slug',
						'terms'    => $atts['portfolio_cats'],
						'operator' => 'IN',
					),
				);
			}
		} else {
			$query_args = array(
				'post__in' => explode(",",$atts['ids']),
				'post_type' => 'portfolio',
			);
		}


		$query = new \WP_Query($query_args);
		$html =array();

		while ($query->have_posts()) : $query->the_post();
			ob_start();
			get_template_part( 'template-parts/content', 'portfolio' );
			$html[] = ob_get_clean();
		endwhile;
		wp_reset_postdata();

		return implode('', $html );
	}

	/**
	* Get Instagram images
	*
	* @param int $limit
	*
	* @return array|\WP_Error
	*/
	public static function instagram_get_photos_by_token( $limit,$access_token ) {

		if ( empty( $access_token ) ) {
			return new \WP_Error( 'instagram_no_access_token', esc_html__( 'No access token', 'oroco' ) );
		}
		$user = self::oroco_get_instagram_user($access_token);
		if ( ! $user || is_wp_error( $user ) ) {
			return $user;
		}
		if (isset($user['error'])) {
			return new \WP_Error( 'instagram_no_images', esc_html__( 'Instagram did not return any images. Please check your access token', 'oroco' ) );
		} else {
			$transient_key = 'oroco_instagram_photos_' . sanitize_title_with_dashes( $user['username'] . '__' . $limit );
			$images = get_transient( $transient_key );

			$images = array();
			$next = false;
			while ( count( $images ) < $limit ) {
				if ( ! $next ) {
					$fetched = self::oroco_fetch_instagram_media( $access_token );
				} else {
					$fetched = self::oroco_fetch_instagram_media( $next );				
				}
				if ( is_wp_error( $fetched ) ) {
					break;
				}

				$images = array_merge( $images, $fetched['images'] );
				$next = $fetched['paging']['cursors']['after'];
			}
			
			if ( ! empty( $images ) ) {
				set_transient( $transient_key, $images, 2 * 3600 ); // Cache for 2 hours.
			}
			if ( ! empty( $images ) ) {

				return $images;

			} else {
				return new \WP_Error( 'instagram_no_images', esc_html__( 'Instagram did not return any images.', 'oroco' ) );
			}
		}
	}

	/**
	* Fetch photos from Instagram API
	*
	* @param  string $access_token
	* @return array
	*/
	public static function oroco_fetch_instagram_media( $access_token ) {
		$url = add_query_arg( array(
			'fields'       => 'id,caption,media_type,media_url,permalink,thumbnail_url',
			'access_token' => $access_token,
		), 'https://graph.instagram.com/me/media' );

		$remote = wp_remote_retrieve_body( wp_remote_get( $url ) );
		$data   = json_decode( $remote, true );

		$images = array();
		if ( isset( $data['error'] ) ) {
			return new \WP_Error( 'instagram_error', $data['error']['message'] );
		} else {
			foreach ( $data['data'] as $media ) {
				
				 $images[] = array(
					'type'    => $media['media_type'],
					'caption' => isset( $media['caption'] ) ? $media['caption'] : $media['id'],
					'link'    => $media['permalink'],
					'images'  => array(
						'thumbnail' => $media['media_url'],
						'original'  => $media['media_url'],
					),
				);
			
			}
		}
		return array(
			'images' => $images,
			'paging' => $data['paging'],
		);
	}
	/**
	* Get user data
	*
	* @return bool|\WP_Error|array
	*/
	public static function oroco_get_instagram_user($access_token) {
		if ( empty( $access_token ) ) {
			return new \WP_Error( 'no_access_token', esc_html__( 'No access token', 'oroco' ) );
		}
		$user = get_transient( 'oroco_instagram_user_' . $access_token );
		if ( false === $user ) {
			$url  = add_query_arg( array( 'fields' => 'id,username', 'access_token' => $access_token ), 'https://graph.instagram.com/me' );
			$data = wp_remote_get( $url );
			$data = wp_remote_retrieve_body( $data );
			if ( ! $data ) {
				return new \WP_Error( 'no_user_data', esc_html__( 'No user data received', 'oroco' ) );
			}
			$user = json_decode( $data, true );
			if ( ! empty( $data ) ) {
				set_transient( 'oroco_instagram_user', $user, 2592000 ); // Cache for one month.
			}
		}
		return $user;
	}

	public static function oroco_get_blog( $atts ) {

		if ($atts['source'] == 'default'){
			$query_args = array(
				'post_status' => 'publish',
				'orderby' => $atts['orderby'],
				'order' => $atts['order'],
				'ignore_sticky_posts' => true,
				'posts_per_page' => $atts['limit'],
			);

			if ( ! empty( $atts['blog_cats'] ) ) {
				$query_args['tax_query'] = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'category',
						'field'    => 'slug',
						'terms'    => $atts['blog_cats'],
						'operator' => 'IN',
					),
				);
			}
		} else {
			$query_args['post__in'] =  explode(",",$atts['ids']) ;
		}

		$query = new \WP_Query($query_args);
		$html =array();
		
		global $oroco_blog_shortcode;

		while ($query->have_posts()) : $query->the_post();

			$oroco_blog_shortcode['style'] = 'grid';

			$GLOBALS['blog_shortcode'] = 'blog_shortcode';

			ob_start();
			get_template_part( 'template-parts/content' );
			$html[] = ob_get_clean();
		endwhile;
		wp_reset_postdata();

		return implode('', $html );
	}

	/**
	 * Load products
	 */
	public static function elementor_load_products() {

		$atts = array(
			'columns'      => isset( $_POST['columns'] ) ? intval( $_POST['columns'] ) : '',
			'products'     => isset( $_POST['products'] ) ? $_POST['products'] : '',
			'order'        => isset( $_POST['order'] ) ? $_POST['order'] : '',
			'orderby'      => isset( $_POST['orderby'] ) ? $_POST['orderby'] : '',
			'per_page'     => isset( $_POST['per_page'] ) ? intval( $_POST['per_page'] ) : '',
			'product_cats' => isset( $_POST['product_cats'] ) ? $_POST['product_cats'] : '',
		);

		$products = self::get_products( $atts );

		wp_send_json_success( $products );
	}

	/**
	 * Load products
	 */
	public static function ajax_load_products() {
		check_ajax_referer( 'oroco_get_products', 'nonce' );

		$settings = $_POST['settings'];

		$atts = [
			'per_page'       => intval( $settings['per_page'] ),
			'columns'        => intval( $settings['columns'] ),
			'product_cats'   => $settings['product_cats'],
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'page'           => isset( $_POST['page'] ) ? $_POST['page'] : 1,
			'load_more'      => isset( $_POST['load_more'] ) ? $_POST['load_more'] : '',
			'load_more_text' => isset( $_POST['text'] ) ? $_POST['text'] : '',
			'products'       => isset( $_POST['type'] ) ? $_POST['type'] : '',
		];

		$products = self::get_products_loop( $atts );

		wp_send_json_success( $products );
	}

	/**
	 * Get the product deals
	 *
	 * @return string.
	 */
	public static function get_products_loop( $settings, $template = 'product' ) {
		$per_page   = intval( $settings['per_page'] );
		$query_args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'posts_per_page'      => $per_page,
			'ignore_sticky_posts' => true,
			'no_found_rows'       => true,
			'meta_query'          => WC()->query->get_meta_query(),
			'tax_query'           => WC()->query->get_tax_query(),
			'orderby'             => $settings['orderby'],
			'order'               => $settings['order'],
			'fields'              => 'ids',
		);

		if ( isset( $settings['load_more'] ) && $settings['load_more'] == 'yes' ) {
			$query_args['paged']         = isset( $settings['page'] ) ? absint( $settings['page'] ) : 1;
			$query_args['no_found_rows'] = false;
		}

		if ( isset( $settings['product_cats'] ) && $settings['product_cats'] ) {
			$query_args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'slug',
					'terms'    => explode( ',', $settings['product_cats'] ),
				)
			);
		}

		if ( isset( $settings['product_brand'] ) && $settings['product_brand'] ) {
			$query_args['tax_query'][] = array(
				array(
					'taxonomy' => 'product_brand',
					'field'    => 'slug', //This is optional, as it defaults to 'term_id'
					'terms'    => explode( ',', $settings['product_brand'] ),
					'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				),
			);
		}

		$type = '';

		if ( isset( $settings['products'] ) ) {
			$type = $settings['products'];

			switch ( $settings['products'] ) {
				case 'recent':
					$query_args['order']   = $settings['order'] ? $settings['order'] : 'DESC';
					$query_args['orderby'] = $settings['orderby'] ? $settings['orderby'] : 'date';

					unset( $query_args['update_post_meta_cache'] );
					break;

				case 'featured':
					if ( version_compare( WC()->version, '3.0.0', '<' ) ) {
						$query_args['meta_query'][] = array(
							'key'   => '_featured',
							'value' => 'yes',
						);
					} else {
						$query_args['tax_query'][] = array(
							'taxonomy' => 'product_visibility',
							'field'    => 'name',
							'terms'    => 'featured',
							'operator' => 'IN',
						);
					}

					unset( $query_args['update_post_meta_cache'] );
					break;

				case 'sale':
					$query_args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
					break;

				case 'best_selling':
					$query_args['meta_key'] = 'total_sales';
					$query_args['orderby']  = 'meta_value_num';
					$query_args['order']    = $settings['order'] ? $settings['order'] : 'DESC';
					break;

				case 'top_rated':
					$query_args['meta_key'] = '_wc_average_rating';
					$query_args['orderby']  = 'meta_value_num';
					$query_args['order']    = $settings['order'] ? $settings['order'] : 'DESC';
					break;
			}
		}


		$query             = json_encode( $query_args );
		$query_hash   = md5( $query );
		$cached_query = (array) get_transient( 'or_woocommerce_cached_products' );

		if ( ! isset( $cached_query[ $query_hash ] ) ) {
			$results                     = new \WP_Query( $query_args );
			$cached_query[ $query_hash ] = $results;
			set_transient( 'or_woocommerce_cached_products', $cached_query, DAY_IN_SECONDS );
		}

		$products = $cached_query[ $query_hash ];

		if ( ! $products->have_posts() ) {
			return '';
		}

		global $woocommerce_loop;

		$woocommerce_loop['columns'] = isset( $settings['columns'] ) && $settings['columns'] ? intval( $settings['columns'] ) : 1;

		ob_start();

		self::products_loop( $products->posts, $template );

		$output = '<div class="woocommerce">' . ob_get_clean() . '</div>';

		$total_page = $products->max_num_pages;

		$params = array(
			'per_page'     => intval( $settings['per_page'] ),
			'columns'      => isset( $settings['columns'] ) && $settings['columns'] ? intval( $settings['columns'] ) : 1,
			'product_cats' => isset( $settings['product_cats'] ) && $settings['product_cats'] ? $settings['product_cats'] : '',
			'orderby'      => isset( $settings['orderby'] ) && $settings['orderby'] ? $settings['orderby'] : '',
			'order'        => isset( $settings['order'] ) && $settings['order'] ? $settings['order'] : '',
			'page'         => isset( $settings['page'] ) && $settings['page'] ? $settings['page'] : 1,
		);

		if ( isset( $settings['load_more'] ) && ( 'yes' == $settings['load_more'] ) && $total_page > 1 ) {
			if ( $query_args['paged'] < $total_page ) {
				$output .= sprintf(
					'<div class="load-more">
						<a href="#" class="ajax-load-products" data-page="%s" data-type="%s" data-load_more="%s" data-text="%s" data-settings="%s" data-nonce="%s" rel="nofollow">
							<span class="button-text oroco-button">%s</span>
							<span class="oroco-loading"></span>
						</a>
					</div>',
					esc_attr( $query_args['paged'] + 1 ),
					esc_attr( $type ),
					esc_attr( $settings['load_more'] ),
					esc_attr( $settings['load_more_text'] ),
					esc_attr( json_encode( $params ) ),
					esc_attr( wp_create_nonce( 'oroco_get_products' ) ),
					$settings['load_more_text']
				);
			}
		}

		return $output;
	}

	/**
	 * Product Loop
	 */
	public static function get_products( $atts ) {
		$params   = '';
		$order    = $atts['order'];
		$order_by = $atts['orderby'];
		if ( $atts['products'] == 'featured' ) {
			$params = 'visibility="featured"';
		} elseif ( $atts['products'] == 'best_selling' ) {
			$params = 'best_selling="true"';
		} elseif ( $atts['products'] == 'sale' ) {
			$params = 'on_sale="true"';
		} elseif ( $atts['products'] == 'recent' ) {
			$order    = $order ? $order : 'desc';
			$order_by = $order_by ? $order_by : 'date';
		} elseif ( $atts['products'] == 'top_rated' ) {
			$params = 'top_rated="true"';
		}

		$params .= ' columns="' . intval( $atts['columns'] ) . '" limit="' . intval( $atts['per_page'] ) . '" order="' . $order . '" orderby ="' . $order_by . '"';
		if ( ! empty( $atts['product_cats'] ) ) {
			$cats = $atts['product_cats'];
			if ( is_array( $cats ) ) {
				$cats = implode( ',', $cats );
			}

			$params .= ' category="' . $cats . '" ';
		}

		if ( ! empty( $atts['product_tags'] ) ) {
			$params .= ' tag="' . implode( ',', $atts['product_tags'] ) . '" ';
		}

		if ( ! empty( $atts['ids'] ) ) {
			$params .= ' ids="' . $atts['ids'] . '" ';
		}

		return do_shortcode( '[products ' . $params . ']' );
	}

	public static function products_loop( $products_ids, $template = 'product' ) {
		update_meta_cache( 'post', $products_ids );
		update_object_term_cache( $products_ids, 'product' );

		$original_post = $GLOBALS['post'];

		woocommerce_product_loop_start();

		foreach ( $products_ids as $product_id ) {
			$GLOBALS['post'] = get_post( $product_id ); // WPCS: override ok.
			setup_postdata( $GLOBALS['post'] );
			wc_get_template_part( 'content', $template );
		}

		$GLOBALS['post'] = $original_post; // WPCS: override ok.
		woocommerce_product_loop_end();

		wp_reset_postdata();
		woocommerce_reset_loop();
	}

}

Elementor::instance();