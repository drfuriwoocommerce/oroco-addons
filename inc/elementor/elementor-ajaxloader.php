<?php

namespace OrocoAddons;

use OrocoAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor_AjaxLoader {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'wc_ajax_or_elementor_get_elements', [ $this, 'elementor_get_elements' ] );
	}

	public static function elementor_get_elements() {
		$output = '';

		if ( isset( $_POST['params'] ) && ! empty( $_POST['params'] ) ) {
			$params   = json_decode( stripslashes( $_POST['params'] ), true );
			$settings = array();
			foreach ( $params as $key => $value ) {
				$settings[ $key ] = $value;
			}

			$els = '';
			if ( isset( $_POST['element'] ) && ! empty( $_POST['element'] ) ) {
				$els = $_POST['element'];
			}

			if ( $els == 'productsGrid' ) {
				ob_start();
				self::get_products_grid( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsTab' ) {
				ob_start();
				self::get_products_tab( $settings );
				$output = ob_get_clean();
            } elseif ( $els == 'productsMetro' ) {
				ob_start();
				self::get_products_metro( $settings );
            } elseif ( $els == 'productsCarousel' ) {
				ob_start();
				self::get_products_carousel( $settings );
				$output = ob_get_clean();
            }
		}

		wp_send_json_success( $output );
		die();
	}
	
	public static function get_products_grid( $settings ) {
		$filter = [ ];
		if ( $settings['show_filter'] == 'yes' && ! empty( $settings['product_cats'] ) ) {
			$filter[] = '<ul class="product-filter">';

			$cats = explode( ',', $settings['product_cats'] );

			if ( empty($cats[0]) ) {
				array_shift($cats);
			}

			if ( $settings['show_all'] == 'yes' ) {
				$filter[] = sprintf( '<li class="" data-filter="%s">%s</li>', esc_attr( implode( ',', $cats ) ), $settings['show_all_text'] );
			} else {
				$settings['product_cats'] = $cats[0];
			}

			foreach ( $cats as $cat ) {
				$cat      = get_term_by( 'slug', $cat, 'product_cat' );
				if ( ! is_wp_error($cat) && $cat ) {
					$filter[] = sprintf( '<li class="" data-filter="%s">%s</li>', esc_attr( $cat->slug ), esc_html( $cat->name ) );
				}
			}

			$filter[] = '</ul>';
		}

		$settings['page'] = 1;
		$products         = Elementor::get_products( $settings );

        ?>
		<div class="product-grid__content">
			<?php echo implode( '', $filter ); ?>
			<div class="products-wrapper">
				<div class="or-loader">
					<div class="oroco-loading"></div>
				</div>
				<?php echo $products; ?>
			</div>
		</div>
		<?php
	}

	public static function get_products_tab( $settings ) {

		$output      = [ ];
		$header_tabs = [ ];

		$header_tabs[] = '<div class="tabs-header-nav">';

		$tab_content = [ ];

		$header_tabs[] = '<ul class="tabs-nav">';
		$i             = 0;
		if ( $settings['product_tabs_source'] == 'special_products' ) {
			$tabs = $settings['special_products_tabs'];

			if ( $tabs ) {
				foreach ( $tabs as $index => $item ) {
					$button_view = '';

					if ( isset( $item['title'] ) ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $item['tab_products'] ), esc_html( $item['title'] ) );
					}

					if ( $item['tab_button_text'] ) {
						$button_view = sprintf( '<div class="or-tabs-button">%s</div>', self::get_link_control( $index, $item['tab_button_link'], $item['tab_button_text'], [ 'class' => 'oroco-button' ] ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $item['tab_products'],
						'order'        => ! empty( $item['tab_order'] ) ? $item['tab_order'] : '',
						'orderby'      => ! empty( $item['tab_orderby'] ) ? $item['tab_orderby'] : '',
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $settings['product_cats'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf(
							'<div class="tabs-panel tabs-%s tab-loaded"><div class="tab-content">%s</div>%s</div>',
							esc_attr( $item['tab_products'] ),
							Elementor::get_products_loop( $tab_atts ),
							wp_kses_post( $button_view )
						);
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s tab-loaded"><div class="tab-content">%s</div>%s</div>',
								esc_attr( $item['tab_products'] ),
								Elementor::get_products_loop( $tab_atts ),
								wp_kses_post( $button_view )
							);
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="tab-content"><div class="oroco-loading"></div></div>%s</div>',
								esc_attr( $item['tab_products'] ),
								esc_attr( wp_json_encode( $tab_atts ) ),
								wp_kses_post( $button_view )
							);
						}
					}

					$i ++;
				}
			}
		} else {
			$cats = $settings['product_cats_tabs'];
			if ( $cats ) {
				foreach ( $cats as $tab ) {
					$term = get_term_by( 'slug', $tab['product_cat'], 'product_cat' );
					if ( ! is_wp_error( $term ) && $term ) {
						$header_tabs[] = sprintf( '<li><a href="#" data-href="%s">%s</a></li>', esc_attr( $tab['product_cat'] ), esc_html( $term->name ) );
					}

					$tab_atts = array(
						'columns'      => intval( $settings['slidesToShow'] ),
						'products'     => $settings['products'],
						'order'        => $settings['order'],
						'orderby'      => $settings['orderby'],
						'per_page'     => intval( $settings['per_page'] ),
						'product_cats' => $tab['product_cat'],
					);

					if ( $i == 0 ) {
						$tab_content[] = sprintf(
							'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
							esc_attr( $tab['product_cat'] ),
							Elementor::get_products_loop( $tab_atts )
						);
					} else {
						if ( $settings['lazy_load'] == 'yes' ) {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s tab-loaded">%s</div>',
								esc_attr( $tab['product_cat'] ),
								Elementor::get_products_loop( $tab_atts )
							);
						} else {
							$tab_content[] = sprintf(
								'<div class="tabs-panel tabs-%s" data-settings="%s"><div class="oroco-loading"></div></div>',
								esc_attr( $tab['product_cat'] ),
								esc_attr( wp_json_encode( $tab_atts ) )
							);
						}
					}

					$i ++;

				}
			}
		}

		$header_tabs[] = '</ul>';

		$header_tabs[] = '</div>';

		$output[] = sprintf( '<div class="tabs-header">%s</div>', implode( ' ', $header_tabs ) );
		$output[] = sprintf( '<div class="tabs-content">%s</div>', implode( ' ', $tab_content ) );

		echo implode( '', $output );
	}

	public static function get_products_metro( $settings ) {
		$output[] = '';

		if ($settings['source'] == 'default'){
			$query_args = array(
				'post_type'           => 'product',
				'post_status'         => 'publish',
				'posts_per_page'      => $settings['per_page'],
				'ignore_sticky_posts' => true,
				'no_found_rows'       => true,
				'fields'              => 'ids',
				'orderby'   		  => $settings['orderby'],
				'order'     		  => $settings['order'],
			);

			if ( ! empty( $settings['product_cats'] ) ) {
				$query_args['tax_query'] = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => $settings['product_cats'],
						'operator' => 'IN',
					),
				);
			}

			if ( function_exists( 'wc_get_product_visibility_term_ids' ) ) {
				$product_visibility_term_ids = wc_get_product_visibility_term_ids();
				$query_args['tax_query'][]         = array(
					'taxonomy' => 'product_visibility',
					'field'    => 'term_taxonomy_id',
					'terms'    => $product_visibility_term_ids['exclude-from-search'],
					'operator' => 'NOT IN',
				);
			}
		} else {
			$query_args = array(
				'post__in' 		=> explode(",",$settings['ids']),
				'post_type' 	=> 'product',
				'orderby'   	=> 'post__in',
			);
		}


		$products = new \WP_Query( $query_args );

		if ( ! $products->have_posts() ) {
			return '';
		}

		$output      = [];
		$i           = 0;
		$lines       = 5;
		$count       = $settings['per_page'];

		ob_start();

		if ( $products->have_posts() ) :

			while ( $products->have_posts() ) : $products->the_post();
				if ( $lines > 1 && $i % $lines == 0 ) {
					$output[] = '<li><ul class="products-metro-list layout-masonry">';
				}

				ob_start();
				wc_get_template_part( 'content', 'product-metro' );
				$output[] =  ob_get_clean();

				if ( $lines > 1 && $i % $lines == $lines - 1 ) {
					$output[] = '</ul></li>';
				} elseif ( $lines > 1 && $i == $count - 1 ) {
					$output[] = '</ul></li>';
				}

				$i ++;

			endwhile; // end of the loop.


		endif;

		wp_reset_postdata();

		$output[] = ob_get_clean();

		$products = sprintf( '<ul class="products">%s</ul>', implode( '', $output ) );
		
        ?>
			<div class="products-content"><?php echo $products; ?></div>
        <?php
	}

	public static function get_products_carousel( $settings ) {
        $settings['columns'] = intval( $settings['slidesToShow'] );
		$products            = Elementor::get_products( $settings );
		
		printf('<div class="products-content">%s</div>',$products);
	}

    /**
	 * Render link control output
	 *
	 * @param       $link_key
	 * @param       $url
	 * @param       $content
	 * @param array $attr
	 *
	 * @return string
	 */
	public static function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [ ];
		if ( isset( $url['url'] ) && $url['url'] ) {
			$attr_default['href'] = $url['url'];
		}

		if ( isset( $url['is_external'] ) && $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( isset( $url['nofollow'] ) && $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$tag = 'a';

		if ( empty( $attr['href'] ) ) {
			$tag = 'span';
		}

        $attributes = [];
        
        foreach( $attr as $name => $v ) {
            $attributes[] = $name . '="' . esc_attr( $v ) . '"';
        }
        
		return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, implode( ' ', $attributes ), $content );
	}
}

new Elementor_AjaxLoader();