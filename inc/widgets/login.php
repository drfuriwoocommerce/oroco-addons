<?php

class Oroco_Login_Widget extends WP_Widget {
	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Class constructor
	 * Set up the widget
	 *
	 * @return Oroco_Latest_Post_Widget
	 */
	function __construct() {
		$this->defaults = array(
			'title'      => '',
			'text'      => '',
		);

		parent::__construct(
			'login-user-widget',
			esc_html__( 'Oroco - Login', 'oroco' ),
			array(
				'classname'   => 'oroco-widget__login',
				'description' => esc_html__( 'Display login account', 'oroco' ),
			)
		);
	}

	/**
	 * Display widget
	 *
	 * @param array $args     Sidebar configuration
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		extract( $args );

		echo wp_kses_post($before_widget);

		if ( $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) ) {
			echo wp_kses_post($before_title) . $title . wp_kses_post($after_title);
		}

		?>

		<div class="header-account">
			<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( 'dashboard' ) ); ?>">
					<?php echo oroco_theme_svg('account'); ?>
				</a>

				<div class="account-links">
					<ul>
						<?php if ( defined( 'YITH_WCWL' ) ) : ?>
							<li class="account-link--wishlist">
								<a href="<?php echo esc_url( get_permalink( yith_wcwl_object_id( get_option( 'yith_wcwl_wishlist_page_id' ) ) ) ) ?>" class="underline-hover">
									<?php echo esc_html__( 'My wishlist', 'oroco' ); ?>
								</a>
							</li>
						<?php endif; ?>
						<li class="account-link--order">
							<a href="<?php echo wc_get_endpoint_url('orders', '', get_permalink(get_option('woocommerce_myaccount_page_id'))) ?>" class="underline-hover">
								<?php echo esc_html__( 'Orders history', 'oroco' ); ?>
							</a>
						</li>
						<li class="account-link--account">
							<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ) ?>" class="underline-hover">
								<?php echo esc_html__( 'Account setting', 'oroco' ); ?>
							</a>
						</li>
						<li class="account-link--logout">
							<a href="<?php echo wp_logout_url() ?>" class="underline-hover">
								<?php echo esc_html__( 'Logout', 'oroco' ); ?>
							</a>
						</li>
					</ul>
				</div>
			<?php else : ?>
				<a id="header-account-icon" class="header-account-icon" href="<?php echo esc_url( wc_get_account_endpoint_url( 'dashboard' ) ); ?>" >
					<?php echo oroco_theme_svg('account'); ?>
					<span class="header-account-icon__text"><?php echo $instance['text'] ?></span>
				</a>
			<?php endif; ?>
		</div>

		<?php

		echo wp_kses_post($after_widget);

	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings
	 * @param array $old_instance Old widget settings
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$new_instance['title']     = strip_tags( $new_instance['title'] );
		$new_instance['text']     = strip_tags( $new_instance['text'] );

		return $new_instance;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php esc_html_e( 'Login text', 'oroco' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['text'] ); ?>">
		</p>

		<?php
	}

}