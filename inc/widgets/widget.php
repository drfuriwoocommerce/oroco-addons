<?php
/**
 * Load and register widgets
 *
 * @package Oroco
 */


/**
 * Register widgets
 *
 * @return void
 */
function oroco_register_widgets() {
	require_once OROCO_ADDONS_DIR . '/inc/widgets/latest-posts.php';
	register_widget( 'Oroco_Latest_Post_Widget' );

	if ( class_exists( 'WC_Widget' ) ) {
		require_once OROCO_ADDONS_DIR . '/inc/widgets/login.php';
		register_widget( 'Oroco_Login_Widget' );
	}

	if ( class_exists( 'WOOCS' ) ) {
		require_once OROCO_ADDONS_DIR . '/inc/widgets/currencies.php';
		register_widget( 'Oroco_Currencies_Widget' );
	}

	$languages = function_exists( 'icl_get_languages' ) ? icl_get_languages() : array();

	if ( ! empty( $languages ) ) {
		require_once OROCO_ADDONS_DIR . '/inc/widgets/language.php';
		register_widget( 'Oroco_Language_Widget' );
	}

	if ( class_exists( 'WC_Widget' ) ) {
		require_once OROCO_ADDONS_DIR . '/inc/widgets/product-sort-by.php';
		require_once OROCO_ADDONS_DIR . '/inc/widgets/woo-attributes-filter.php';
		require_once OROCO_ADDONS_DIR . '/inc/widgets/product-tag.php';
		require_once OROCO_ADDONS_DIR . '/inc/widgets/product-categories.php';
		require_once OROCO_ADDONS_DIR . '/inc/widgets/filter-price-list.php';
		require_once OROCO_ADDONS_DIR . '/inc/widgets/widget-layered-nav-filters.php';

		register_widget( 'Oroco_Product_SortBy_Widget' );
		register_widget( 'Oroco_Widget_Attributes_Filter' );
		register_widget( 'Oroco_Widget_Product_Tag_Cloud' );
		register_widget( 'Oroco_Widget_Product_Categories' );
		register_widget( 'Oroco_Price_Filter_List_Widget' );
		register_widget( 'Oroco_Widget_Layered_Nav_Filters' );
	}
}

add_action( 'widgets_init', 'oroco_register_widgets' );