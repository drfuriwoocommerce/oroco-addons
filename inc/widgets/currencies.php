<?php

class Oroco_Currencies_Widget extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since  2.8.0
	 * @access public
	 */
	public function __construct() {

		$widget_ops  = array(
			'classname'   => 'widget_text oroco-widget__currency',
			'description' => esc_html__( 'Shows currency list by WooCommerce Currency Switcher plugin', 'oroco' ),
		);
		$control_ops = array( 'width' => 400, 'height' => 350 );
		parent::__construct( 'oroco-currency-widget', esc_html__( 'Oroco - Currency', 'oroco' ), $widget_ops, $control_ops );
	}

	/**
	 * Display widget
	 *
	 * @param array $args     Sidebar configuration
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		echo wp_kses_post($before_widget);

		?>

		<div class="oroco-widget__currencies-inner">
			<?php
			echo $this->woocs_currencies( $instance );
			echo $this->alg_currencies( $instance );
			oroco_theme_svg('arrow-bottom');
			?>
		</div>
		<?php

		echo wp_kses_post($after_widget);

	}

	function woocs_currencies( $settings ) {
		if ( ! class_exists( 'WOOCS' ) ) {
			return;
		}

		global $WOOCS;

		$key_cur = 'name';
		if ( $settings['currencies'] == 'description' ) {
			$key_cur = 'description';
		}

		$currencies    = $WOOCS->get_currencies();
		$currency_list = array();
		foreach ( $currencies as $key => $currency ) {
			if ( $WOOCS->current_currency == $key ) {
				array_unshift(
					$currency_list, sprintf(
						'<li class="actived"><a href="#" class="woocs_flag_view_item woocs_flag_view_item_current" data-currency="%s">%s</a></li>',
						esc_attr( $currency['name'] ),
						esc_html( $currency[$key_cur] )
					)
				);
			} else {
				$currency_list[] = sprintf(
					'<li><a href="#" class="woocs_flag_view_item" data-currency="%s">%s</a></li>',
					esc_attr( $currency['name'] ),
					esc_html( $currency[$key_cur] )
				);
			}
		}

		return sprintf(
			'<span class="current">%s</span>' .
			'<div class="currency-content"><ul>%s</ul></div>',
			$currencies[$WOOCS->current_currency][$key_cur],
			implode( "\n\t", $currency_list )
		);

	}

	function alg_currencies( $settings ) {
		if ( ! class_exists( 'Alg_WC_Currency_Switcher' ) ) {
			return '';
		}

		$function_currencies    = alg_get_enabled_currencies();
		$currencies             = get_woocommerce_currencies();
		$selected_currency      = alg_get_current_currency_code();
		$selected_currency_name = '';
		$currency_list          = array();
		$first_link             = '';
		foreach ( $function_currencies as $currency_code ) {
			if ( isset( $currencies[$currency_code] ) ) {

				$the_text = $this->alg_format_currency_switcher( $currencies[$currency_code], $currency_code, false );
				$the_name = $settings['currencies'] == 'description' ? $the_text : $currency_code;
				$the_link = '<li><a id="alg_currency_' . $currency_code . '" href="' . add_query_arg( 'alg_currency', $currency_code ) . '">' . $the_name . '</a></li>';
				if ( $currency_code != $selected_currency ) {
					$currency_list[] = $the_link;
				} else {
					$first_link             = $the_link;
					$selected_currency_name = $the_name;
				}
			}
		}
		if ( '' != $first_link ) {
			$currency_list = array_merge( array( $first_link ), $currency_list );
		}

		if ( ! empty( $currency_list ) && ! empty( $selected_currency_name ) ) {
			return sprintf(
				'<span class="current">%s</span>' .
				'<ul>%s</ul>',
				$selected_currency_name,
				implode( "\n\t", $currency_list )
			);
		}

		return '';

	}

	function alg_format_currency_switcher( $currency_name, $currency_code, $check_is_product = true ) {
		$product_price = '';
		if ( ! $check_is_product || ( $check_is_product && is_product() ) ) {
			$_product = wc_get_product();
			if ( $_product ) {
				$product_price = alg_get_product_price_html_by_currency( $_product, $currency_code );
			}
		}
		$replaced_values = array(
			'%currency_name%'   => $currency_name,
			'%currency_code%'   => $currency_code,
			'%currency_symbol%' => get_woocommerce_currency_symbol( $currency_code ),
			'%product_price%'   => $product_price,
		);

		return str_replace(
			array_keys( $replaced_values ),
			array_values( $replaced_values ),
			get_option( 'alg_currency_switcher_format', '%currency_name%' )
		);
	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings
	 * @param array $old_instance Old widget settings
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance['currencies']     = sanitize_text_field( $new_instance['currencies'] );

		return $new_instance;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'currencies' => '' ) );
		$type     = sanitize_title( $instance['currencies'] );

		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'currencies' ) ); ?>"><?php esc_html_e( 'Type:', 'oroco' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'currencies' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'currencies' ) ); ?>">
				<option value="name" <?php echo ( 'name' == $type ) ? 'selected' : '' ?>><?php esc_html_e( 'Name', 'oroco' ); ?></option>
				<option value="description" <?php echo ( 'description' == $type ) ? 'selected' : '' ?>><?php esc_html_e( 'Description', 'oroco' ); ?></option>
			</select>
		</p>

		<?php
	}

}