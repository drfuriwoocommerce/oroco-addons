<?php

class Oroco_Language_Widget extends WP_Widget {

	/**
	 * Sets up a new Text widget instance.
	 *
	 * @since  2.8.0
	 * @access public
	 */
	public function __construct() {

		$widget_ops  = array(
			'classname'   => 'widget_text oroco-widget__language',
			'description' => esc_html__( 'Shows language list by WPML plugin', 'oroco' ),
		);
		$control_ops = array( 'width' => 400, 'height' => 350 );
		parent::__construct( 'oroco-language-widget', esc_html__( 'Oroco - Language', 'oroco' ), $widget_ops, $control_ops );
	}

	/**
	 * Display widget
	 *
	 * @param array $args     Sidebar configuration
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		echo wp_kses_post($before_widget);

		?>

		<div class="header-languages">
			<?php echo oroco_language_switcher(); ?>
		</div>
		<?php

		echo wp_kses_post($after_widget);

	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings
	 * @param array $old_instance Old widget settings
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	public function form( $instance ) {

		?>

		<p>
			<label><?php esc_html_e( 'This widget will show language by WPML plugin', 'oroco' ); ?></label>
		</p>

		<?php
	}

}